import $ from 'jquery';

if (!("ontouchstart" in document.documentElement))
    document.documentElement.className += " no-touch";
else
    document.documentElement.className += " touch";

(function ($) {
    /*Initializing START*/
    $(window).resize(function () {
        if ($(window).width() <= 767) {
            $('.search-box').click(function () {
                $(this).addClass('mobile-search');
                $('.default-ui').hide();
                $('.mobile-ui').show();
            });
            $('.search-arrow').click(function () {
                $('.default-ui').show();
                $('.mobile-ui').hide();
                $('.mobile-icon').hide();
                $('.search-box').removeClass('mobile-search');
            });
            $('.mobile-icon').click(function () {
                $(this).hide();
                $('.downshift-dropdown').hide();
            });
            $('.mobile-search').keydown(function () {
                $('.mobile-icon').show();
            });
        }
        else {
            $('.search-box').removeClass('mobile-search');
            $('.mobile-icon').hide();
            $('.mobile-ui').hide();
        }
        if ($(window).width() < 1201) {
            $(".main").removeClass('nav-wrap');
            $("body").click(function (event) {
                if ((!$(event.target.parentElement).hasClass("menuBtn")) && (!$(event.target.parentElement).hasClass("nav-parent"))) {
                    if ($(".main").hasClass("nav-wrap")) {
                        $(".main").removeClass('nav-wrap');
                    }
                }
                $('body').removeClass('fixed');
            });
            $('body').on('click', '.menuBtn', function () {
                $('body').toggleClass('fixed');
            });
        }
        else {
            $('body').on('click', '.menuBtn', function () {
                $('body').removeClass('fixed');
            });
        }

    });

    $(document).ready(function () {
        if (this.currentItem == this.maximumItem) {
            $('.next').addClass('test');
        }
        $('body').on('mouseout', '.downshift-dropdown', function () {

            $('.dropdown-item').css({ 'background': '#fff', 'color': '#212529' });
        });

        $('body').on('click', '.profile-tabs .nav-link', function () {
            var ht = $(this).html();
            $('.breadcrumb-item.active').html(ht);
        });
        $('body').on('click', '.dropdown-menu .dropdown-item', function () {
            var ht = $(this).html();
            $('body').find('.breadcrumb-item.active').html(ht);
            $('dropdown-toggle .dropdown-item').removeClass('active');
        });
        $('body').on('click', '.nav-pills #personal-info-tab', function () {
            $(this).trigger("#personal-info-tab");
        });
        if ($(window).width() < 1201) {
            $('body').on('click', '.menuBtn', function () {
                $('body').toggleClass('fixed');
            });
        }
        else {
            $('body').removeClass('fixed');
        }
        // Main left sidebar navigation 
        $('body').on('click', '.menuBtn', function () {
            if ($(".main").hasClass("nav-wrap")) {
                $(".main").removeClass('nav-wrap');
                // $("body").removeClass('open-sidebar');
            }
            else {
                $('.main').addClass('nav-wrap');
            }
        });
        // Main left sidebar navigation  end
        // Filter right navigation 
        $('body').on('click', '.filter-btn', function () {
            $('.filter-sidebar').toggleClass('open-filter-nav');
            $('body').toggleClass('open-filter');
        });
        // on boldy click sidenavbar close jquery start
        if ($(window).width() <= 767) {
            $('.search-box').click(function () {
                $(this).addClass('mobile-search');
                $('.default-ui').hide();
                $('.mobile-ui').show();
            });
            $('.search-arrow').click(function () {
                $('.default-ui').show();
                $('.mobile-ui').hide();
                $('.mobile-icon').hide();
                $('.search-box').removeClass('mobile-search');
            });
            $('.mobile-icon').click(function () {
                $(this).hide();
                $('.downshift-dropdown').hide();
            });
            $('.mobile-search').keydown(function () {
                $('.mobile-icon').show();
            });
        }
        else {
            $('.search-box').removeClass('mobile-search');
            $('.mobile-icon').hide();
            $('.mobile-ui').hide();
            $('.mobile-search').keydown(function () {
                $('.desk-close').show();
                $('.desk-icon').hide();
            });
        }

        if ($(window).width() < 1201) {
            $(".main").removeClass('nav-wrap');
            $("body").click(function (event) {
                if ((!$(event.target.parentElement).hasClass("menuBtn")) && (!$(event.target.parentElement).hasClass("nav-parent"))) {
                    if ($(".main").hasClass("nav-wrap")) {
                        $(".main").removeClass('nav-wrap');
                    }
                }
                $('body').removeClass('fixed');
            });
        }

        $('body').on('click', '.page-overlay', function () {
            if ($(".main").hasClass("nav-wrap")) {
                $(".main").removeClass('nav-wrap');
            }
            else {
                $('.main').addClass('nav-wrap');
            }

        });
        // on boldy click sidenavbar close jquery end

        // Filter right navigation End 
        $('.menu-icon').on('click', function () {
            $(this).css('z-index', '10000');
            $(this).find('.top-line').toggleClass('top-line-rotate');
            $(this).find('.bot-line').toggleClass('bot-line-rotate');
            $(this).find('.mid-line').toggle();
            $('.left-nav').toggleClass('open');
            $('.overlay').fadeToggle();
        });
        $('.overlay').on('click', function () {
            $('.left-nav').removeClass('open');
            $('.top-line').removeClass('top-line-rotate');
            $('.bot-line').removeClass('bot-line-rotate');
            $('.mid-line').fadeIn();
            $(this).fadeOut();
        });
        $('a[href="#"]').click(function (e) {
            e.preventDefault();
        });
        $(".get-year").text((new Date()).getFullYear());


        /* Scroll to positopn */
        $(document).on("scroll", onScroll);
        $('.main-nav a').on('click', function (e) {
            e.preventDefault();
            $(document).off("scroll");

            $('a').each(function () {
                $(this).removeClass('active');
            })
            let target = this.hash;
            let $target = $(target);
            $('html, body').stop().animate({
                'scrollTop': $target.offset().top - 100
            }, 1000, 'swing', function () {
                window.location.hash = target;
                $(document).on("scroll", onScroll);
            });
        });
    });

    function onScroll(event) {
        var scrollPosition = $(document).scrollTop();
        $('.main-nav a').each(function () {
            var currentLink = $(this);
            var refElement = $(currentLink.attr("href"));
            if (refElement.position().top - 100 <= scrollPosition && refElement.position().top + refElement.height() > scrollPosition) {
                $('.main-nav a').removeClass("active");
                currentLink.addClass("active");
            }
            else {
                currentLink.removeClass("active");
            }
        });
    }

})($);

/*Initializing END*/
