import {
	LOG_OUT,
	USER_REGISTER,
	GET_SINGLE_PRODUCT,
	GET_SUBCATNAME,
	GET_PRODUCTS,
	CAT_DETAILS,
	SUB_PAGE,
	USER_INFO,
	WITHOUT_TOKEN,
	GET_PRODUCTS_Cat,
	SIDE_BAR,
	SELECT_DROP_DOWN,
	SEARCH_BAR,
	OPEN_SIDEBAR
} from '../actions';


const INITIAL_STATE = {
	singleproduct: '',
	getproductId: '',
	getproductIdCat: '',
	catDetails: {},
	getProductnames: {},
	subCatDetails: {},
	products: [],
	subpage: [],
	updateShippingAddress: {},
	userDetails: {},
	userInfo: {},
	withOutToken: [],
	sideBarId: {},
	selectDropDown: '',
	searchBar: [],
	productCatPrevious: {},
	openSideBar: ''
}

export default (state = INITIAL_STATE, action) => {

	switch (action.type) {
		/************************************* Register ***************************************/
		case USER_REGISTER:
			return action.payload || false;
		/*************************************** Logout ***************************************/
		case LOG_OUT:
			return INITIAL_STATE;
		/************************************ GetSingleProduct ********************************/
		case GET_SINGLE_PRODUCT:
			return Object.assign({}, state, {
				singleproduct: action.payload
			});
		/************************** Get Categories Id from SideBar ****************************/
		case GET_PRODUCTS:
			return Object.assign({}, state, {
				getproductId: action.payload
			});
		/************************* Get Categories Id from homePage ****************************/
		case GET_PRODUCTS_Cat:
			return Object.assign({}, state, {
				getproductIdCat: action.payload
			});
		/************************** Get Side Bar Id within the page ***************************/
		case SIDE_BAR:
			return Object.assign({}, state, {
				sideBarId: action.payload
			});
		/***************************** User Details for Profile *******************************/
		case USER_INFO:
			return Object.assign({}, state, {
				userInfo: action.payload
			});
		/****************************** Home Page Slider Ids **********************************/
		case SUB_PAGE:
			return Object.assign({}, state, {
				subpage: action.payload
			});
		/**************************** Side Bar to store Id  ***********************************/
		case CAT_DETAILS:
			return Object.assign({}, state, {
				catDetails: action.payload
			});
		/******************************** Side Bar to store Id  *******************************/
		case GET_SUBCATNAME:
			return Object.assign({}, state, {
				subCatDetails: action.payload
			});
		/******************************* local Storage data ***********************************/
		case WITHOUT_TOKEN:
			return Object.assign({}, state, {
				withOutToken: action.payload
			});
		/******************************* Select Drop Down ***********************************/
		case SELECT_DROP_DOWN:
			return Object.assign({}, state, {
				selectDropDown: action.payload
			});
		/******************************* Search-Bar ***********************************/
		case SEARCH_BAR:
			return Object.assign({}, state, {
				searchBar: action.payload
			});
		/******************************* Open side bar ***********************************/
		case OPEN_SIDEBAR:
			return Object.assign({}, state, {
				openSideBar: action.payload
			});

		default:
			return state;
	}
}