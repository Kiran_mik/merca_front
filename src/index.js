import React from 'react';
import ReactDOM from 'react-dom';
import App from './App.jsx';
import * as serviceWorker from './serviceWorker';
import {
    BrowserRouter
} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'owl.carousel/dist/assets/owl.carousel.min.css';
import 'antd/dist/antd.css';
import "react-image-gallery/styles/css/image-gallery.css";
import 'rc-pagination/assets/index.css';
import './assets/css/style.css';
import './assets/css/responsive.css';
import 'jquery/dist/jquery.min.js';
import './assets/js/custom';
import 'bootstrap/dist/js/bootstrap.min.js';
import 'owl.carousel/dist/owl.carousel.min.js';
import "babel-polyfill/dist/polyfill.min.js";

ReactDOM.render( <BrowserRouter> < App /> </BrowserRouter> , document.getElementById('root'));
        serviceWorker.register();