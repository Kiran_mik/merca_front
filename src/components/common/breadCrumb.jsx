import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import { withRouter } from "react-router-dom";
import { Select } from 'antd';
import 'antd/dist/antd.css';

const BreadCrumb = ({ isSort, categoryNamesArray, sort, sortProducts, history, getproductIdCat, getproducts, getSubCat, breadCrums, selectDropDown, urlFromCategory, urlField, bread }) => {
	return (
		<React.Fragment>
			{breadCrums ?
				<nav aria-label="breadcrumb" className="mobile-d-none">
					<ol className="breadcrumb" style={{ textTransform: 'capitalize' }}>
						{/* style={{ textTransform: 'capitalize' }} */}
						<li className="breadcrumb-item active" aria-current="page" onClick={() => history.push('/')} style={{ cursor: 'pointer' }}>Inicio</li>
						{(bread[1] && bread[1] !== "allProducts" && bread[1] !== "all")
							&&
							(bread[2] ? <li className="breadcrumb-item active" aria-current="page" onClick={() => history.push(`/${bread[1]}`)}>
								{bread[1].split("-").join(" ")}
							</li> : <li className="breadcrumb-item active" aria-current="page">
									{bread[1].split("-").join(" ")}
								</li>)
						}
						{bread[2] && bread[2] !== "product" && bread[1] !== "allProducts" ?
							(bread[3] ?
								<li className="breadcrumb-item active" aria-current="page" onClick={() => history.push({ pathname: `/${bread[1]}/${bread[2]}` })}>
									{bread[2].split("-").join(" ")}
								</li> :
								<li className="breadcrumb-item active" aria-current="page">
									{bread[2].split("-").join(" ")}
								</li>)
							:
							((bread[1] === "all" && bread[2] !== "product") || (bread[1] === "allProducts" && bread[2] !== "product")) ?
								<li className="breadcrumb-item active" aria-current="page" onClick={() => history.push({ pathname: `/${bread[2]}` })}>
									{bread[2].split("-").join(" ")}
								</li> : ''
						}
						{bread[3] && <li className="breadcrumb-item active" aria-current="page">
							{bread[3].split("-").join(" ")}
						</li>}
					</ol>
				</nav>
				: ''}
			{
				isSort ?
					//***************************** Select DropDown Starts *******************************//
					<div className="selectdropdownBox">
						<div className="select-Box mobile-d-none">
							<label htmlFor="" className="col-form-label text-right" > Ordenar: </label>
							<Select className="dropdown-select" value={sort}
								onChange={(value) => selectDropDown(value)} >
								<Select.Option value="">Relevancia</Select.Option>
								<Select.Option value={1}>Precio de menor a mayor</Select.Option>
								<Select.Option value={-1}>Precio mayor a menor</Select.Option>
							</Select>
						</div>
						<button className="filter-btn btn mobile-d-block" title="Filter"> Filtrar </button>
					</div>
					//***************************** Select DropDown Ends *******************************//
					:
					undefined

			}
		</React.Fragment>
	);
}

const mapStateToProps = state => ({});
export default withRouter(connect(mapStateToProps, actions)(BreadCrumb));
