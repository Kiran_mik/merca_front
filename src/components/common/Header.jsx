
import { withRouter, Link } from "react-router-dom";
import React, { Component } from 'react';
import queryString from 'query-string';
import { connect } from 'react-redux';
import Downshift from 'downshift';
import _ from 'lodash';
import { IMAGE_URL } from '../../config/configs';
import CF from './commonFuntions';
import EM from './errorMessages'
import * as actions from '../../actions';
import menubtn from '../../../src/assets/images/menu-btn.svg';
import logo from '../../../src/assets/images/logo.svg';
import searchIcon from '../../../src/assets/images/search-icon.svg';
import userIcon from '../../../src/assets/images/user-icon.svg';
import cartIcon from '../../../src/assets/images/cart-icon.svg';
import SideBar from '../common/SideBar';
// import axios from 'axios';
class Header extends Component {
    state = {
        name: '',
        photo: "",
        countOfprod: 0,
        searchText: '',
        suggestions: [],
        page: 1,
        pagesize: 10,
        pathname: '',
        mobileView: false,
        selectedItem: '',
        redirectPage: '',
        buttonEnter: false,
        sideBarId: ''
    }
    validTokenOrNot = async () => {
        const response = await CF.callApi('/user/isTokenValid', '', 'get', false, true);
        if (response.message === "Invalid token.") {
            CF.showErrorMessage("Sesión expirada.");
            CF.removeItem(["token", "fname", "photo"]);
        }
    }
    logout = async () => {
        let body = { "token": localStorage.getItem('token') }
        const response = await CF.callApi('/auth/logOut', body, 'post', false, true);
        if (response.status === 1) CF.showSuccessMessage(EM._242);
        CF.removeItem(["token", "fname", "photo"]);
        this.props.history.push("/")
    }

    componentWillReceiveProps(nextProps) {
        // to get the card items count -- Start
        let count = localStorage.getItem('withOutToken');
        count = JSON.parse(count);
        const productCountArray = count && Array.isArray(count) && count.length ? count.map((each) => each.productCount) : [];
        const countOfprod = productCountArray.length !== 0 ? productCountArray.reduce((a, c) => Number(a) + Number(c)) : 0;
        // to get the card items count -- End
        // For redirecting page for after login
        let redirectPage = window.location.pathname.split("/");
        // redirectPage = redirectPage.reduce((a, c) => a + c);
        // for getting the name 
        let name = localStorage.getItem("fname");
        name = _.isEmpty(name) ? 'Sign In' : name;
        // for getting the photo
        const photo = localStorage.getItem("photo")
        // to get the query values 
        let { searchText } = queryString.parse(window.location.search);
        searchText = searchText ? searchText : '';
        // for getting the router link
        const { pathname } = nextProps.location;
        if (pathname === '/cart' && localStorage.getItem('token') !== null) this.validTokenOrNot();
        this.setState({ pathname, countOfprod, name, searchText, selectedItem: searchText, photo, redirectPage });
    }


    //********************* Search DropDown List Starts*******************//


    // async searchProduct() {
    //     const { page, pagesize, searchText, suggestions } = this.state
    //     console.log("searchProduct in searchText ===>", searchText)
    //     console.log("searchProduct ===>suggestions", suggestions)
    //     const body = { searchText, page, pagesize };
    //     const response = await CF.callApi('/products/searchProduct', body, 'post');
    //     console.log("response ===>>>", response);
    //     if (response.status === 1) {
    //         const { data } = response;
    //         const { productListing } = data;
    //         console.log("searchProduct API ===>if   suggestions", productListing)
    //         await this.setState({ suggestions: productListing });
    //     }
    //     else if (response.status === 0) {
    //         this.setState({ suggestions: [] })
    //         console.log("no response searchProduct===>else suggestions")
    //     }
    //     // this.setState({ suggestions: [] });
    // }
    //********************* Search DropDown List Ends*******************//

    //********************* Search HandleChange starts*******************//
    handleChange = ({ target }) => {
        const { value } = target;
        return this.setState({ searchText: value, buttonEnter: false }
            // async () => await this.searchProduct()
        )
    }

    // downshiftOnChange = (searchText, e) => {
    //     // console.log("downshiftOnChange===>")
    //     // console.log("downshiftOnChange===>suggestions", this.state.suggestions)
    //     if (_.isEmpty(searchText)) return;
    //     const data = searchText.productName;
    //     this.setState({ mobileView: false, searchText: data, selectedItem: data, buttonEnter: false, suggestions: [] }, () => this.searchAllProducts(e))
    // }

    searchAllProducts = async (e) => {
        const { searchText } = this.state;
        this.setState({ suggestions: [] })
        if (typeof e.preventDefault === 'function') e.preventDefault();
        if (_.isEmpty(searchText)) return;
        this.setState({ mobileView: false, buttonEnter: true, searchText, selectedItem: searchText }, () => this.props.history.push(`/searchProducts?searchText=${searchText}`))
    }
    //********************* Search HandleChange Ends *******************//

    //********************* Input Change for On EnterKeyWord Starts  ********************//
    handleKeyPress = (e) => {
        window.gtag('event', 'Search');
        const { searchText } = this.state;
        this.setState({ mobileView: true })
        e.persist();
        if (e.key === 'Enter') return this.setState({ buttonEnter: false, searchText }, () => this.searchAllProducts());
        return;
    }

    handleClick = () => {
        this.setState({ mobileView: true, buttonEnter: false })
    }
    //********************* Input Change for On EnterKeyWord Starts*******************//

    //********************* For Mobile View && Web to Clear*******************//
    resetForm = () => {
        this.setState({ selectedItem: '' })
    }
    openSideBar = () => {
        // this.props.openSideBar('toggle-sidenav')
        this.setState({ sideBarId: 'toggle-sidenav' })
    }

    render() {
        const searchTextFromQuery = queryString.parse(window.location.search).searchText;
        let { suggestions, countOfprod, name, photo, pathname, redirectPage, mobileView, selectedItem, searchText, buttonEnter, sideBarId } = this.state;
        searchText = searchTextFromQuery ? searchTextFromQuery : searchText;
        suggestions = _.isEmpty(searchText) ? [] : suggestions;
        let token = localStorage.getItem('token')
        return (
            <div className="page-wrap">
                <header className="header">
                    <nav className={(mobileView === true) ? "navbar navbar-light navbar-custom headersearchbox" : "navbar navbar-light navbar-custom"}>
                        <div className="left-navBox" >
                            <div className="menu-wrapper" onClick={this.openSideBar}>
                                <div className='menuBtn mobile-m'><img src={menubtn} alt="Menu Button" width='22px' height='16px' />
                                </div>
                                <span className="fa fa-arrow-left search-arrow" onClick={() => this.setState({ mobileView: false })}></span>
                            </div>
                            <Link to={'/'} className="navbar-brand" title="Mercado Mayorista" >
                                <img src={logo} alt="Mercado Mayorista" width="231px" height="29px" className="img-fluid" />
                                <h1 className="header-Logo" title="Mercado Mayorista"><span>Mercado Mayorista</span></h1>
                            </Link>
                        </div>
                        {(pathname !== '/login' && pathname !== '/register' && pathname !== '/verifiedUser') &&
                            <div className="justify-content-md-center" >
                                <form className="has-search" onSubmit={this.searchAllProducts} >
                                    <Downshift
                                        onChange={this.downshiftOnChange}
                                        initialInputValue={searchText}
                                        selectedItem={selectedItem ? selectedItem : ''}
                                    >
                                        {({
                                            getInputProps,
                                            getItemProps,
                                            highlightedIndex,
                                            isOpen,
                                        }) => (
                                                <div>
                                                    {buttonEnter ?
                                                        <input
                                                            className={mobileView === true ? "form-control search-box mobile-search" : "form-control search-box "}
                                                            value={selectedItem ? selectedItem : ''}
                                                            readOnly
                                                            {...getInputProps({
                                                                placeholder: "Buscar Productos",
                                                                onChange: this.handleChange,
                                                                onFocus: this.handleKeyPress,
                                                                onClick: this.handleClick
                                                            })}
                                                        />
                                                        :
                                                        <input
                                                            className={mobileView !== true ? "form-control search-box mobile-search" : "form-control search-box "}
                                                            value={selectedItem ? selectedItem : ''}
                                                            {...getInputProps({
                                                                placeholder: "Buscar Productos",
                                                                onChange: this.handleChange,
                                                                onFocus: this.handleKeyPress,
                                                            })}
                                                        />}
                                                    {_.isEmpty(selectedItem) ?
                                                        <span className="form-control-feedback desk-icon" ><img src={searchIcon} alt="" width='18px' height='18px' /></span>
                                                        :
                                                        <span className="fa fa-times desk-close form-control-feedback" onClick={this.resetForm}></span>
                                                    }
                                                    {isOpen && !_.isEmpty(suggestions) && (
                                                        <div className="downshift-dropdown">
                                                            {suggestions
                                                                .slice(0, 10)
                                                                .map((item, index) => {
                                                                    return (<div
                                                                        className="dropdown-item"
                                                                        {...getItemProps({
                                                                            key: index,
                                                                            index,
                                                                            item,
                                                                            style: {
                                                                                color: highlightedIndex === index ? 'white' : '#212529',
                                                                                backgroundColor: highlightedIndex === index ? '#2472DC' : 'white',
                                                                                fontWeight: selectedItem === item ? 'bold' : 'normal'
                                                                            }
                                                                        })} >
                                                                        {item.productName}
                                                                    </div>)
                                                                })}
                                                        </div>
                                                    )}
                                                </div>
                                            )
                                        }
                                    </Downshift>
                                </form>
                            </div>
                        }
                        <div className="navbar-right">
                            <div className="mobile-icon" >
                                <span className="fa fa-times" onClick={this.resetForm}></span>
                            </div>
                            <div className="desktop-icon" >
                                {pathname !== '/login' && pathname !== '/register' && pathname !== '/verifiedUser' && pathname !== '/forgotPassword' && pathname !== '/resetPassword' ?

                                    (name !== "Sign In" && token) ?
                                        (<div className="dropdown">
                                            <button className="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                {(photo === "" || photo === null) ?
                                                    <span className="sm-profile-img">
                                                        <img src={userIcon} alt="" style={{}} className="nouser" />
                                                    </span> :
                                                    <span className="sm-profile-img">
                                                        <img src={IMAGE_URL + photo} alt="" style={{ 'borderRadius': "50%", "width": 25, "height": 25 }} />
                                                    </span>}
                                                <span>{name}</span>
                                            </button>
                                            <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a className="dropdown-item" title="Mis ordenes" role="tab" onClick={() => this.props.history.push('/profile', 'Mis ordenes')}><span className="icon-icon-my-order" /> Mis ordenes</a>
                                                <a className="dropdown-item" title="Perfil" onClick={() => this.props.history.push('/profile', 'Perfil')}><span className="icon-icon-user" /> Perfil</a>
                                                <a className="dropdown-item" title="Administrar dirección" onClick={() => this.props.history.push('/profile', 'Administrar dirección')}><span className="icon-icon-book" /> Administrar dirección</a>
                                                <a className="dropdown-item" onClick={this.logout} title="Desconectarse"><span className="icon-icon-logout" /> Desconectarse</a>
                                            </div>
                                        </div>
                                        ) :
                                        (<div>
                                            <a className="login-btn" onClick={() =>
                                                this.props.history.push(`/login${"?redirectPage=" + (redirectPage[1] === undefined ? "" : redirectPage[1])}${(redirectPage[2] ? '/' + redirectPage[2] : '')}${(redirectPage[3] ? '/' + redirectPage[3] : '')}${(redirectPage[4] ? '/' + redirectPage[4] : '')}${(redirectPage[5] ? '/' + redirectPage[5] : '')}`)}>
                                                <img src={userIcon} alt="userImage" className="mr-2" /><label >Ingresar</label>
                                            </a>
                                        </div>)
                                    : null}
                                <div >
                                    <div className="cartIcon-box" onClick={() => this.props.history.push('/cart')}>
                                        <img src={cartIcon} alt="carrito" width={17} height="18px" />
                                        <span>{countOfprod}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </nav>
                </header>
                <SideBar header={sideBarId} />
            </div>
        );
    }
}

// *************************component will recevie props working need to check *********************/
const mapStateToProps = state => ({
    countValue: state.user.withOutToken,
    userInfo1: state.user.userInfo,
});

export default withRouter(connect(mapStateToProps, actions)(Header));
