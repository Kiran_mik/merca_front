import swal from "sweetalert";
import axios from 'axios';
import {
    API_URL
} from '../../config/configs';
import {
    withRouter
} from "react-router-dom";

export default {
    /*************************************************************************************
        @Purpose : We can use following function to use localstorage  -- Start
    **********************************  Start  *******************************************/
    // for set an item
    setItem(key, value) {
        localStorage.setItem(key, value);
    },

    // To get the value of item
    getItem(key) {
        return localStorage.getItem(key);
    },

    // To remove the item
    removeItem(key) {
        if (typeof key === 'string') localStorage.removeItem(key);
        else key.map(k => localStorage.removeItem(k))
    },

    // To know the status of accessToken
    isAuthenticated() {
        return this.getToken('accessToken') ? true : false;
    },
    /**********************************   End  **********************************************/


    // for email validate
    validateEmail(email) {
        //    var  pattern = "^\w+([\.-_]?\w+)*@\w+([\.-_]?\w+)*(\.\w{2,3})+$"
        var pattern = new RegExp(/^(([^<>()\]\\.,;:\s@"]+(\.[^<>()\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);

        return pattern.test(email);
    },

    // for password validate
    validatePassword(pass) {
        var pattern = new RegExp(/^.{6,}$/);
        return pattern.test(pass);
    },

    // for mobile number validate
    validateMobileNumber(mobileNo) {
        var pattern = /^\d{8,20}$/;
        return pattern.test(mobileNo);
    },

    // for mobile number validate
    validatePinNumber(pin) {
        var pattern = /^\d{7,8}$/;
        return pattern.test(pin);
    },

    // for pincode number validate
    validatePincodeNumber(mobileNo) {
        var pattern = new RegExp(/^[0-9]{4}(?:-[0-9]{4})?$/);
        return pattern.test(mobileNo);
    },

    // only for characters
    allowChar(e) {
        const pattern = new RegExp(/^[a-zA-Z\s]{0,255}$/);
        return pattern.test(e);
    },

    // only for numbers
    allowNumbers(e) {
        const pattern = new RegExp(/^[0-9\b]+$/);
        return pattern.test(e);
    },

    /*************************************************************************************
        @PURPOSE    : Success Message.
        @Parameters :{
                        msg: message
                     }
    **************************************************************************************/
    showSuccessMessage(msg) {
        swal({
            position: 'center',
            icon: 'success',
            title: msg,
            buttons: false,
            timer: 1500,
            className: 'custom-toaster'
        });
    },

    /*************************************************************************************
        @PURPOSE    : Error Message.
        @Parameters :{
                        msg: message
                     }    
    **************************************************************************************/
    showErrorMessage(msg) {
        swal({
            position: 'center',
            icon: 'error',
            title: msg,
            buttons: false,
            timer: 1500,
            className: 'custom-toaster'
        });
    },
    /*************************************************************************************
        @PURPOSE    : Error Message POP-UP WithOut TimeLimit .
        @Parameters :{
                        msg: message 'ok'
                     }    
    **************************************************************************************/
    showPopUp(msg) {
        swal({
            position: 'center',
            icon: 'error',
            title: msg,
            button: true,
            className: 'custom-toaster'
        });
    },
    /*************************************************************************************
        @PURPOSE    : Warning Message.
        @Parameters :{
                        msg: message
                     }                            
    **************************************************************************************/
    showWarningMessage(msg) {
        swal({
            position: 'center',
            icon: 'warning',
            title: msg,
            buttons: false,
            timer: 1500,
            className: 'custom-toaster'
        });
    },
    /*************************************************************************************
        @PURPOSE    : Info Message.
        @Parameters :{
                        msg: message
                     }
    **************************************************************************************/
    showInfoMessage(msg) {
        swal({
            position: 'center',
            icon: 'info',
            title: msg,
            buttons: false,
            timer: 2500,
            className: 'custom-toaster'
        });
    },
    /*******************************************************************************************/


    /*******************************************************************************************
        @PURPOSE    : Call api.
        @Parameters :{
                        url : <url of api>
                        data : <data object (JSON)>
                        method : String (get, post)
                        isForm (Optional) : Boolean - to call api with form data
                        isPublic (Optional) : Boolean - to call api without auth header
                    }
    /*****************************************************************************************/
    callApi(url, body, method, isForm = false, isAuthorized = false) {
        url = API_URL + url;
        if (body === '') body = {};
        let headers = {
            'Content-Type': 'application/json'
        };
        if (isAuthorized) headers.Authorization = this.getItem('token');
        if (isForm) headers['Content-Type'] = 'multipart/form-data';
        // Api calling
        return new Promise(async (resolve, reject) => {
            await axios({
                method,
                url,
                headers,
                data: body
            })
                .then(response => {
                    let {
                        data
                    } = response;
                    resolve(data);
                    if (data.status === 0) {
                        // this.showWarningMessage(data.message);
                        if (data.statusCode === 407 || data.message === 'Invalid token.') {
                            this.removeItem('token');
                            this.removeItem('fname');
                            this.props.history.push('/login');
                        }
                    }
                })
                .catch((error) => {
                    console.log("error is ", error);
                    reject(error);
                });
        })
    }

}