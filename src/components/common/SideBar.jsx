import React, { Component } from "react";
import { withRouter, Link } from "react-router-dom";
import { connect } from "react-redux";
import * as actions from "../../actions";
import { IMAGE_URL } from "../../config/configs";
import CF from './commonFuntions';
import logo from '../../../src/assets/images/logo.svg';
// import noImage from '../../../src/assets/images/noimage.jpg';
import $ from "jquery";
// import ReactGA from 'react-ga';

window.jQuery = $;
window.$ = $;
global.jQuery = $;

class SideBar extends Component {
	state = {
		categorynames: [],
	};

	componentDidMount() {
		this.getCategories();
		let reduxSideBar = this.props.openSideBar1 ? this.props.openSideBar1 : '';
		if ($(window).width() <= 1200) {
			$(".main").removeClass('nav-wrap');
		}
		$(".page-overlay").click(function () {
			$(".main").removeClass('nav-wrap');
		});
		this.setState({ reduxSideBar })
		// console.log('props', this.props.openSideBar1)
	}

	async getCategories() {
		const response = await CF.callApi('/categories/getCategories', {}, 'get');
		if (response.status === 1) {
			let { data } = response;
			this.setState({ categorynames: data });
		}
	};
	//************************** going to subLevels ******************************//
	getProducts = (id, name, customUrl, total) => {
		if (total > 0) {
			window.gtag('config', 'G-WB2B2WMR7Q', {
				'page_title': 'Side Bar',
				'page_path': `/${customUrl}`
			});
			window.gtag('event', 'product Category');
			this.props.sideBarId(id)
			this.props.getproductIdCat(id);
			this.props.selectDropDown("");
			this.props.history.push(`/${customUrl}`);
			this.props.catName(name);
		}
	};
	//************************** going to final level if no subLevels ******************************//
	getproductSubSubCategory = (id, count, name, total, customUrl, categoryName) => {
		if (count === 0 && total !== 0) {
			window.gtag('config', 'G-WB2B2WMR7Q', {
				'page_title': 'Side Bar',
				'page_path': `/all/product/${customUrl}`
			});
			window.gtag('event', 'Products');
			this.props.getSubCat(id, name)
			this.props.history.push(`/all/product/${customUrl}`, customUrl)
		}
	}

	render() {
		const { categorynames } = this.state;
		return (
			<React.Fragment>
				<div className={this.props.header !== '' ? "main menuBtn nav-wrap" : 'main menuBtn'}>
					<div id="sidebar-wrapper" className='sidebar' >
						<Link to={'/'} className="navbar-brand" title="Mercado Mayorista" >
							<img src={logo} alt="Mercado Mayorista" width="231px" height="29px" className="img-fluid" />
							<h1 className="header-Logo" title="Mercado Mayorista" >
								<span>Mercado Mayorista</span>
							</h1>
						</Link>
						<ul className="navbar-nav" id="accordionExample">
							{categorynames.map((each, i) => {
								if (each.total > 0) {
									const icon = each.icon ? IMAGE_URL + each.icon : IMAGE_URL + 'icon.jpg';
									const categoryName = each.categoryName ? each.categoryName : "Category";
									const total = each.total ? each.total : 0;
									return (
										<li className="nav-item nav-parent" key={i}>
											<a className="nav-link" data-toggle="collapse" aria-expanded="false" aria-controls="collapseExample"
												onClick={each.count === 0 ? () => this.getproductSubSubCategory(each._id, each.count, each.categoryName, total, each.customUrl, each.categoryName) : () => this.getProducts(each._id, each.categoryName, each.customUrl, total, each.count, each.categoryName)}
											>
												<img src={icon} alt="icon" width="18px" height="21px" />{categoryName}
												<span className="badge greenBg">{total}</span>
											</a>
										</li>
									);
								}
							})}
						</ul>
						{/******************* Sidebar Footer section Start **************************/}
						<div className="sidebar-footer">
							<ul className="social-media text-center">
								<li>
									<a title="facebook" className="facebook-icon" rel="noopener noreferrer" href="https://www.facebook.com/mercadomayoristaargentina " target="_blank" >
										<i className="fab fa-facebook-f" />
									</a>
								</li>
								<li>
									<a title="twitter" className="twitter-icon" rel="noopener noreferrer" href="https://twitter.com/search?q=mercadomayorista" target="_blank">
										<i className="fab fa-twitter" />
									</a>
								</li>
								<li>
									<a title="linkedin" className="linkedin-icon" rel="noopener noreferrer" href="https://www.linkedin.com/company/mayoristas-&-mercado" target="_blank">
										<i className="fab fa-linkedin-in" />
									</a>
								</li>
								<li>
									<a title="instagram" className="instagram-icon" rel="noopener noreferrer" href="https://www.instagram.com/mercadomayorista/" target="_blank">
										<i className="fab fa-instagram" />
									</a>
								</li>
							</ul>
							<div className="copyrightbox text-center">
								<p className="copyrights-text">© Copyright 2019. Todos los derechos reservados.</p>
								{/* <a className="copyrights-text" onClick={() => this.props.history.push('/aboutUs')}> Sobre nosotros.</a> */}
								<a className="copyrights-text" onClick={() => this.props.history.push('/preguntas-frecuentes')}>Preguntas frecuentes.</a>
								<a className="copyrights-text" onClick={() => this.props.history.push('/termsAndConditions')}>Términos y Condiciones.</a>

							</div>
						</div>
					</div>
					<div className="page-overlay"></div>
				</div>
			</React.Fragment>
		);
	}
}

const mapStateToProps = state => ({
	getcategoryName: state.user.getcategories,
	openSideBar1: state.user.openSideBar,
});

export default withRouter(connect(mapStateToProps, actions)(SideBar));