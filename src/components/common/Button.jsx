import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import _ from 'lodash';
import EM from './errorMessages'; // ERROR MESSAGE
import CF from './commonFuntions';

class Button extends Component {

    state = {
        buttonEnter: false,
        render: true,
        updateButton: false,
        cartProducts: [],
        data: this.props.data,
        dummyQuantity: '',
        errors: {
            stockQuantity: true,
            quantity: true
        },
        previousValue: '',
        productData: []
    }

    componentDidMount() {
        this.getfromLocalstore();
    }

    getfromLocalstore() {
        const cartProducts = JSON.parse(localStorage.getItem("withOutToken"))
        this.setState({ cartProducts })
    }

    //****************************  most viewed ****************************/
    mostViewed = async (id) => {
        let body = { productId: id }
        await CF.callApi('/products/mostViewed', body, 'post');
    }
    //***************** Input change ********************************* */
    previousValue(currentObject) {
        const { cartProducts, previousValue, updateButton } = this.state;
        currentObject.productCount = previousValue;
        this.setState({ cartProducts })
        localStorage.setItem('withOutToken', JSON.stringify(cartProducts));
        if (updateButton) this.setState({ updateButton: false })
    }

    handleChangeForQuantity(event, currentObject) {
        const { errors } = this.state;
        if (event.target.value > currentObject.stockQuantity) {
            if ((event.target.value)) errors.stockQuantity = EM.INVALID_STOCKQUANTITY;
            else errors.stockQuantity = true;
        }
        if (_.isEmpty(event.target.value)) {
            if (_.isEmpty(event.target.value)) errors.quantity = EM.INVALID_QUANTITY;
            else errors.quantity = true;
        }
        if (CF.allowNumbers(event.target.value) || event.target.value === '') {
            currentObject.productCount = event.target.value
            this.setState({ dummyQuantity: currentObject.productCount, buttonEnter: false })
            return;
        }
        if (!CF.allowNumbers(event.target.value)) return;
    }

    handleKeyPress(event, obj, value) {
        if ((event.key === 'Enter' && event.target.value <= obj.stockQuantity && event.target.value >= obj.minQuantity) || (event.target.value <= obj.stockQuantity && value === "blurevent" && event.target.value >= (obj.minQuantity > obj.stockQuantity ? obj.stockQuantity : obj.minQuantity))) {
            let value1 = event.target.value
            this.SaveDataToLocalStorage(obj, value1)
            this.setState({ buttonEnter: true })
        }
    }

    changeUpdateButton() {
        const { updateButton } = this.state;
        if (!updateButton) this.setState({ updateButton: !updateButton })
    }

    handleClick = (object) => {
        object.isValidButton1 = false
        this.setState({ previousValue: object.productCount, buttonEnter: false, dummyQuantity: object.productCount })
        this.changeUpdateButton()
    }
    //************************ Input change End **********************************/

    //*****************  Function for buttons ********************************* */
    estimatedFunction(product_object) {
        const { cartProducts, updateButton, errors, dummyQuantity } = this.state;
        var isItemaddedtocart = _.filter(cartProducts, object => object._id.toString() === product_object._id.toString())
        if (isItemaddedtocart.length > 0) {
            return (
                <span className="btn btn-block qyn-cart mt-3 add-to-cart-btn">
                    {!updateButton ?
                        <button className="decrement-btn float-left" onClick={() => this.decrementCart(isItemaddedtocart[0])}>
                            <span className="fa fa-minus-circle"></span>
                        </button>
                        :
                        <button className="float-left btn-text" onClick={() => this.previousValue(isItemaddedtocart[0])} >Cancelar</button>

                    }
                    <span className="cart-feild">
                        {(isItemaddedtocart[0].productCount > product_object.stockQuantity || isItemaddedtocart[0].isValidButton1 === true) ? <span className="error-msg text-danger invalid-error-msg"
                        >{`Cantidad máxima ${product_object.stockQuantity} alcanzada`}</span> : ""}
                        {(isItemaddedtocart[0].productCount === "") ? <span className="error-msg text-danger invalid-error-msg"
                        >{errors.quantity}</span> : (isItemaddedtocart[0].productCount >= (isItemaddedtocart[0].minQuantity > isItemaddedtocart[0].stockQuantity ? isItemaddedtocart[0].stockQuantity : isItemaddedtocart[0].minQuantity)) ? '' : <span className="error-msg text-danger invalid-error-msg"
                        >{`Cantidad mínima ${isItemaddedtocart[0].minQuantity > isItemaddedtocart[0].stockQuantity ? isItemaddedtocart[0].stockQuantity : isItemaddedtocart[0].minQuantity}  unid`}</span>}
                        {this.state.buttonEnter ?
                            <input
                                className="form-control mr-2"
                                type="number"
                                name="quantity"
                                value={isItemaddedtocart[0].productCount}
                                onChange={(event) => this.handleChangeForQuantity(event, isItemaddedtocart[0])}
                                onClick={() => this.handleClick(isItemaddedtocart[0])}
                                onKeyPress={(event) => this.handleKeyPress(event, isItemaddedtocart[0])}
                                onBlur={(event) => this.handleKeyPress(event, isItemaddedtocart[0], "blurevent")}
                                readOnly
                            />
                            :
                            <input
                                className="form-control mr-2"
                                type="number"
                                name="quantity"
                                value={isItemaddedtocart[0].productCount}
                                onChange={(event) => this.handleChangeForQuantity(event, isItemaddedtocart[0])}
                                onClick={() => this.handleClick(isItemaddedtocart[0])}
                                onKeyPress={(event) => this.handleKeyPress(event, isItemaddedtocart[0])}
                                onBlur={(event) => this.handleKeyPress(event, isItemaddedtocart[0], "blurevent")}

                            />}
                    </span>
                    {!updateButton ?
                        ((isItemaddedtocart[0].productCount < product_object.stockQuantity || isItemaddedtocart[0].isValidButton1 === false) ?
                            <button className="increment-btn float-right" onClick={() => this.SaveDataToLocalStorage(isItemaddedtocart[0])}>
                                <span className="fa fa-plus-circle"></span>
                            </button> :
                            <button className="increment-btn float-right blur_image">
                                <span className="fa fa-plus-circle"></span>
                            </button>) :
                        (isItemaddedtocart[0].productCount === "" || isItemaddedtocart[0].productCount > product_object.stockQuantity || isItemaddedtocart[0].productCount < (isItemaddedtocart[0].minQuantity > product_object.stockQuantity ? product_object.stockQuantity : isItemaddedtocart[0].minQuantity)) ?
                            <button className="float-right blur_image btn-text">Actualizar</button> :
                            <button className="float-right btn-text" onClick={() => this.SaveDataToLocalStorage(isItemaddedtocart[0], dummyQuantity)}>Actualizar</button>
                    }
                </span>
            )
        }
        else {
            return (
                (product_object.stockQuantity) &&
                <button className="btn btn-block qyn-cart mt-3" onClick={() => this.SaveDataToLocalStorage(product_object, product_object.minQuantity > product_object.stockQuantity ? product_object.stockQuantity : product_object.minQuantity, product_object.minQuantity >= product_object.stockQuantity ? true : false)}>Añadir al carrito</button>
                // :
                // <button className="btn btn-block qyn-cart mt-3">Out of stock</button>
            )
        }
    }

    //**********************************  Adding ,update ***********************************//
    SaveDataToLocalStorage(addCart, value = 1, start) {
        if (start === true) addCart.isValidButton1 = false;
        value = Number(value);
        const { updateButton, render } = this.state;
        let withOutTokenProducts = []
        let withOutToken = localStorage.getItem("withOutToken");
        withOutToken = JSON.parse(withOutToken);
        if (withOutToken) withOutTokenProducts = withOutToken;
        let itemExist = _.filter(withOutTokenProducts, (object) => { return _.isEqual(object._id.toString(), addCart._id.toString()) })
        if (itemExist[0] && itemExist[0].productCount && itemExist[0].productCount === itemExist[0].stockQuantity && value === 1) {
            itemExist[0].isValidButton1 = true;
            localStorage.setItem('withOutToken', JSON.stringify(withOutTokenProducts));
            this.setState({ localProducts: withOutTokenProducts })
            this.props.withOutToken(withOutTokenProducts);
        }
        else if (itemExist && Array.isArray(itemExist) && itemExist.length > 0) {
            itemExist[0].isValidButton1 = false;
            // for updating value
            if (value === 0) {
                if (withOutToken && Array.isArray(withOutToken) && withOutToken.length) {
                    let itemIndex = _.findIndex(withOutToken, (object) => { return _.isEqual(object._id.toString(), addCart._id.toString()) });
                    if (itemIndex !== -1) {
                        withOutToken.splice(itemIndex, 1);
                    }
                    localStorage.setItem('withOutToken', JSON.stringify(withOutToken))
                    this.props.withOutToken(withOutToken)
                    this.getfromLocalstore();
                    if (updateButton) this.setState({ updateButton: false })
                }
            }
            if (updateButton) this.setState({ updateButton: false })
            //********************************* update value *********************************//
            if (value !== 1 && value !== 0) {
                itemExist[0].productCount = value;
                if (updateButton) this.setState({ updateButton: false });
            }
            //*********************************** increment **********************************//
            else if (value !== 0) {
                if (arguments.length === 2) itemExist[0].productCount = value;
                if (arguments.length === 1) itemExist[0].productCount += value;
            }
        }
        //***************** push to local storage at first time **********************************/
        else if (value !== 0) {
            window.fbq('track', 'Add to Cart',
                {
                    value: addCart.salePrice,
                    currency: 'ARS',
                    contents: [
                        {
                            id: addCart.SKU,
                            quantity: value
                        }],
                    content_type: 'product'
                }
            );
            window.gtag('config', 'G-WB2B2WMR7Q', {
                'page_title': 'Add to Cart',
            });
            addCart.productCount = value
            withOutTokenProducts.push(addCart);
        }
        localStorage.setItem('withOutToken', JSON.stringify(withOutTokenProducts));
        this.mostViewed(addCart._id);
        this.getfromLocalstore();
        this.props.withOutToken(withOutTokenProducts);
        this.setState({ render: !render })
    }

    //**********************************  dercrement **********************************//
    async decrementCart(addCart) {
        let withOutTokenProducts = []
        const withOutToken = localStorage.getItem("withOutToken")
        if (withOutToken) withOutTokenProducts = JSON.parse(withOutToken);
        let itemExist = _.findIndex(withOutTokenProducts, (object) => { return _.isEqual(object._id.toString(), addCart._id.toString()) })
        let existItemCount = _.filter(withOutTokenProducts, (object) => { return _.isEqual(object._id.toString(), addCart._id.toString()) })
        if (existItemCount[0].productCount <= (existItemCount[0].minQuantity > existItemCount[0].stockQuantity ? existItemCount[0].stockQuantity : existItemCount[0].minQuantity)) {
            withOutTokenProducts.splice(itemExist, 1);
        }
        else {
            existItemCount[0].productCount--;
            existItemCount[0].isValidButton1 = false;
        }
        await localStorage.setItem('withOutToken', JSON.stringify(withOutTokenProducts));
        this.props.withOutToken(withOutTokenProducts)
        this.getfromLocalstore();
        this.props.withOutToken(withOutTokenProducts)
        const { render } = this.setState;
        this.setState({ render: !render });
    }
    render() {
        let { data } = this.state;
        return (this.estimatedFunction(data));
    }
}

const mapStateToProps = state => ({
});

export default (connect(mapStateToProps, actions)(Button));