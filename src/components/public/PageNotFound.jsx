import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import PageNotFoundImage from '../../../src/assets/images/error-404.png';
import { withRouter } from 'react-router-dom';
class PageNotFound extends Component {
    constructor(props) {
        super(props)
        this.state = {}
    }
    render() {
        return (
            <div>
                <Helmet>
                    <meta charSet="utf-8" />
                    <title>página no encontrada</title>
                </Helmet>
                <div className="error-page container">
                    <div className="row align-items-center">
                        <div className="col-sm-6 error-img">
                            <img src={PageNotFoundImage} alt="error-404" />
                        </div>
                        <div className="col-sm-6 error-detail">
                            <h2>404</h2>
                            <h3>Página no encontrada</h3>
                            <p>Lo sentimos, no se pudo encontrar la página que solicitó. Regrese a la página de inicio o contáctenos en support@xyz.com</p>
                            <button className="btn btn-primary" onClick={() => this.props.history.push('/')}>Ir a inicio</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default withRouter(PageNotFound);
