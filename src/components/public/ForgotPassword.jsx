import React from 'react'
import CF from '../common/commonFuntions'; // COMMON FUNCTIONS
import Form from '../reusable/form';
import EM from '../common/errorMessages';

class ForgotPassword extends Form {
    state = {
        formData: {
            emailId: "",
        },
        errors: {},
        isFormValid: true,
    }


    /*******************  Forgot password  ******************/
    async doSubmit() {
        window.gtag('config', 'G-WB2B2WMR7Q', {
            'page_title': 'Forgot Password',
            'page_path': '/forgotPassword'
        });
        window.gtag('event', 'forgotPassword');
        window.fbq('track', 'forgotPassword',
        );
        let { emailId } = this.state.formData;
        const body = { emailId };
        const response = await CF.callApi('/user/forgotPassword', body, 'post');
        if (response.status === 1) {
            CF.showSuccessMessage(EM._205);
            this.props.history.push(`/login`);
        }
        else if (response.status === 0 && response.statusCode === 406) CF.showErrorMessage(EM._406);
    }

    render() {
        let { emailId } = this.state.formData;
        return (
            <div className="main">

                <div id="page-content-wrapper">
                    <form className="login-wrap" onSubmit={(event) => this.handleSubmit(event)}>
                        <h2 className="main-title mb-25">Se te olvidó tu contraseña</h2>
                        <p> Si olvidó su contraseña, ingrese el correo electrónico registrado.</p>
                        <div className="row">
                            {this.renderInput('col-12 p-0', true, 'emailId', 'text', emailId, 'Por favor introduzca su correo electrónico')}
                        </div>
                        {this.renderButton('btnbox d-flex justify-content-center mt-38 mb-50', 'btn btn-block btn-default', 'Enviar')}
                    </form>
                </div>
            </div>
        );
    }

}

export default ForgotPassword;