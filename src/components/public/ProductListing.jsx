import React, { Component } from 'react';
// import { withRouter } from "react-router-dom";
import { connect } from 'react-redux';
import * as actions from '../../actions';
import OwlCarousel from 'react-owl-carousel';
import { Select } from 'antd';
import 'antd/dist/antd.css';
import { Helmet } from "react-helmet";
import InputRange from 'react-input-range';
import _ from 'lodash';
import CF from '../common/commonFuntions'; // COMMON FUNCTIONS
// import { BeatLoader } from 'react-spinners';
import BreadCrumb from '../common/breadCrumb';
import CommonOwl from '../reusable/commonOwl';
// import moment from 'moment';
import tagIcon from '../../../src/assets/images/tag-icon.svg';
import $ from "jquery";
import AllCategory from '../reusable/allCategory';
import PageNotFound from './PageNotFound';
import Spinner from 'react-spinner-material';

class ProductListing extends Component {

	constructor(props) {
		window.fbq('track', 'Product SubCategory',
		);
		super(props);
		this.state = {
			loading: true,
			productListing: [],
			categoryName: '',
			subCategoryName: [],
			categoryId: '',
			brandArray: [],
			selectedBrands: [],
			categoryNamesArray: [],
			price: {
				min: 0,
				max: 1
			},
			maximumPriceRange: 1,
			minimumPriceRange: 0,
			_id: '',
			stock: false,
			sort: "",
			categoryImage: '',
			rating: '',
			subCategory: [],
			categorynames: [],
			parentCategory: '',
			productSlider: {
				autoWidth: true,
				margin: 20,
				loop: false,
				mouseDrag: true,
				items: 4,
				slideBy: 4,
				nav: true,
				responsive: {
					0: { mouseDrag: false, items: 1, nav: false },
					480: { items: 1, nav: false },
					600: { mouseDrag: false, items: 2, nav: false },
					899: { mouseDrag: false, items: 2, nav: false },
					1000: { mouseDrag: false, items: 3, nav: false, },
					1025: { mouseDrag: false, items: 3, nav: true, slideBy: 1 },
					1200: { mouseDrag: false, items: 3, nav: true, slideBy: 2 },
					1600: { mouseDrag: false, items: 4, nav: true, slideBy: 3 },
					1920: { mouseDrag: false, items: 4, nav: true, slideBy: 4 }
				}
			},
			productCatSlider: {
				margin: 10,
				loop: false,
				autoWidth: true,
				nav: true,
			},
			productCatSlider1: {
				margin: 25,
				loop: true,
				autoplay: true,
				animateOut: 'fadeOut',
				autoplayTimeout: 5000,
				autoWidth: false,
				nav: false,
				items: 1
			},
			// Sets up our initial state
			error: false,
			hasMore: true,
			isLoading: false,
			users: {},
			page: 1, pagesize: 3,
			discountLabel: [],
			brands: false,
			applyPrice: false,
			subCategories: false,
			sortValue: '',
			total: '',
			subcategoryName: [],
			message: '',
			url: '',
			urlField: '',
			allCat: false,
			allCategory: [],
			page1: 1,
			pagesize1: 12,
			categoryName1: '',
			productListingNoData: [],
			maxRangeName: '',
			pageFound: true,
			previousValue: 1
		};
		window.onscroll = () => {
			const {
				state: { error, isLoading, hasMore, allCat },
			} = this;
			// Bails early if:
			// * there's an error
			// * it's already loading
			// * there's nothing left to load
			if (error || isLoading || !hasMore) return;
			// Checks that the page has scrolled to the bottom
			// if (subCategoryName.length >= 8) {
			if ($(window).scrollTop() + window.innerHeight >= document.body.scrollHeight) {
				this.setState({ isLoading: true }, allCat === true ? () => this.getAllCategories() : () => this.getProductFilterApi())
			}
			if (document.documentElement.scrollTop + window.innerHeight === document.documentElement.scrollHeight) {
				this.setState({ isLoading: true }, allCat === true ? () => this.getAllCategories() : () => this.getProductFilterApi())
			}
			if (document.body.scrollTop + window.innerHeight === document.body.scrollHeight) {
				this.setState({ isLoading: true }, allCat === true ? () => this.getAllCategories() : () => this.getProductFilterApi())
			}
		}
		// };
	}

	componentDidMount() {
		window.scrollTo(0, 0);
		if ($(window).width() > 1200) {
			$(".main").removeClass('nav-wrap home-page');
		}
		else {
			$(".main").addClass('fixed-sidebar');
		}
		const url = this.props.match.url;
		this.setState({ url, allCat: false, categoryName1: this.props.location.state ? this.props.location.state.categoryName1 : '' }, () => this.getProductFilterApi())
		// if (this.props.getproductId1) this.setState({ isLoading: true }, () => this.getProductFilterApi())
		if (this.props.selectDropDown1) this.sortProducts(this.props.selectDropDown1)
		window.gtag('config', 'G-WB2B2WMR7Q', {
			'page_title': 'Product SubCategory ',
			'page_path': `${url}`
		});
		this.discountLabel();
	}


	componentWillReceiveProps(nextProps) {
		this.sortProducts(nextProps.selectDropDown1)
	}
	//************************** getting all categories and products ********************************//
	async getAllCategories() {
		this.setState({ allCat: true })
		const existedIds = []
		this.state.productListing && this.state.productListing.map(list => {
			list.products.map(prod => {
				existedIds.push(prod._id)
			})
		})
		const { price, sort, page1, pagesize1, brandArray, applyPrice } = this.state;
		let body = { 'page': page1, 'pagesize': pagesize1, existedIds };
		if (applyPrice) {
			if (price.max !== 1) {
				const minSalePrice = price.min;
				const maxSalePrice = price.max;
				body.salePrice = { minSalePrice, maxSalePrice }
			}
		}
		if (!_.isEmpty(brandArray)) {
			const brandName = _.map(_.filter(brandArray, (o) => { return o.checked }), object => { return object._id });
			if (!_.isEmpty(brandName)) body.brandName = brandName;
		}
		if (sort !== '') body.sort = { salePrice: Number(sort) };
		const response = await CF.callApi('/products/allCategoryProducts/' + this.props.match.params.customUrl, JSON.stringify(body), 'post');
		if (response.status === 1) {
			const { data } = response;
			this.setState({
				// lazy loading -- Start
				// //  data.subcategoryName.length
				hasMore: (page1 * pagesize1 < data.total),
				allCategory: data && data.categoryProducts && data.categoryProducts.products && [...this.state.allCategory, ...data.categoryProducts.products],
				page1: page1 + 1,
				pagesize1: 12,
				isLoading: false,
				total: data.total ? data.total : '',
			})
		}
	}

	// to get all the products 
	getProductFilterApi = async () => {
		this.setState({ previousValue: this.state.page })
		let categoryId = this.props.getproductId1;
		const { _id, brandArray, price, sort, page, pagesize, applyPrice, previousValue } = this.state;
		if (!_.isEmpty(_id)) categoryId = _id;
		const body = { categoryId, page, pagesize };
		if (applyPrice) {
			if (price.max !== 1) {
				const minSalePrice = price.min;
				const maxSalePrice = price.max;
				body.salePrice = { minSalePrice, maxSalePrice }
			}
		}
		if (!_.isEmpty(brandArray)) {
			const brandName = _.map(_.filter(brandArray, (o) => { return o.checked }), object => { return object._id });
			if (!_.isEmpty(brandName)) body.brandName = brandName;
		}
		if (sort !== '') body.sort = { salePrice: Number(sort) };
		const response = await CF.callApi('/products/productFilter/' + this.props.match.params.customUrl, JSON.stringify(body), 'post');
		if (response.status === 1) {
			const { data } = response;
			const minMaxPrices = {
				min: data.minRange,
				max: data.maxRange
			};
			const { maximumPriceRange, minimumPriceRange } = this.state;
			if (data.productDetails && data.productDetails.length !== 0 && (page === 1 || page !== previousValue)) {
				this.setState({
					hasMore: (page * pagesize < data.total),
					page: page + 1,
					pagesize: 3,
					productListing: [...this.state.productListing, ...data.productDetails],
					isLoading: false,
					pageFound: true,
					color: data && data.categoryName && data.categoryName.categoryName ? data.categoryName.color : '', categoryName: data && data.categoryName && data.categoryName.categoryName ? data.categoryName.categoryName : '',
					categoryImage: data && data.categoryName && data.categoryName.categoryImage ? data.categoryName.categoryImage : '',
					categoryId: data && data.categoryName && data.categoryName._id ? data.categoryName._id : '',
					subCategoryName: data.subcategoryName && Array.isArray(data.subcategoryName) && data.subcategoryName.length ? data.subcategoryName : [],
					total: data && data.total ? data.total : '',
					brandArray: this.state.brandArray && this.state.brandArray.length ? this.state.brandArray : data.brand,
					categoryNamesArray: data && data.categoryNamesArray ? data.categoryNamesArray : '',
					price: price.max === 1 ? minMaxPrices : price,
					maximumPriceRange: (maximumPriceRange === 1 || maximumPriceRange <= minMaxPrices.max) ? minMaxPrices.max : maximumPriceRange,
					minimumPriceRange: (minimumPriceRange === 0 || minimumPriceRange <= minMaxPrices.min) ? minMaxPrices.min : minimumPriceRange,
				});
			}
			else if (data.productDetails && data.productDetails.length === 0 && (page * pagesize <= data.total)) {
				this.setState({
					hasMore: (page * pagesize < data.total),
					page: page + 1,
					pagesize: 3,
					isLoading: false
				}, () => this.getProductFilterApi())
			}

			if (page * pagesize >= data.total) {
				this.getAllCategories()
			}
		}
		else if (response.status === 0 && response.message === "Category details are not found.") {
			this.setState({ isLoading: false, hasMore: false, message: response.message, allCat: false, pageFound: false });
		}
		else if (response.status === 0 && response.message !== "Category details are not found.") {
			this.setState({ isLoading: false, hasMore: false, message: response.message, allCat: false });
		}
	}

	getproducts(id, customUrl) {
		window.gtag('event', 'Products');
		this.props.getSubCat(id, this.state.categoryName)
		this.props.history.push({ pathname: `${this.state.url}/${customUrl}`, state: { customUrl: customUrl, categoryName1: this.state.categoryName1, categoryName2: this.state.categoryName } })
	}


	// for handling the price
	handleChangeForPrice(price) {
		// let { subCategoryName } = this.state;
		this.setState({ price, page: 1, pagesize: 3, productListing: [], page1: 1, pagesize1: 12, allCategory: [], applyPrice: true, isLoading: true }, () => this.getProductFilterApi())
	}

	// for selecting the brand 
	handleChangeForBrand(event, brand) {
		let { brandArray } = this.state;
		const index = brandArray.findIndex(b => b._id === brand._id);
		if (index > -1) brandArray[index].checked = event.target.checked;
		this.setState({ brandArray, page: 1, pagesize: 3, productListing: [], isLoading: true }, () => this.getProductFilterApi());
	}

	// for sorting the products
	sortProducts(value) {
		const { sort } = this.state;
		if (sort === value) return;
		this.setState({ sort: value, page: 1, pagesize: 3, productListing: [], isLoading: true }, () => this.getProductFilterApi());
	}

	//**************************************** discounts ******************************************//
	async discountLabel() {
		const response = await CF.callApi('/discount/getAllDiscounts', '', 'get', false, true);
		if (response.status === 1) {
			this.setState({ discountLabel: response.data })
		}
	}

	render() {
		// const { error, hasMore, isLoading } = this.state;
		const Option = Select.Option;
		const { error, hasMore, isLoading, categoryName, subCategoryName, productListing, loading, price, brandArray, sort, maximumPriceRange, brands, subCategories, minimumPriceRange, discountLabel, total, message, allCat, allCategory, categoryName1, pageFound } = this.state;
		let bread = this.state.url.split('/');

		return (
			<React.Fragment>

				{pageFound === true ?
					categoryName.length === 0 ?
						<div className="text-center page-loader">
							<Spinner size={70} spinnerColor={'#2472DC'} spinnerWidth={7} visible={true} />
							{/* <BeatLoader sizeUnit={"px"} size={20} color={'#2472DC'} loading={loading} /> */}
							{!loading ? <p2 className="text-center text-danger" colSpan="6">Ups! Algo salió mal</p2> : null}
						</div> :
						<div>
							<Helmet>
								<meta charSet="utf-8" />
								<title>{categoryName}</title>
								<meta name="description" content={categoryName}></meta>
							</Helmet>
							<div className="main fixed-sidebar mt-0">
								<div id="page-content-wrapper" className="page-content-wrapper product-listing-content filter-sidepanel">
									<div className="breadcrumb-wrap">
										<BreadCrumb
											breadCrums={true}
											bread={bread}
											categoryName1={categoryName1}
											categoryName2={categoryName}
											// categoryNamesArray={categoryNamesArray}
											sort={sort}
											isSort={true}
										// urlField={urlField}
										// sortProducts={this.sortProducts.bind(this)}
										/>
									</div>
									{/* Filter right sidenav */}
									<div className="filter-sidebar">
										<div className="filter-header">
											<button className="filter-btn btn" title="clear all" onClick={() => this.props.history.push('/')}>Limpiar todo</button>
											<button className="filter-btn btn" title="Done">Hecho</button>
										</div>
										{/* {subCategoryName && subCategoryName.length > 0 && brandArray && brandArray.length > 0 ? */}
										{total > 0 ?
											<div id="accordion" className="category-nav">
												<div className="card">
													<div className="card-header">
														<a className="card-link collapsed" data-toggle="collapse" href="#shortbyNav" aria-expanded="false">
															Ordenar por
            </a>
													</div>
													<div id="shortbyNav" className="collapse" data-parent="#accordion" style={{}}>
														{/* <label htmlFor="" className="col-form-label text-right" > Ordenar: </label> */}
														<Select className="dropdown-select" value={sort}
															onChange={(value) => this.sortProducts(value)}>
															<Option value="">Relevancia</Option>
															<Option value={1}>Precio de menor a mayor</Option>
															<Option value={-1}>Precio mayor a menor</Option>
														</Select>
													</div>
												</div>
												<div className="card">
													<div className="card-header">
														<a className="card-link collapsed" data-toggle="collapse" href="#categoryNav" aria-expanded="false">
															{categoryName}
														</a>
													</div>
													<div id="categoryNav" className="collapse" data-parent="#accordion" style={{}}>
														{subCategories === false ?
															<ul className="card-body">
																{(subCategoryName) ? (
																	subCategoryName.map((subcategory, key) => {
																		if (subcategory.availableProducts > 0 && key < 15) {
																			return (<li key={key} onClick={() => this.getproducts(subcategory._id, subcategory.customUrl)}>
																				<a >{subcategory.categoryName}</a></li>)
																		}
																	})) : null
																}
																{subCategoryName && subCategoryName.length > 15 &&
																	<a className='btn nav-more-btn' onClick={() => this.setState({ subCategories: true })}>MÁS</a>
																}
															</ul> :
															<ul className="card-body">
																{(subCategoryName) ? (
																	subCategoryName.map((subcategory, key) => {
																		if (subcategory.availableProducts > 0) {
																			return (<li key={key} onClick={() => this.getproducts(subcategory._id, subcategory.customUrl)}>
																				<a >{subcategory.categoryName}</a></li>)
																		}
																	})) : null
																}
																<a className='btn nav-more-btn' onClick={() => this.setState({ subCategories: false })}>MENOS</a>
															</ul>}
													</div>
												</div>

												{maximumPriceRange === minimumPriceRange ? '' :
													<div className="card custom-input-range">
														<div className="card-header">
															<a className="collapsed card-link" data-toggle="collapse" href="#storeNav" aria-expanded="false">
																Rango
														</a>
														</div>
														<div id="storeNav" className="collapse" data-parent="#accordion" style={{}}>
															<div className="card-body radiobtn-nav-list">
																<InputRange
																	className="form-control"
																	id="typeinp"
																	type="range"
																	maxValue={maximumPriceRange}
																	minValue={minimumPriceRange}
																	value={price}
																	onChange={(value) => this.setState({ price: value })}
																	onChangeComplete={(value) => this.handleChangeForPrice(value)}
																/>
															</div>
														</div>
													</div>}

												{brandArray && brandArray.length > 1 &&
													<div className="card">
														<div className="card-header">
															<a className="collapsed card-link" data-toggle="collapse" href="#deliveryNav" aria-expanded="false">
																Marca
            </a>
														</div>
														<div id="deliveryNav" className="collapse" data-parent="#accordion" style={{}}>


															<div className="card-body radiobtn-nav-list">
																{(brands === false) ?
																	<div>
																		{brandArray.map((brand, key) => {
																			brand.checked = brand.checked ? brand.checked : false;
																			if (key < 5) {
																				return (<div className='custom-control custom-checkbox' key={key}>
																					<input type="checkbox" className="custom-control-input" name="brand" id={key} checked={brand.checked} onChange={(e) => this.handleChangeForBrand(e, brand)} />
																					<label className="custom-control-label" htmlFor={key}>	{brand.brandName}</label>
																				</div>)
																			}
																		})}
																		{brandArray && brandArray.length > 5 ? <a className='btn nav-more-btn' onClick={() => this.setState({ brands: true })}>MÁS</a> : ''}
																	</div>
																	: ''}
																{(brands === true) ?
																	<div>
																		{brandArray.map((brand, key) => {
																			brand.checked = brand.checked ? brand.checked : false;
																			return (<div className='custom-control custom-checkbox' key={key}>
																				<input type="checkbox" className="custom-control-input" name="brand" id={key} checked={brand.checked} onChange={(e) => this.handleChangeForBrand(e, brand)} />
																				<label className="custom-control-label" htmlFor={key}>	{brand.brandName}</label>
																			</div>)
																		})}
																		<a className='btn nav-more-btn' onClick={() => this.setState({ brands: false })}>MENOS</a>
																	</div>
																	: ''}

															</div>
														</div>
													</div>}

											</div>
											: ""}
									</div>
									{/* Breadcrumb Wrap End */}
									{(subCategoryName.length === 0) ? "" : (
										<div className="categoryList-mobile">
											<OwlCarousel className="product-cat-slider" {...this.state.productCatSlider}>
												{subCategoryName.map((subcategory, key) => {
													if (subcategory.availableProducts > 0) {
														return (<div className="item" key={key} onClick={() => this.getproducts(subcategory._id, subcategory.customUrl)}>
															<a title={subcategory.categoryName}>{subcategory.categoryName}</a>
														</div>)
													}
												})}
											</OwlCarousel>
										</div>
									)}
									{/* contentWrap Start */}
									<div className="contentWrap">
										{/* Product category Sidebar Start */}
										<div className="category-sidebar">
											<div id="accordion" className="category-nav">
												<div className="card">
													<div className="card-header">
														<a className="card-link collapsed" data-toggle="collapse" href="#collapseOne">{categoryName}</a>
													</div>
													<div id="collapseOne" className="collapse show" data-parent="#accordion">
														{subCategories === false ?
															<ul className="card-body">
																{(subCategoryName) ? (
																	subCategoryName.map((subcategory, key) => {
																		if (subcategory.availableProducts > 0 && key < 15) {
																			return (<li key={key} onClick={() => this.getproducts(subcategory._id, subcategory.customUrl)}>
																				<a>{subcategory.categoryName}</a></li>)
																		}
																	})) : null
																}
																{subCategoryName && subCategoryName.length > 15 &&
																	<a className='btn nav-more-btn' onClick={() => this.setState({ subCategories: true })}>MÁS</a>
																}
															</ul> :
															<ul className="card-body">
																{(subCategoryName) ? (
																	subCategoryName.map((subcategory, key) => {
																		if (subcategory.availableProducts > 0) {
																			return (<li key={key} onClick={() => this.getproducts(subcategory._id, subcategory.customUrl)}>
																				<a >{subcategory.categoryName}</a></li>)
																		}
																	})) : null
																}
																<a className='btn nav-more-btn' onClick={() => this.setState({ subCategories: false })}>MENOS</a>
															</ul>}
													</div>
												</div>
												{/* subCategoryName && subCategoryName.length > 0 && */}

												<React.Fragment>
													{maximumPriceRange === minimumPriceRange ? '' :
														<div className="card mb-3">
															<div className="card-header">
																<a className="collapsed card-link" data-toggle="collapse" href="#collapseTwo">Rango</a>
															</div>
															<div className="form-group  my-3">
																<div className="col-sm-10 pl-4">
																	<InputRange
																		className="form-control"
																		id="typeinp"
																		type="range"
																		maxValue={maximumPriceRange}
																		minValue={minimumPriceRange}
																		value={price}
																		onChange={(value) => this.setState({ price: value })}
																		onChangeComplete={(value) => this.handleChangeForPrice(value)}
																	/>
																</div>
															</div>
														</div>}


													{brandArray && brandArray.length > 1 &&
														<div className="card">
															<div className="card-header">
																<a className="card-link collapsed" data-toggle="collapse" href="#brandNav" aria-expanded="false">Marca</a>
															</div>
															<div id="brandNav" className="collapse show" data-parent="#accordion" >
																<ul className="card-body checkbox-nav-list">
																	{(brands === false) ?
																		<div>
																			{brandArray.map((brand, key) => {
																				brand.checked = brand.checked ? brand.checked : false;
																				if (key < 5) {
																					return (<div className='custom-control custom-checkbox' key={key}>
																						<input type="checkbox" className="custom-control-input" name="brand" id={key} checked={brand.checked} onChange={(e) => this.handleChangeForBrand(e, brand)} />
																						<label className="custom-control-label" htmlFor={key}>	{brand.brandName}</label>
																					</div>)
																				}
																			})}
																			{brandArray && brandArray.length > 5 ? <a className='btn nav-more-btn' onClick={() => this.setState({ brands: true })}>MÁS</a> : ''}
																		</div>
																		: ''}

																	{(brands === true) ?
																		<div>
																			{brandArray.map((brand, key) => {
																				brand.checked = brand.checked ? brand.checked : false;
																				return (<div className='custom-control custom-checkbox' key={key}>
																					<input type="checkbox" className="custom-control-input" name="brand" id={key} checked={brand.checked} onChange={(e) => this.handleChangeForBrand(e, brand)} />
																					<label className="custom-control-label" htmlFor={key}>	{brand.brandName}</label>

																				</div>)

																			})}
																			<a className='btn nav-more-btn' onClick={() => this.setState({ brands: false })}>MENOS</a>
																		</div>
																		: ''}

																</ul>
															</div>
														</div>}
												</React.Fragment>
											</div>
										</div>
										{/* Product category Sidebar Start */}
										{/* Product Listing Wrap Start */}
										<div className="product-listing-wrap">
											{discountLabel && Array.isArray(discountLabel) && discountLabel.length ?
												<div className="couponcodeStripe">
													<img src={tagIcon} alt="Coupon" className="img-fluid coupon-img" />
													<OwlCarousel className="product-cat-slider" {...this.state.productCatSlider1}>
														{discountLabel && Array.isArray(discountLabel) && discountLabel.length ?
															discountLabel.map((each, key) => {
																let endDate = each && each.validity.endDate ? each.validity.endDate : ""
																let discount = each && each.discount ? each.discount : ''
																let onpurchaseAmount = each && each.minAmount ? each.minAmount : ""
																return (
																	<div className="coupon-text" key={key}>
																		<h4>Descuento del {discount}% comprando más de ${onpurchaseAmount}</h4>
																		{/* <p>Expira {moment(endDate).format('ll')}.</p> */}
																	</div>
																)
															}) : ''}
													</OwlCarousel>
												</div> : ""}
											{/* {total > 0 ? */}
											{productListing &&

												< CommonOwl
													productListing={productListing}
													homePage={false}
													productSlider={this.state.productSlider}
													subCategoryName={subCategoryName}
													onClick={this.getproducts.bind(this)}
													products={true}
													message={message}
													categoryName1={categoryName1}
													categoryName2={categoryName}
													url={this.state.url}
													productDetail={this.state.url}
												/>}
											{allCat === true && <AllCategory allCategory={allCategory} categoryName={categoryName} categoryName1={categoryName1}
												categoryName2={categoryName}
												url={this.state.url} />}

											{isLoading && <div className="text-center product-loader">
												<Spinner size={70} spinnerColor={'#2472DC'} spinnerWidth={7} visible={true} />
												{/* <Loader type="Oval" color={'#2472DC'} height={70} width={120} /> */}
												{/* <ClipLoader sizeUnit={"px"} size={50} color={'#2472DC'} border-width="20" /> */}
											</div>}
											{<div style={{ color: '#900' }}>{error}</div>}
											{!hasMore && <div className="text-center"></div>}
											<div className="page-footer"></div>
										</div>
									</div>
								</div>
							</div>
						</div> : <PageNotFound />}
			</React.Fragment>
		);
	}
}
const mapStateToProps = state => ({
	getproductId1: state.user.getproductId,
	selectDropDown1: state.user.selectDropDown
});
export default (connect(mapStateToProps, actions)(ProductListing));