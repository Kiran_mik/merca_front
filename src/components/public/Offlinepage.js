import React, { Component } from 'react';
import offlinepage from '../../../src/assets/images/offline-page.png';
// import offline from '../../assets/images/offline-page.png';

class OfflinePage extends Component {
    state = {}
    render() {
        return (
            <div className="offline-outer offline-msg">
                <div className="offline-inner">
                    <img src={offlinepage} alt="offline" title="offline" className="img-fluid" />
                    <h4>Estas desconectado</h4>
                    <p>No se encontró internet o conexión lenta.</p>
                    <p>Comprueba tu conexión o vuelve a intentarlo</p>
                    <button className="btn btn-primary" onClick={() => window.location.reload(true)}>Recargar</button>
                </div>
            </div>
        );
    }
}

export default OfflinePage;