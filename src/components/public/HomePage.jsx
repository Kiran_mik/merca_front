import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { IMAGE_URL } from '../../config/configs';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import OwlCarousel from 'react-owl-carousel';
import _ from 'lodash'
import { Helmet } from "react-helmet";
// for jquery -- Start
import $ from "jquery";
import CommonOwl from '../reusable/commonOwl';
import noImage from '../../../src/assets/images/noimage.jpg';
import CF from '../common/commonFuntions';
import { BeatLoader } from 'react-spinners';
// import { LazyLoadImage } from 'react-lazy-load-image-component';
// import LoadingBar from 'react-top-loading-bar';
window.jQuery = $;
window.$ = $;
global.jQuery = $;
// for jquery -- End

class HomePage extends Component {
    constructor(props) {
        window.gtag('config', 'G-WB2B2WMR7Q', {
            'page_title': 'homepage',
            'page_path': ''
        });
        window.fbq('track', 'homepage',
        );
        super(props)
        this.state = {
            banner: [],
            shopByCategory: [],
            productData: [],
            // styles        
            bannerSlider: {
                loop: true,
                responsiveClass: true,
                nav: true,
                autoplay: true,
                autoplayTimeout: 5000,
                responsive: {
                    0: { items: 1, nav: false },
                    600: { items: 1, nav: false },
                    1000: { items: 1, nav: false },
                    1025: { items: 1 }
                }
            },
            categorySlider: {
                merge: true,
                margin: 20,
                loop: false,
                mouseDrag: true,
                slideBy: 2,
                nav: true,
                responsiveClass: true,
                responsive: {
                    0: { mouseDrag: false, items: 2, nav: false, autoWidth: false, },
                    375: { items: 2, nav: false, autoWidth: false, },
                    600: { mouseDrag: false, items: 3, nav: false, autoWidth: false, },
                    1000: { mouseDrag: false, items: 3, nav: false, autoWidth: false, },
                    1025: { mouseDrag: false, items: 3, nav: true, autoWidth: true, },
                    1366: { mouseDrag: false, items: 3, slideBy: 1, nav: true, autoWidth: true, },
                    1920: { mouseDrag: false, items: 3, slideBy: 3, nav: true, autoWidth: true, }
                }
            },
            productSlider: {
                merge: true,
                margin: 20,
                loop: false,
                mouseDrag: true,
                items: 4,
                slideBy: 4,
                mergeFit: true,
                nav: true,
                responsive: {
                    0: { mouseDrag: false, items: 2, nav: false, autoWidth: false, margin: 0 },
                    480: { items: 2, nav: false, autoWidth: false, margin: 0 },
                    600: { mouseDrag: false, items: 3, nav: false, autoWidth: false, margin: 0 },
                    1000: { mouseDrag: false, items: 3, nav: false, autoWidth: false, margin: 20 },
                    1025: { mouseDrag: false, items: 3, slideBy: 3, nav: true, autoWidth: true, margin: 20 },
                    1366: { mouseDrag: false, items: 3, slideBy: 2, nav: true, autoWidth: true, },
                    1920: { mouseDrag: false, items: 4, slideBy: 4, nav: true, autoWidth: true, margin: 20 }
                    // 1440: { mouseDrag: false, items: 6,autoWidth: false,slideBy: 'page', }
                }
            },
            loading: true,
            loadingBarProgress: 0
        }
    }

    // complete = () => {
    //     this.setState({ loadingBarProgress: 100 });
    // };
    // onLoaderFinished = () => {
    //     this.setState({ loadingBarProgress: 0 });
    // };

    componentDidMount() {
        window.scrollTo(0, 0);
        if ($(window).width() > 1200)
            $(".main").addClass('nav-wrap home-page');
        this.bannerPage();
    }

    async bannerPage() {
        let { loading } = this.state
        const response = await CF.callApi('/home/homePage', {}, 'post');
        if (response.status === 1) {
            // 
            const { data } = response;
            const { shopByCategory } = data;
            const { sectionData: banner } = data.banner;
            const productData = this.getProductListForRender(data)
            this.setState({ productData, banner, shopByCategory })
            // this.complete();
        }
        else {
            this.setTimeout(() => {
                loading = !loading
            }, 10000);
        }
    }

    getProductListForRender(data) {
        const localData = localStorage.getItem("withOutToken")
        const localDataIds = (!_.isEmpty(localData) && localData.length > 0) ? JSON.parse(localData).map(list => list._id) : [];
        if (localDataIds.length > 0) {
            Object.keys(data).map(keyName => {
                if (keyName !== "banner" && keyName !== "shopByCategory") {
                    let productList = data[keyName]
                    localDataIds.map(lid => {
                        let index = productList.findIndex(ele => ele._id.toString() === lid.toString());
                        if (index >= -1) productList.splice(index, 1);
                        return lid;   // this line is not useful 
                    })
                    data[keyName] = productList;
                    return null;
                }
                return keyName;  // this line is not useful 
            })
        }
        return data;
    }
    commonPage = (name) => {
        window.gtag('event', 'products');
        this.props.subPage(name)
        this.props.history.push('/products')
    }

    getProducts = (id) => {
        if (this.props.product) this.props.product(id);
        this.props.getproductIdCat(id)
    }

    render() {
        const { banner, shopByCategory, loading, productData, bannerSlider, productSlider, categorySlider } = this.state;
        return (<React.Fragment>
            {shopByCategory.length === 0 ?
                <div className="text-center page-loader">
                    <BeatLoader sizeUnit={"px"} size={20} color={'#2472DC'} loading={loading} />
                    {!loading ? <p2 className="text-center text-danger" colSpan="6">Ups! Algo salió mal</p2> : null}
                </div> :
                <div>
                    {/* {console.log("calleld", this.state.loadingBarProgress)} */}
                    {/* <LoadingBar
                    progress={this.state.loadingBarProgress}
                    height={3}
                    color='blue'
                    onLoaderFinished={() => this.onLoaderFinished()}
                /> */}
                    <Helmet>
                        <meta charSet="utf-8" />
                        <title>MercadoMayorista</title>
                        <meta name="description" content="MercadoMayorista"></meta>
                    </Helmet>
                    <div className="main nav-wrap home-page mt-0">
                        <div id="page-content-wrapper">
                            {/* for banner */}
                            <Banner data={banner} classes={bannerSlider} />
                            {/* Shop by Category */}
                            <ShopByCategory data={shopByCategory} classes={categorySlider} onClick={this.getProducts} />
                            {/* For Products */}
                            {$(window).width() > 600 ?
                                <CommonOwl productListing={productData} homePage={true} productSlider={productSlider} onClick={this.commonPage} products={false} url={''} /> :
                                <CommonOwl productListing={productData} homePage={true} productSlider={null} onClick={this.commonPage} products={false} url={''} />}
                        </div>
                    </div>
                </div>}
            {/* } */}
        </React.Fragment>);
    }
}
const mapStateToProps = state => ({
    subPage: state.user.subpage,
});

export default (connect(mapStateToProps, actions)(HomePage));

// For Banner
const Banner = ({ data, classes }) => {
    return (<React.Fragment>
        <section className="banner-slider-section section">
            <div className="banner-slider">
                {data.length !== 0 &&
                    <OwlCarousel  {...classes}>
                        {data.map((each, i) => {
                            const photo = each.bannerImage ? IMAGE_URL + each.bannerImage : noImage;
                            return (<Link to={each.bannerUrl} key={i}>
                                <div className="item" >
                                    <img src={photo} alt='banner' width="1500px" height="350px" />
                                    <div className={each.position === "right" ? "banner-captionRight" : "banner-caption"} style={{ background: each.color ? each.color : "blue" }}>
                                        <h3>{each.title}</h3>
                                        <p> {each.description}</p>
                                        <div className="shopnow-link"> Compra ahora <span className="fa fa-play-circle"></span></div>
                                    </div>
                                </div>
                            </Link>)
                        })}
                    </OwlCarousel>
                }
            </div>
            <div className="banner-slider mobile-banner">
                {data.length !== 0 &&
                    <OwlCarousel  {...classes}>
                        {data.map((each, i) => {
                            const photo = each.mobileBannerImage ? IMAGE_URL + each.mobileBannerImage : noImage;
                            return (<Link to={each.bannerUrl} key={i} >
                                <div className="item">
                                    <img src={photo} alt='banner' width="1500px" height="350px" />
                                    <div className={each.position === "right" ? "banner-captionRight" : "banner-caption"} style={{ background: each.color ? each.color : "blue" }}>
                                        <h3 className="mobile-banner-title">{each.title}</h3>
                                        <p> {each.description}</p>
                                        <div className="shopnow-link">Compra ahora <span className="fa fa-play-circle"></span></div>
                                    </div>
                                </div>
                            </Link>)
                        })}
                    </OwlCarousel>
                }
            </div>
        </section>
    </React.Fragment >);
}
// For Shop By Category
const ShopByCategory = ({ data, classes, onClick }) => {
    return (
        <section className="category-slider-section section">
            {data.length !== 0 &&
                <React.Fragment>
                    <h3 className="sub-title pl-0">Comprar por categoría</h3>
                    <div className="shop-cat-slider">
                        <OwlCarousel {...classes} >
                            {data.map((each, i) => {
                                if (each.count > 0) {
                                    const url = each && each.image ? IMAGE_URL + (each.image) : noImage;
                                    return (<div className="item" key={i}>
                                        <Link to={`/${each.customUrl}`} onClick={() => onClick(each._id)}>
                                            <div className="categoryBox">
                                                <div className="img-box">
                                                    {/* <LazyLoadImage
                                                        className="img-fluid"
                                                        alt={each.categoryName}
                                                        height={'275px'}
                                                        delayTime={100}
                                                        src={url} // use normal <img> attributes as props
                                                        width={"400px"} /> */}
                                                    <img src={url} alt={each.categoryName} width="400px" height="275px" className="img-fluid" />
                                                </div>
                                                <div className="cat-title blueBg" style={{ background: each.color ? each.color : '' }}>
                                                    <h5>{each.categoryName}</h5>
                                                </div>
                                            </div>
                                        </Link>
                                    </div>)
                                }
                            })}
                        </OwlCarousel>
                    </div>
                </React.Fragment>}
        </section>
    );
}
