import React, { Component } from 'react';
import { API_URL, IMAGE_URL } from '../../config/configs';
// import StarRatingComponent from 'react-star-rating-component';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { withRouter } from "react-router-dom";
import * as actions from '../../actions';
import axios from 'axios';
import swal from 'sweetalert';
import OwlCarousel from 'react-owl-carousel';
import _ from 'lodash';
import CF from '../common/commonFuntions'; // COMMON FUNCTIONS
import EM from '../common/errorMessages';  // ERROR MESSAGE
// import minusIcon from '../../../src/assets/images/remove-icn.svg';
// import plusIcon from '../../../src/assets/images/add-icn.svg';
// import noImage from '../../../src/assets/images/noimage.jpg';
import deliveryTruck from '../../../src/assets/images/delivery-truck.svg';
// import tagIcon from '../../../src/assets/images/tag-icon.svg'
import $ from "jquery";
// import moment from 'moment';

window.jQuery = $;
window.$ = $;
global.jQuery = $;

class Cart extends Component {
	constructor(props) {
		window.gtag('config', 'G-WB2B2WMR7Q', {
			'page_title': 'Cart',
			'page_path': '/cart'
		});
		super(props);
		this.state = {
			products: [],
			button: true,
			count: false,
			name: localStorage.getItem('fname'),
			localProducts: [],
			countValue: "",
			productid: '',
			total1: '',
			cartCount: '0',
			recommendedItems: [],
			quantity: 1,
			productSlider: {
				margin: 10,
				loop: false,
				mouseDrag: true,
				items: 6,
				slideBy: 3,
				autoWidth: true,
				nav: true,
				responsive: {
					0: { mouseDrag: false, items: 1, nav: false },
					480: { items: 1, nav: false },
					600: { mouseDrag: false, items: 2, nav: false },
					1000: { mouseDrag: false, items: 3, nav: false },
					1025: { mouseDrag: false, items: 3, nav: true, slideBy: 2 },
					1366: { mouseDrag: false, items: 4, slideBy: 3, nav: true, }
				}
			},
			productCatSlider: {
				margin: 0,
				loop: true,
				autoplay: true,
				autoplayTimeout: 3000,
				autoWidth: false,
				nav: false,
				items: 1
			},
			render: true,
			updateButton: false,
			data: {},
			dummyQuantity: '',
			errors: {
				quantity: true,
				stockQuantity: true
			},
			isQuantityFormValid: true,
			previousValue: '',
			CalproductCount: [],
			CalsalePrice: [],
			amount: '',
			discount: '',
			discountLabel: [],
			buttonEnter: false,
			Max: '',
			removed: false,
			minOrderAmount: '',
			availability: '',
			loadingBarProgress: 0

		}
	};

	componentDidMount() {
		window.scrollTo(0, 0);
		if ($(window).width() > 1200) {
			$(".main").removeClass('nav-wrap home-page');
		}
		else {
			$(".main").addClass('fixed-sidebar');
		}
		let withOutTokenProducts = localStorage.getItem("withOutToken")
		withOutTokenProducts = JSON.parse(withOutTokenProducts)
		if (withOutTokenProducts) {
			this.setState({ localProducts: withOutTokenProducts })
			let contents = [];
			withOutTokenProducts.map(each => { contents.push({ 'id': each.SKU, 'quantity': each.productCount }) })
			window.fbq('track', 'Cart',
				{
					currency: 'ARS',
					contents,
					content_type: 'product'
				}
			);
			this.getDiscounts();
			// this.complete();
		}

		this.minOrderAmount();
		this.stockAvailableOrNot();
	}
	//************************ Updates ****************************//
	componentWillReceiveProps = (nextProps) => {
		let withOutTokenProducts = localStorage.getItem("withOutToken")
		withOutTokenProducts = JSON.parse(withOutTokenProducts)
		if (withOutTokenProducts) {
			this.setState({ localProducts: withOutTokenProducts })
			this.recommendedItems();
			this.getDiscounts();
		}
	}
	//************************ Empty ALL ****************************//
	removeAllProductsFromLocalStorage = () => {
		window.gtag('event', 'EmptyCart');
		localStorage.setItem('withOutToken', '[]');
		let withOutTokenProducts = localStorage.getItem("withOutToken");
		withOutTokenProducts = JSON.parse(withOutTokenProducts);
		if (withOutTokenProducts) {
			this.setState({ localProducts: withOutTokenProducts })
			this.props.withOutToken(withOutTokenProducts);
		}
		localStorage.removeItem('totalAmount');
		localStorage.removeItem('discount');
	}

	//************************ To Back to ProductDetail Page ****************************//
	getSingleProduct = (customUrl) => {
		this.props.getSingleProduct(customUrl)
		this.props.history.push('/productDetails?id=' + customUrl)
	}
	//******************************** Recommended Items API ****************************//
	recommendedItems = () => {
		window.gtag('event', 'RecommendedItems');
		let withOutTokenProducts = localStorage.getItem("withOutToken")
		withOutTokenProducts = JSON.parse(withOutTokenProducts)
		var productsArray = withOutTokenProducts.map(each => each._id)
		var data = { productsArray: productsArray, page: 1, pagesize: 12 };
		axios({
			method: 'post',
			url: API_URL + '/user/recommendedItems',
			headers: {
				'Content-Type': 'application/json',
			},
			data
		}).then(response => {
			this.setState({ recommendedItems: response.data.data })
		})
	}
	//************************ remove item and Pop UP ****************************//
	remove(id) {
		swal({
			title: EM.REMOVE_ITEM,
			icon: "warning",
			buttons: true,
			dangerMode: true,
			className: 'custom-toaster'
		})
			.then(async (willDelete) => {
				if (willDelete) {
					var withOutToken = localStorage.getItem('withOutToken');
					if (withOutToken) {
						window.gtag('event', 'Item Deleted from cart');
						var withOutTokenProducts = JSON.parse(withOutToken)
						withOutTokenProducts.splice(id, 1);
						localStorage.setItem('withOutToken', JSON.stringify(withOutTokenProducts));
						this.props.withOutToken(withOutTokenProducts)
						this.setState({ localProducts: withOutTokenProducts })
					}
				}
			})

			.catch(error => {
				console.log("error is " + JSON.stringify(error));
			});
	}

	//******************** Input change starts ********************************* */
	previousValue(currentObject) {
		const { localProducts, previousValue, updateButton } = this.state;
		// let withOutTokenProducts = []
		// const withOutToken = localStorage.getItem("withOutToken")
		// if (withOutToken) withOutTokenProducts = JSON.parse(withOutToken);
		// var updateShippingdata = _.map(withOutTokenProducts, obj => (obj.specified = true))
		currentObject.specified = false
		currentObject.productCount = previousValue;
		this.setState({ localProducts })
		localStorage.setItem('withOutToken', JSON.stringify(localProducts));
		if (updateButton) this.setState({ updateButton: false })
	}

	handleClick = (object) => {
		// console.log("object", object)
		// let withOutTokenProducts = []
		// const withOutToken = localStorage.getItem("withOutToken")
		// if (withOutToken) withOutTokenProducts = JSON.parse(withOutToken);
		// var updateShippingdata = _.map(withOutTokenProducts, obj => (obj.specified = false))
		object.specified = true
		object.isValidButton = false
		this.setState({ previousValue: object.productCount, dummyQuantity: object.productCount, buttonEnter: false })
		this.changeUpdateButton()
	}
	handleChangeForQuantity(event, currentObject) {
		const { errors } = this.state;
		if (event.target.value > currentObject.stockQuantity) {
			if ((event.target.value)) errors.stockQuantity = EM.INVALID_STOCKQUANTITY;
			else errors.stockQuantity = true;
		}
		if (_.isEmpty(event.target.value)) {
			if (_.isEmpty(event.target.value)) errors.quantity = EM.INVALID_QUANTITY;
			else errors.quantity = true;
		}
		if (CF.allowNumbers(event.target.value) || event.target.value === '') {
			currentObject.productCount = event.target.value
			this.setState({ dummyQuantity: currentObject.productCount, buttonEnter: false })
			return;
		}
		if (!CF.allowNumbers(event.target.value)) return;
	}

	changeUpdateButton() {
		const { updateButton } = this.state;
		if (!updateButton) this.setState({ updateButton: !updateButton })
	}

	handleKeyPress(event, obj, val) {
		window.gtag('event', 'Quantity Selected');
		if ((event.key === 'Enter' && event.target.value <= obj.stockQuantity && event.target.value >= obj.minQuantity) || (event.target.value <= obj.stockQuantity && val === "blurevent" && event.target.value >= obj.minQuantity)) {
			let value = event.target.value
			this.SaveDataToLocalStorage(obj, value)
			this.setState({ buttonEnter: true })
		}
	}

	onBlur_ = (event, obj) => {
		if (event.target.value <= obj.stockQuantity && event.target.value >= obj.minQuantity) {
			let value = event.target.value
			this.SaveDataToLocalStorage(obj, value)
			this.setState({ buttonEnter: true })
		}
	}
	//********************************* Input change Ends **********************************//
	//**********************************  Adding ,update ***********************************//
	async SaveDataToLocalStorage(addCart, value = 1) {
		let withOutTokenProducts = []
		const withOutToken = localStorage.getItem("withOutToken")
		if (withOutToken) withOutTokenProducts = JSON.parse(withOutToken);
		let itemExist = _.filter(withOutTokenProducts, (object) => { return _.isEqual(object._id.toString(), addCart._id.toString()) })
		if (itemExist[0].productCount === itemExist[0].stockQuantity && value === 1) {
			itemExist[0].isValidButton = true;
			await localStorage.setItem('withOutToken', JSON.stringify(withOutTokenProducts));
			this.setState({ localProducts: withOutTokenProducts })
			this.props.withOutToken(withOutTokenProducts);
		}
		else if (itemExist && value) {
			value = Number(value);
			const { updateButton, render } = this.state;
			if (itemExist && Array.isArray(itemExist) && itemExist.length > 0) {
				// for updating value
				if (value === 0) {
					if (withOutToken && Array.isArray(withOutToken) && withOutToken.length) {
						let itemIndex = _.findIndex(withOutToken, (object) => { return _.isEqual(object._id.toString(), addCart._id.toString()) })
						if (itemIndex !== -1) {
							withOutToken.splice(itemIndex, 1);
						}
						localStorage.setItem('withOutToken', JSON.stringify(withOutToken))
						this.props.withOutToken(withOutToken)
						this.setState({ localProducts: withOutToken })
						if (updateButton) this.setState({ updateButton: false })
					}
					return null;
				}
				if (updateButton) this.setState({ updateButton: false })
				if (value !== 1 && value !== 0) {
					itemExist[0].productCount = value;
				}
				else if (value !== 0) {
					if (arguments.length === 2) itemExist[0].productCount = value;
					if (arguments.length === 1) itemExist[0].productCount += value;
				}
			} else if (value !== 0) {
				addCart.productCount = value
				withOutTokenProducts.push(addCart);
			}
			itemExist[0].isValidButton = false;
			await localStorage.setItem('withOutToken', JSON.stringify(withOutTokenProducts));
			this.setState({ localProducts: withOutTokenProducts, render: !render })
			this.props.withOutToken(withOutTokenProducts);
		}
	}
	//*****************  Function for buttons **********************************/
	SaveDataToLocalStorage1(addCart) {
		let withOutTokenProducts = []
		var withOutToken = localStorage.getItem("withOutToken")
		if (withOutToken) {
			withOutTokenProducts = JSON.parse(withOutToken)
		}
		addCart['productCount'] = 1;
		withOutTokenProducts.push(addCart);
		swal("Artículo agregado al carrito", "", 'success');
		this.setState({ localProducts: withOutTokenProducts, Max: '' });
		localStorage.setItem('withOutToken', JSON.stringify(withOutTokenProducts));
		this.props.withOutToken(withOutTokenProducts)
	}

	estimatedFunction(product_object) {
		const { localProducts, errors, dummyQuantity } = this.state;
		var isItemaddedtocart = _.filter(localProducts, object => object._id.toString() === product_object._id.toString())
		if (isItemaddedtocart.length > 0) {
			return (
				<>
					{/* && isItemaddedtocart[0].stockQuantity > 0 */}
					{product_object.specified ?

						<span className="btn btn-block qyn-cart">
							<button className="float-left" onClick={() => this.previousValue(isItemaddedtocart[0])} >Cancelar</button>
							<span className="cart-feild">
								{(isItemaddedtocart[0].productCount > product_object.stockQuantity) ? <span className="error-msg text-danger invalid-error-msg"
								>{`Cantidad máxima ${product_object.stockQuantity} unid`}</span> : ""}
								{(isItemaddedtocart[0].productCount === "") ? <span className="error-msg text-danger invalid-error-msg"
								>{errors.quantity}</span> : (isItemaddedtocart[0].productCount >= isItemaddedtocart[0].minQuantity) ? '' : <span className="error-msg text-danger invalid-error-msg"
								>{`Cantidad mínima ${isItemaddedtocart[0].minQuantity} unid`}</span>}
								<input
									className="form-control mr-2"
									type="number"
									name="quantity"
									value={isItemaddedtocart[0].productCount}
									onChange={(event) => this.handleChangeForQuantity(event, isItemaddedtocart[0])}
									onClick={() => this.handleClick(isItemaddedtocart[0])}
									onKeyPress={(event) => this.handleKeyPress(event, isItemaddedtocart[0])}
									onBlur={(event) => this.handleKeyPress(event, isItemaddedtocart[0], "blurevent")}
								/>
								{isItemaddedtocart[0].increasedStock ? <span className="error-msg text-success stock-error-msg"
								>{isItemaddedtocart[0].stockQuantity + '' + ' Unidades disponibles'}</span> : ""}
							</span>
							{
								(isItemaddedtocart[0].productCount === "" || isItemaddedtocart[0].productCount > product_object.stockQuantity
									|| isItemaddedtocart[0].productCount < isItemaddedtocart[0].minQuantity) ?
									<button className="blur_image">Actualizar</button> :
									<button className="float-right" onClick={() => this.SaveDataToLocalStorage(isItemaddedtocart[0], dummyQuantity)}>Actualizar</button>
							}
						</span> :

						<span className="btn btn-block qyn-cart add-to-cart-btn">
							{/* {isItemaddedtocart[0].stockQuantity > 0 ? */}
							<React.Fragment>
								<button className="decrement-btn float-left" onClick={() => this.decrementCart(isItemaddedtocart[0])}>
									<span className="fa fa-minus-circle"></span>
									{/* <img src={minusIcon} alt="decrement" /> */}
								</button>
								<span className="cart-feild">
									{(isItemaddedtocart[0].isValidButton === true) ? <span className="error-msg text-danger invalid-error-msg"
									>{`Cantidad máxima ${product_object.stockQuantity}  unid.`}</span> : ""}
									{(isItemaddedtocart[0].productCount === "") ? <span className="error-msg text-danger invalid-error-msg"
									>{errors.quantity}</span> : ""}

									<input
										className="form-control mr-2"
										type="number"
										name="quantity"
										value={isItemaddedtocart[0].productCount}
										onChange={(event) => this.handleChangeForQuantity(event, isItemaddedtocart[0])}
										onClick={() => this.handleClick(isItemaddedtocart[0])}
										onKeyPress={(event) => this.handleKeyPress(event, isItemaddedtocart[0])}
										onBlur={(event) => this.handleKeyPress(event, isItemaddedtocart[0], "blurevent")}
										readOnly
									/>
									{isItemaddedtocart[0].increasedStock ? <span className="error-msg text-success stock-error-msg"
									>{isItemaddedtocart[0].stockQuantity + '' + 'Stock disponible'}</span> : ""}
								</span>
								{((isItemaddedtocart[0].productCount > product_object.stockQuantity || isItemaddedtocart[0].isValidButton) ?
									<button className="increment-btn float-right blur_image " >
										<span className="fa fa-plus-circle"></span>
										{/* <img src={plusIcon} alt="increment" /> */}
									</button> :
									<button className="increment-btn float-right" onClick={() => this.SaveDataToLocalStorage(isItemaddedtocart[0])}>
										<span className="fa fa-plus-circle"></span>
										{/* <img src={plusIcon} alt="increment" /> */}
									</button>
								)
								}
							</React.Fragment>
						</span>
					}
				</>
			)
		}

	}

	//********************** if stock is not available after user add into cart ************************************//
	stockAvailableOrNot = async () => {
		let withOutTokenProducts = localStorage.getItem("withOutToken")
		withOutTokenProducts = JSON.parse(withOutTokenProducts)
		let CalproductCount = withOutTokenProducts && Array.isArray(withOutTokenProducts) && withOutTokenProducts.length ? withOutTokenProducts.map(list => list._id) : [];
		let body = { "productIds": CalproductCount }
		const response = await CF.callApi('/products/cartProductDetails', body, 'post', false, true);
		if (response.status === 1) {
			const result = withOutTokenProducts.filter(local => {
				response.data.product.filter(cart => {
					if (local._id === cart._id) {
						if (local.productCount > cart.stockQuantity) {
							local.stockQuantity = cart.publish === true ? cart.stockQuantity : 0;
							local.productCount = cart.stockQuantity;
							local.increasedStock = false;
						}
						if (cart.stockQuantity > local.stockQuantity) {
							local.stockQuantity = cart.publish === true ? cart.stockQuantity : 0;
							local.increasedStock = true;
							local.isValidButton = false;
						}
						else {
							local.increasedStock = false
							local.stockQuantity = cart.publish === true ? cart.stockQuantity : 0;
						}
					}
				});
				if (local.stockQuantity) return local;
			});
			localStorage.setItem('withOutToken', JSON.stringify(result));
			this.setState({ localProducts: result, availability: response.data && response.data.orderDelivery ? response.data.orderDelivery.orderDelivery : '' })
			this.props.withOutToken(result);
		}
		else if (response.message === "Invalid token.") {
			CF.showErrorMessage("Sesión expirada.");
		}
		return;
	}

	stockAvailableOrNotCheckout = async (withOutToken) => {
		let productIds = withOutToken && Array.isArray(withOutToken) && withOutToken.length ? withOutToken.map(list => list._id) : [];
		let body = { productIds }
		const response = await CF.callApi('/products/cartProductDetails', body, 'post', false, true);
		if (response.status === 1) {
			// console.log("response", response)
			const { data } = response;
			// Check if any new stock is availble
			let newStock = [];
			withOutToken.filter(local => {
				data.product.filter(cart => {
					if (local._id.toString() === cart._id.toString()) {
						if (cart.stockQuantity > local.stockQuantity) newStock.push(cart);
					}
				});
				return local;
			});
			if (!_.isEmpty(newStock)) {
				// Update the stock quantity
				// console.log("new stock is not empty")
				this.stockAvailableOrNot();
				return true;
			}
			return false;
		}
		else if (response.message === "Invalid token.") {
			CF.showErrorMessage("Sesión expirada.");
		}
	}
	//********************************** delete,decrement ***********************************//
	async decrementCart(addCart) {
		let withOutTokenProducts = []
		const withOutToken = localStorage.getItem("withOutToken")
		if (withOutToken) withOutTokenProducts = JSON.parse(withOutToken);
		let itemExist = _.findIndex(withOutTokenProducts, (object) => { return _.isEqual(object._id.toString(), addCart._id.toString()) })
		let existItemCount = _.filter(withOutTokenProducts, (object) => { return _.isEqual(object._id.toString(), addCart._id.toString()) })
		if (existItemCount[0].productCount <= existItemCount[0].minQuantity) this.remove(itemExist)
		else {
			existItemCount[0].productCount--;
			existItemCount[0].isValidButton = false;
		}
		await localStorage.setItem('withOutToken', JSON.stringify(withOutTokenProducts));
		this.props.withOutToken(withOutTokenProducts)
		this.setState({ localProducts: withOutTokenProducts })
		const { render } = this.setState;
		this.setState({ render: !render });
	}
	//********************************** Recommeded Items update in list ***********************************//
	getProductListForRender(products) {
		const localData = localStorage.getItem("withOutToken")
		const localDataIds = (localData && localData.length > 0) ? JSON.parse(localData).map(list => list._id) : ""
		let productList = products && products.length ? products.filter(list => list) : [];
		if (localDataIds && localDataIds.length > 0) {
			localDataIds.filter(lid => {
				let index = productList.findIndex(product => product._id === lid);
				if (index >= -1) productList.splice(index, 1)
			})
		}
		products = productList;
		return products;
	}

	//************************ getting all discounts ****************************/
	async getDiscounts() {
		let withOutTokenProducts = localStorage.getItem("withOutToken")
		withOutTokenProducts = JSON.parse(withOutTokenProducts)
		let CalproductCount = withOutTokenProducts && Array.isArray(withOutTokenProducts) && withOutTokenProducts.length ? withOutTokenProducts.map(list => {
			if (list.stockQuantity) return list.productCount
		}) : ""
		let CalsalePrice = withOutTokenProducts && Array.isArray(withOutTokenProducts) && withOutTokenProducts.length ? withOutTokenProducts.map(list => { return list.salePrice }) : "";
		let amount = 0;
		if (!_.isEmpty(CalproductCount)) {
			amount = CalproductCount.map(e => Number.isInteger(e) ? e : 0).reduce((a, c, i) => a + c * CalsalePrice[i], 0);
		}
		this.setState({ amount })
		const body = { "amount": amount }
		const response = await CF.callApi('/discount/getDiscount', body, 'post', false, true);
		if (response.status === 1) {
			let { data } = response;
			let { discount } = data
			this.setState({ discount })
		}
		else this.setState({ discount: 0 })
	}
	//********************** discounts ************************************//
	async discountLabel() {
		const response = await CF.callApi('/discount/getAllDiscounts', '', 'get', false, true);
		if (response.status === 1) {
			this.setState({ discountLabel: response.data })
		}
	}

	//********************** min-orderAmount ************************************//
	async minOrderAmount() {
		const response = await CF.callApi('/order/minOrderAmount', '', 'get', false, true);
		if (response.status === 1) {
			this.setState({ minOrderAmount: response.data.minOrderAmount })
		}
	}

	addWeekdays = (date, days) => {
		date.setDate(date.getDate());
		var counter = 0;
		if (days > 0) {
			while (counter < days) {
				date.setDate(date.getDate() + 1); // Add a day to get the date tomorrow
				var check = date.getDay(); // turns the date into a number (0 to 6)
				if (check === 0 || check === 6) {
					// Do nothing it's the weekend (0=Sun & 6=Sat)
				}
				else {
					counter++;  // It's a weekday so increase the counter
				}
			}
		}
		return date;
	}
	render() {
		let { recommendedItems, localProducts, discount, amount, availability } = this.state;
		var date = this.addWeekdays(new Date(), availability);
		const options = { year: 'numeric', month: 'long', day: 'numeric' };
		let discountApplied = (amount * discount) / 100;
		recommendedItems = this.getProductListForRender(recommendedItems)
		return (
			<div>
				<div id="cart-content" className="cart-content">
					<div className="container">
						<div className="selected-items-list">
							<div className="justify-content-between">
								<div className="title-subheader">
									{localProducts.length > 0 ? <h2 className="sub-title d-inline">Carrito</h2> : null}
									<div>
										{localProducts.length > 0 &&
											<div className="text-right d-inline-block mr-3 mobile-cart">
												{discountApplied === 0 ? '' : <h6 className="d-inline-block pr-3">Descuento : <span> ${discountApplied.toFixed(2)}</span></h6>}
												<h6 className="d-inline-block">Subtotal <span>${(amount - discountApplied).toFixed(2)}</span></h6>
											</div>}
										{localProducts.length > 0 ?
											<div className="cart-totalBox text-right d-inline-block">
												<a onClick={this.checkoutCart} className="btn btn-primary" title="Go to Checkout">Ir a la caja</a>
											</div> : null}
									</div>
									<div className="mobile-cart-view">
										{localProducts.length > 0 &&
											<div className="text-right d-inline-block mr-3">
												{discountApplied === 0 ? '' : <h6 className="d-inline-block pr-3">Descuento : <span> ${discountApplied.toFixed(2)}</span></h6>}
												<h6 className="d-inline-block">Subtotal <span>${(amount - discountApplied).toFixed(2)}</span></h6>
											</div>}
									</div>
								</div>

							</div>
							<div className="card mt-10">
								{localProducts.length > 0 &&
									<div className="card-header">
										<span className="greentext delivery-title mb-10">
											{availability &&
												<span><img src={deliveryTruck} alt="Delivery Time" width="22px" height="14px" className="mr-2" />
													{('Será entregado a su transporte el') + " " + (date && date.toLocaleDateString('es-eS', options))}
													{/* (date && (moment(date).format('D MMM YY'))) */}
												</span>}
										</span>
										<div>
											<button className="btn btn-block qyn-cart empty-cart-btn" onClick={() => this.emptyCart()}>Vaciar Carrito</button>
										</div>
									</div>}
								<div className="card-body">
									<ul className="cart-items-list">
										{(localProducts) && localProducts.length > 0 ? (
											localProducts.map((each, key) => {
												let salePrice = each && each.salePrice ? each.salePrice : "no price";
												let photo = each && each.productImage ? IMAGE_URL + each.productImage.slice(0, -4) + "-th.jpg" : IMAGE_URL + "product.jpg";
												let productName = each && each.productName ? each.productName : "no name";
												let productNameNoImg = each && each.productName ? (each.productName.substring(0, 5) + '...') : "no name";
												let url = photo;
												return (
													<li key={key}>
														<Link to={{ pathname: `/categorias/${each.customUrl}`, state: { url: '' } }}>
															<div className="item-imgbox">
																<img src={url} alt={productNameNoImg} width="93px" height="93px" className="img-fluid" />
															</div>
														</Link>
														<div className="item-info-wrapper">
															<div className="item-info">
																<div>
																	<Link to={`/categorias/${each.customUrl}`}><h6> {productName}</h6></Link>
																	<span>${salePrice}</span>
																</div>
																<div className="action-text desktop-addto-cart-btn">
																	<button className="btn bluetext" type="button" title="Eliminar" onClick={() => { this.remove(key) }} >Eliminar</button>
																</div>
															</div>
															<div className="item-quantityBox desktop-addto-cart-btn">
																<div className="form-group quantitybox">
																	{this.estimatedFunction(each)}
																</div>
															</div>
															{each && each.stockQuantity > 0 &&

																<div className="priceBox desktop-addto-cart-btn">
																	<h6>${(salePrice * (each.productCount))}</h6>
																</div>}
														</div>
														<div className="mobile-add-to-cart">

															<div className="item-quantityBox p-0">
																<div className="form-group quantitybox">
																	{this.estimatedFunction(each)}
																</div>
															</div>
															{each && each.stockQuantity > 0 ?
																<div className="priceBox">
																	<h6>${(salePrice * (each.productCount))}</h6>
																</div> : ''}
														</div>
													</li>)
											})) : <div className="p-3 text-center">
												<h5>Tu carrito esta vacío</h5>
												<button className="text-center btn btn-primary"
													onClick={() => this.props.history.replace('/')}
												>Regresar a Compras</button>
												<br />

											</div>
										}
									</ul>
								</div>
							</div>
							<div className="row">
								{localProducts.length > 0 &&
									<div className="col-md-12 text-right">
										{discountApplied === 0 ? '' : <h6 className="d-sm-inline-block d-xs-block mt-15">Descuento : <span> ${discountApplied.toFixed(2)}</span></h6>}
										<h6 className="d-sm-inline-block d-xs-block mt-15 pl-3">Subtotal <span>${(amount - discountApplied).toFixed(2)}</span></h6>
									</div>}
								{localProducts.length > 0 ?
									<div className="cart-totalBox col-md-12 text-right">
										<a onClick={() => this.checkoutCart()} className="btn btn-primary" title="Go to Checkout">Ir a la caja</a>
									</div> : null}
							</div>
						</div>
					</div>
					{/************************* product Item Slider Start *******************************/}
					<section className="product-slider-section section product-slider-part">
						{recommendedItems && recommendedItems.length === 0 ? "" :
							(<div className="container">
								<h3 className="sub-title">Recomendado para ti</h3>
								<div className="mt-15">
									<OwlCarousel {...this.state.productSlider} >
										{recommendedItems.map((each, id) => {
											if (id < 12) {
												let salePrice = each && each.salePrice ? each.salePrice : "no price";
												let regularPrice = each && each.price ? each.price : "no price";
												let photo = each && each.productImage ? IMAGE_URL + each.productImage.slice(0, -4) + "-sm.jpg" : IMAGE_URL + "product.jpg";
												return (
													<div className={each.stockQuantity > 0 ? "product-itemBox" : "product-itemBox blur_image"} key={id}>
														<Link to={`/categorias/${each.customUrl}`}>
															<div className="imgBox">
																<img src={photo} alt={each.productName} className="img-fluid" />
															</div>
														</Link>
														<div className="pro-details">
															<Link to={{ pathname: `/categorias/${each.customUrl}`, state: { url: '' } }}>
																<h6 className="product-name">{each.productName}	</h6>
															</Link>
															<h4>${salePrice} <strike>${regularPrice}</strike></h4>
															<div className="info">
																{/* <h6 className="delivery-offter-text">
																	<span className="greentext">Entrega</span>gratis
                                    								</h6> */}
															</div>
															{each.stockQuantity > 0 ?
																<button className="btn btn-block mt-3"
																	onClick={() => this.SaveDataToLocalStorage1(each)}
																>Añadir al carrito</button>
																: <button className="btn btn-block mt-3"
																>Agotado</button>}
														</div>
													</div>
												)
											}
										})}
										<div className="item">
											{recommendedItems.length >= 6 ?
												<div className="product-itemBox-more">
													<Link to={'/products'} onClick={() => this.commonPage("Recommended Items")} className="btn moreBtn more-product-btn"><span>›</span> Más</Link>
												</div>
												: null}
										</div>
									</OwlCarousel>
								</div>
							</div>
							)}
					</section>
				</div>
			</div >
		);
	}
	commonPage = (name) => {
		this.props.subPage(name)
		this.props.history.push('/products')
	}

	emptyCart = () => {
		swal({
			title: EM.EMPTY_CART,
			icon: "warning",
			buttons: true,
			dangerMode: true,
		})
			.then(async (willDelete) => {
				if (willDelete) this.removeAllProductsFromLocalStorage();
			})
			.catch(error => {
				console.log("error is " + JSON.stringify(error));
			});
	}

	checkoutCart = async () => {
		let { minOrderAmount, amount } = this.state
		var sepratepathId = window.location.pathname.split("/")
		let token = localStorage.getItem("token");
		let withOutToken = localStorage.getItem("withOutToken");
		withOutToken = JSON.parse(withOutToken)
		if (token && withOutToken && withOutToken.length > 0 && amount >= minOrderAmount) {
			// Show pop-up when stock is available
			let checkNewItems = await this.stockAvailableOrNotCheckout(withOutToken);
			if (checkNewItems) {
				// CF.showInfoMessage("New stock is Aailable");
				setTimeout(() => {
					this.props.history.push('/checkOut');
					checkNewItems = false;
				}, 3000)
			}
			else {
				this.props.history.push('/checkOut');
			}
		}
		else if (withOutToken && withOutToken.length > 0 && amount < minOrderAmount) {
			// for conforming :: minOrderAmount 
			swal({
				title: `Para ir a la caja, el valor mínimo del carrito debe ser $${minOrderAmount}`,
				icon: "warning",
				dangerMode: true,
			})
		}
		else if (!token) {
			// for conforming :: Start
			swal({
				title: EM.PLEASE_SIGN_IN,
				icon: "warning",
				buttons: true,
				dangerMode: true,
				className: 'custom-toaster'
			})
				.then(async (willDelete) => {
					if (willDelete) this.props.history.push(`/login${"?redirectPage=" + sepratepathId[1]}`);
				})
				.catch(error => {
					console.log("error is " + JSON.stringify(error));
				});
			// for conforming :: End
		}
		else {
			swal("Tu carrito esta vacío", "", 'warning')
		}
	}
}

const mapStateToProps = state => ({
	productId1: state.user.productId,
	countValue: state.user.withOutToken,
	subPage: state.user.subpage
});

export default withRouter(connect(mapStateToProps, actions)(Cart));
