
import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import { withRouter, Link } from "react-router-dom";
// import { ClipLoader } from 'react-spinners';
import CF from '../common/commonFuntions'; // COMMON FUNCTIONS
import { Select } from 'antd';
import ProductListing from '../reusable/productListing';
import { API_URL } from '../../config/configs';
import axios from 'axios';
import moment from 'moment';
import tagIcon from '../../../src/assets/images/tag-icon.svg';
import OwlCarousel from 'react-owl-carousel';
import Spinner from 'react-spinner-material';

import $ from "jquery";
window.jQuery = $;
window.$ = $;
global.jQuery = $;

class HomePageProducts extends Component {
    constructor(props) {
        window.gtag('config', 'G-WB2B2WMR7Q', {
            'page_title': 'HomePage-Products',
            'page_path': '/products'
        });
        super(props);
        this.state = {
            productListing: {},
            categoryName: '',
            subcategoryName: '',
            discountLabel: [],
            products: '',
            subCatName: '',
            quantity: 1,
            catName: '',
            banner: [],
            recentlyAdded: [],
            shopByCategory: [],
            bestSeller: [],
            featuredProducts: [],
            rating: "",
            productsData: [],
            productCatSlider: {
                margin: 20,
                loop: true,
                autoplay: true,
                animateOut: 'fadeOut',
                autoplayTimeout: 5000,
                autoWidth: false,
                nav: false,
                items: 1
            },
            sort: '',
            // Sets up our initial state
            error: false,
            hasMore: true,
            isLoading: false,
            users: {},
            page: 1,
            pagesize: 12,
            message: '',
            loadingBarProgress: 0
            // recommendedItems:[]
        };
        window.onscroll = () => {
            const {
                loadProducts,
                state: { error, isLoading, hasMore },
            } = this;
            // Bails early if:
            // * there's an error
            // * it's already loading
            // * there's nothing left to load
            if (error || isLoading || !hasMore) return;
            // Checks that the page has scrolled to the bottom
            if ($(window).scrollTop() + window.innerHeight >= document.body.scrollHeight) { loadProducts(); }
            if (document.documentElement.scrollTop + window.innerHeight === document.documentElement.scrollHeight) { loadProducts(); }
            if (document.body.scrollTop + window.innerHeight === document.body.scrollHeight) { loadProducts(); }
        };
    };

    componentDidMount() {
        window.scrollTo(0, 0);
        if ($(window).width() > 1200) {
            $(".main").removeClass('nav-wrap home-page');
        }
        else {
            $(".main").addClass('fixed-sidebar');
        }

        this.setState({ categoryName: this.props.subpage })
        if (this.props.subpage === "Recommended Items") this.setState({ isLoading: true }, () => this.recommendedItems())
        else if (this.props.subpage !== "Recommended Items") this.setState({ isLoading: true }, () => this.bannerPage())
        this.discountLabel()
    }


    getSingleProduct = (customUrl) => {
        this.props.getSingleProduct(customUrl)
        this.props.history.push('/product?id=' + customUrl)
    }

    loadProducts = async () => {
        let { categoryName } = this.state;
        if (categoryName === "Recommended Items") this.setState({ isLoading: true }, () => this.recommendedItems());
        else if (categoryName !== "Recommended Items") this.setState({ isLoading: true }, () => this.bannerPage());
    }

    recommendedItems = () => {
        var { sort, page, pagesize } = this.state;
        let withOutTokenProducts = localStorage.getItem("withOutToken")
        withOutTokenProducts = JSON.parse(withOutTokenProducts)
        var productsArray = withOutTokenProducts.map(each => each._id)
        var data = { productsArray: productsArray, page, pagesize };
        if (sort !== '') data.sort = { salePrice: Number(sort) };
        axios({
            method: 'post',
            url: API_URL + '/user/recommendedItems',
            headers: {
                'Content-Type': 'application/json',
            },
            data
        }).then(response => {
            const { productsData, categoryName } = this.state;
            if (response.data.status === 1) {
                if (categoryName === "Recommended Items") {
                    this.setState({
                        hasMore: (page < (response.data.data).length),
                        productsData: [...productsData, ...response.data.data],
                        isLoading: false,
                        page: page + 1,
                        pagesize: 12,
                    })
                }
            }
            else if (response.data.status === 0) {
                this.setState({ isLoading: false, hasMore: false, message: response.message });
            }
        })
    }
    bannerPage() {
        var { sort, page, pagesize } = this.state;
        var dataa = { page, pagesize }
        if (sort !== '') dataa.sort = { salePrice: Number(sort) };
        var body = dataa
        axios({
            "method": "post",
            url: API_URL + '/home/homePage',
            headers: {
                "Content-Type": "application/json"
            },
            data: JSON.stringify(body)
        })
            .then(res => {
                const { productsData, categoryName } = this.state;
                let { data } = res.data;
                if (res.data.status === 1) {

                    if (categoryName) {
                        // setInterval(() => {
                        this.setState({
                            hasMore: (page < data[categoryName].length),
                            productsData: [...productsData, ...data[categoryName]],
                            isLoading: false,
                            page: page + 1,
                            pagesize: 12,
                        })
                        // }, 1000000)
                    }
                }
                else if (res.status === 0) {
                    this.setState({ isLoading: false, hasMore: false, message: res.message });
                }
            })
            .catch(err => console.log('err', err))
    }


    sortProducts(value) {
        const { sort, categoryName } = this.state;
        if (sort === value) return;
        if (categoryName === "Recommended Items") this.setState({ sort: value, page: 1, pagesize: 12, productsData: [] }, () => this.recommendedItems());
        else this.setState({ sort: value, page: 1, pagesize: 12, productsData: [] }, () => this.bannerPage());
    }
    //********************** discounts ************************************/
    async discountLabel() {
        const response = await CF.callApi('/discount/getAllDiscounts', '', 'get', false, true);
        if (response.status === 1) {
            this.setState({ discountLabel: response.data })
        }
    }

    render() {
        let Option = Select.Option;
        const { error, hasMore, isLoading, categoryName, sort, productsData, discountLabel, message } = this.state;
        return (
            <div>
                <div id="page-content-wrapper" className="page-content-wrapper product-listing-content product-listinview">
                    <div className="breadcrumb-wrap">
                        <nav aria-label="breadcrumb" className="mobile-d-none">
                            {this.props.subpage ?
                                <ol className="breadcrumb">
                                    <li className="breadcrumb-item"><Link to={'/'} >Inicio</Link></li>
                                    <li className="breadcrumb-item active">{(categoryName === 'recentlyAdded' && 'Agregados Recientemente' || categoryName === 'bestSeller' && 'Más Vendidos' || categoryName === 'featuredProducts' && 'Productos Recomendados' || categoryName === 'Recommended Items' && 'Recomendado para ti')}</li>
                                </ol> : null
                            }
                        </nav>

                        <div className="selectdropdownBox">
                            <div className="select-Box mobile-d-none">
                                <label htmlFor="" className="col-form-label text-right" > Ordenar: </label>
                                <Select className="dropdown-select" value={sort}
                                    onChange={(value) => this.sortProducts(value)}>
                                    <Option value="">Relevancia</Option>
                                    <Option value={1}>Precio de menor a mayor</Option>
                                    <Option value={-1}>Precio mayor a menor</Option>
                                </Select>
                            </div>
                            <button className="filter-btn btn mobile-d-block" title="Filter"> Filtrar </button>
                            {/* <p>sailaja</p> */}

                        </div>
                        {/* Filter right sidenav */}
                        <div className="filter-sidebar">
                            <div className="filter-header">
                                <button className="filter-btn btn" title="clear all" onClick={() => this.props.history.push('/')}>Limpiar todo</button>
                                <button className="filter-btn btn" title="Done">Hecho</button>
                            </div>
                            {/* {subCategoryName && subCategoryName.length > 0 && brandArray && brandArray.length > 0 ? */}
                            {this.props.subpage &&
                                <div id="accordion" className="category-nav">
                                    <div className="card">
                                        <div className="card-header">
                                            <a className="card-link collapsed" data-toggle="collapse" href="#shortbyNav" aria-expanded="false">
                                                Ordenar por</a>
                                        </div>
                                        <div id="shortbyNav" className="collapse" data-parent="#accordion" style={{}}>
                                            {/* <label htmlFor="" className="col-form-label text-right" > Ordenar: </label> */}
                                            <Select className="dropdown-select" value={sort}
                                                onChange={(value) => this.sortProducts(value)}>
                                                <Option value="">Relevancia</Option>
                                                <Option value={1}>Precio de menor a mayor</Option>
                                                <Option value={-1}>Precio mayor a menor</Option>
                                            </Select>
                                        </div>
                                    </div>
                                </div>}
                        </div>
                    </div>
                    <div className="contentWrap">
                        {!this.props.subpage ? "no products" :
                            <div className="product-listing-wrap single-product-listing product-all-item">
                                {discountLabel && Array.isArray(discountLabel) && discountLabel.length ?
                                    <div className="couponcodeStripe">
                                        <img src={tagIcon} alt="Coupon" className="img-fluid coupon-img" />
                                        <OwlCarousel className="product-cat-slider" {...this.state.productCatSlider}>
                                            {discountLabel && Array.isArray(discountLabel) && discountLabel.length ?
                                                discountLabel.map((each, key) => {
                                                    let endDate = each && each.validity.endDate ? each.validity.endDate : ""
                                                    let discount = each && each.discount ? each.discount : ''
                                                    let onpurchaseAmount = each && each.minAmount ? each.minAmount : ""
                                                    return (
                                                        <div className="coupon-text" key={key}>
                                                            <h4>Descuento del {discount}% comprando más de ${onpurchaseAmount}</h4>
                                                            {/* <p>Expira {moment(endDate).format('ll')}.</p> */}
                                                        </div>
                                                    )
                                                }) : ''}
                                        </OwlCarousel>
                                    </div> : ""}
                                <ProductListing productListing={productsData} msg={message} productSubCat={`/${(categoryName === 'recentlyAdded' && 'Agregados Recientemente' || categoryName === 'bestSeller' && 'Más Vendidos' || categoryName === 'featuredProducts' && 'Productos Recomendados' || categoryName === 'Recommended Items' && 'Recomendado para ti')}`} />
                            </div>}
                        {isLoading && <div className="text-center product-loader">
                            <Spinner size={70} spinnerColor={'#2472DC'} spinnerWidth={7} visible={true} />
                            {/* <ClipLoader sizeUnit={"px"} size={50} color={'#2472DC'} border-width="20" /> */}
                        </div>}
                        {<div style={{ color: '#900' }}>{error}</div>}
                        {!hasMore && <div className="text-center"></div>}
                        <div className="page-footer"></div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    subpage: state.user.subpage
});

export default (connect(mapStateToProps, actions)(HomePageProducts));