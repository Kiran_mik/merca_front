import { Link } from 'react-router-dom';
import queryString from 'query-string';
import React from 'react';
import _ from 'lodash';
import Form from '../reusable/form';
import CF from '../common/commonFuntions'; // COMMON FUNCTIONS
import EM from '../common/errorMessages';
import swal from "sweetalert";
import $ from "jquery";
// import LoadingBar from 'react-top-loading-bar';
window.jQuery = $;
window.$ = $;
global.jQuery = $;
class Login extends Form {

    state = {
        formData: {
            emailId: '',
            password: '',
            rememberMe: false,
        },
        errors: {},
        isFormValid: true,
    };

    async componentDidMount() {
        window.gtag('config', 'G-WB2B2WMR7Q', {
            'page_title': 'Login',
            'page_path': '/login'
        });
        window.fbq('track', 'Login',
        );
        if ($(window).width() > 1200) {
            $(".main").removeClass('nav-wrap home-page');
        }
        else {
            $(".main").addClass('fixed-sidebar');
        }
        const loggedin = await CF.getItem('token');
        if (!_.isEmpty(loggedin)) return this.props.history.push('/');
        let rememberMe = await CF.getItem('rememberMe');
        if (rememberMe === 'true') {
            const emailId = await CF.getItem('email');
            const password = await CF.getItem('password');
            if (emailId && password) {
                let formData = { emailId, password: atob(password), rememberMe: true };
                this.setState({ formData });
            }
        }
    }
    // complete = () => {
    //     this.setState({ loadingBarProgress: 100 });
    // };
    // onLoaderFinished = () => {
    //     this.setState({ loadingBarProgress: 0 });
    // };
    /*******************  For submitting the Form : End *******************/
    async doSubmit() {
        window.gtag('event', 'login');
        const { formData } = this.state;
        const { emailId, password, rememberMe } = formData;
        const body = { emailId, password };
        const response = await CF.callApi('/user/login', body, 'post');
        if (response.status === 1) {
            const { data } = response;
            CF.showSuccessMessage(EM._201);
            if (rememberMe) {
                CF.setItem('email', emailId)
                CF.setItem('password', btoa(password))
                CF.setItem('rememberMe', rememberMe)
            } else {
                let localStorageEmail = CF.getItem('email');
                if (localStorageEmail === emailId) CF.removeItem(['email', 'password', 'rememberMe']);
            }
            CF.setItem('token', data.token);
            CF.setItem('email', emailId)
            CF.setItem('fname', data.firstName);
            CF.setItem('photo', data.photo ? data.photo : "");
            // For redirecting to the previous page
            const url = this.props.location.search;
            const { redirectPage } = queryString.parse(url);
            let bread = redirectPage && redirectPage.split('/');
            if (!_.isEmpty(redirectPage) && redirectPage !== 'login' && redirectPage !== 'forgotPassword' && redirectPage !== 'register') return this.props.history.replace({ pathname: `/${redirectPage}`, state: { categoryName1: bread[0], categoryName2: bread[1], categoryName3: bread[2], categoryName4: bread[3], categoryName5: bread[4] } });

            this.props.history.replace('/');
        }
        else if (response.statusCode === 413 && response.status === 0) CF.showWarningMessage(EM._413)
        else if (response.statusCode === 410 && response.status === 0) CF.showWarningMessage(EM._410)
        else if (response.statusCode === 414 && response.status === 0) {
            swal({
                title: EM._414,
                icon: "warning",
                button: true,
                dangerMode: true,
                className: 'custom-toaster'
            })
        }
    }

    render() {
        let { formData } = this.state;
        const { emailId, password, rememberMe } = formData;
        return (
            <div>
                <div className='page-wrap'>
                    {/* Main content  -- start*/}
                    <div className='main mt-0'>
                        {/* Content -- starts */}
                        <div id='page-content-wrapper'>
                            {/* Form -- start */}
                            <form className='login-wrap' onSubmit={(e) => this.handleSubmit(e)}>
                                <h2 className='main-title mb-25'>Clientes Registrados</h2>
                                <p> Si tiene una cuenta, inicie sesión con su dirección de correo electrónico.</p>
                                <div className='row'>
                                    {this.renderInput('col-12 p-0', false, 'emailId', 'text', emailId, 'Email')}
                                    {this.renderInput('col-12 p-0', false, 'password', 'password', password, 'Contraseña')}
                                </div>
                                <div className='row'>
                                    {this.renderCheckBox('col-12 col-lg-6 col-md-6 col-sm-6', 'custom-control custom-checkbox', 'Recuérdame', 'rememberMe', rememberMe)}
                                    <div className='col-12 col-lg-6 col-md-6 col-sm-6 text-right'>
                                        <Link to='/forgotPassword'>Se te olvidó tu contraseña</Link>
                                    </div>
                                </div>
                                {this.renderButton('btnbox d-flex justify-content-center mt-20', 'btn btn-block btn-default', 'Iniciar sesión')}
                                <div className='btnbox d-flex justify-content-center mt-20 mb-20' >
                                    <Link to='/register' className='btn btn-block btn-info'>Crear una cuenta</Link>
                                </div>
                                <h2 className='main-title'>Nuevos clientes</h2>
                                <p> Crear una cuenta tiene muchos beneficios: pague más rápido, agregue más de una dirección, rastree pedidos y mucho más.</p>

                            </form>
                            {/* Form -- end */}
                        </div>
                        {/* Content -- end */}
                    </div>
                    {/* Main Content -- end */}
                </div>
                {/* Footer Starts */}
            </div>
        );
    }
}

export default Login;