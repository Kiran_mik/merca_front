
import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import { withRouter } from "react-router-dom";
import { Select } from 'antd';
import InputRange from 'react-input-range';
import { Helmet } from "react-helmet";
import _ from 'lodash';
import CF from '../common/commonFuntions'; // COMMON FUNCTIONS
import { BeatLoader } from 'react-spinners';
import BreadCrumb from '../common/breadCrumb'
import ProductListing from '../reusable/productListing';
import moment from 'moment';
import tagIcon from '../../../src/assets/images/tag-icon.svg';
import OwlCarousel from 'react-owl-carousel';
import PageNotFound from './PageNotFound';
import $ from "jquery";
import Spinner from 'react-spinner-material';
window.jQuery = $;
window.$ = $;
global.jQuery = $;
class ProductSubCategory extends Component {
	constructor(props) {
		window.fbq('track', 'Products',
		);
		super(props);
		this.state = {
			loading: true,
			productListing: [],
			stock: false,
			sort: "",
			categoryName: '',
			brandArray: [],
			categoryNamesArray: [],
			subcategoryName: '',
			products: '',
			subCatName: '',
			// catName: '',
			banner: [],
			recentlyAdded: [],
			shopByCategory: [],
			bestSeller: [],
			featuredProducts: [],
			rating: "",
			changedValue: "Relavence",
			productsData: [],
			render: true,
			price: {
				min: 0,
				max: 1
			},
			maximumPriceRange: 1,
			minimumPriceRange: 0,
			productCatSlider: {
				margin: 20,
				loop: true,
				autoplay: true,
				animateOut: 'fadeOut',
				autoplayTimeout: 5000,
				autoWidth: false,
				nav: false,
				items: 1
			},
			////////// loader //////////
			error: false,
			hasMore: true,
			isLoading: false,
			users: [],
			page: 1,
			pagesize: 12,
			discountLabel: [],
			brandArray1: [],
			brands: false,
			applyPrice: false,
			message: '',
			total: '',
			statusCode: '',
			productSubCat: '',
			categoryName1: '',
			categoryName2: '',
			pageFound: true,
			// loadingBarProgress: 0
		}
		// Binds our scroll event handler
		window.onscroll = () => {
			const {
				state: { error, isLoading, hasMore },
			} = this;
			// Bails early if:
			// * there's an error
			// * it's already loading
			// * there's nothing left to load
			if (error || isLoading || !hasMore) return;
			// Checks that the page has scrolled to the bottom
			if ($(window).scrollTop() + window.innerHeight >= document.body.scrollHeight) { this.setState({ isLoading: true }, () => this.getProducts()) }
			if (document.documentElement.scrollTop + window.innerHeight === document.documentElement.scrollHeight) { this.setState({ isLoading: true }, () => this.getProducts()) }
			if (document.body.scrollTop + window.innerHeight === document.body.scrollHeight) this.setState({ isLoading: true }, () => this.getProducts())
		};
	}

	componentDidMount() {
		window.scrollTo(0, 0);
		if ($(window).width() > 1200) {
			$(".main").removeClass('nav-wrap home-page');
		}
		else {
			$(".main").addClass('fixed-sidebar');
		}
		const productSubCat = this.props.location.pathname;
		let categoryName1 = this.props.location.state && this.props.location.state.categoryName1 ? this.props.location.state.categoryName1 : '';
		let categoryName2 = this.props.location.state && this.props.location.state.categoryName2 ? this.props.location.state.categoryName2 : '';
		this.setState({ isLoading: true, productSubCat, categoryName1, categoryName2 }, () => this.getProducts());
		if (this.props.selectDropDown1) this.sortProducts(this.props.selectDropDown1)
		this.discountLabel();
		window.gtag('config', 'G-WB2B2WMR7Q', {
			'page_title': 'Product Sub-SubCategory ',
			'page_path': `${this.props.location.pathname}`
		});
	}

	componentWillReceiveProps(nextProps) {
		this.sortProducts(nextProps.selectDropDown1)
	}

	async getProducts() {
		let categoryId = this.props.subcatName.id;
		const { brandArray, stock, price, sort, page, pagesize, productListing, applyPrice } = this.state;
		const body = { categoryId, page, pagesize };
		if (applyPrice) {
			if (price.max !== 1) {
				const minSalePrice = price.min;
				const maxSalePrice = price.max;
				body.salePrice = { minSalePrice, maxSalePrice }
			}
		}
		if (stock === true) body.stock = "inStock"
		if (!_.isEmpty(brandArray)) {
			const brandName = _.map(_.filter(brandArray, (o) => { return o.checked }), object => { return object._id });
			if (!_.isEmpty(brandName)) body.brandName = brandName;
		}
		if (sort !== '') body.sort = { salePrice: Number(sort) };
		const response = await CF.callApi('/products/productFilter/' + this.props.match.params.customUrl, JSON.stringify(body), 'post');
		if (response.status === 1) {
			const { data } = response;
			const minMaxPrices = {
				min: data.minRange,
				max: data.maxRange
			};
			const { maximumPriceRange, minimumPriceRange } = this.state;
			this.setState({
				hasMore: (Number(page) * Number(pagesize) < data.total),
				productListing: [...productListing, ...data.products],
				isLoading: false,
				page: page + 1,
				pagesize: 12,
				pageFound: true,
				categoryName: data.categoryName.categoryName ? data.categoryName.categoryName : '',
				total: data.total ? data.total : "",
				brandArray: brandArray.length > 0 ? brandArray : data.brand.length > 0 ? data.brand : [],
				categoryNamesArray: data && data.categoryNamesArray.length ? data.categoryNamesArray : '',
				price: price.max === 1 ? minMaxPrices : price,
				maximumPriceRange: (maximumPriceRange === 1 || maximumPriceRange <= minMaxPrices.max) ? minMaxPrices.max : maximumPriceRange,
				minimumPriceRange: (minimumPriceRange === 0 || minimumPriceRange <= minMaxPrices.min) ? minMaxPrices.min : minimumPriceRange,
			})
		}
		else if (response.status === 0 && response.message === "Category details are not found.") {
			this.setState({ isLoading: false, hasMore: false, message: response.message, allCat: false, pageFound: false });
		}
		else if (response.status === 0 && response.message !== "Category details are not found.") {
			this.setState({ isLoading: false, hasMore: false, message: response.message });
		}
	}

	// for sorting
	sortProducts(value) {
		const { sort } = this.state;
		if (sort === value) return;
		this.setState({ sort: value, page: 1, pagesize: 12, productListing: [], isLoading: true }, () => this.getProducts());
	}

	// for handling the price
	handleChangeForPrice(price) {
		this.setState({ price, page: 1, pagesize: 12, productListing: [], applyPrice: true, isLoading: true }, () => this.getProducts())
	}

	// for selecting the brand
	handleChangeForBrand(event, brand) {
		let { brandArray } = this.state;
		const index = brandArray.findIndex(b => b._id === brand._id);
		if (index > -1) brandArray[index].checked = event.target.checked;
		this.setState({ brandArray, page: 1, pagesize: 12, productListing: [], isLoading: true }, () => this.getProducts());
	}

	getSingleProduct = (customUrl) => {
		this.props.getSingleProduct(customUrl)
		this.props.history.push(`/product/${customUrl}`)
	}

	handleStock = e => {
		this.setState({ stock: e.target.checked, page: 1, pagesize: 12, productListing: [], isLoading: true }, () => this.getProducts());
	}

	//********************** discounts ************************************/
	async discountLabel() {
		const response = await CF.callApi('/discount/getAllDiscounts', '', 'get', false, true);
		// console.log("response discount", response)
		if (response.status === 1) {
			this.setState({ discountLabel: response.data })
		}
	}
	render() {
		const { error, hasMore, isLoading } = this.state;
		const { Option } = Select;
		let { productListing, categoryName, price, brandArray, maximumPriceRange, sort, brands, loading, minimumPriceRange, discountLabel, message, total, productSubCat, categoryName1, categoryName2, pageFound } = this.state;
		let bread = this.state.productSubCat.split('/')
		return (
			<React.Fragment>
				{pageFound === true ?
					categoryName.length === 0 ?
						<div className="text-center page-loader">
							<Spinner size={70} spinnerColor={'#2472DC'} spinnerWidth={7} visible={true} />
							{/* <BeatLoader sizeUnit={"px"} size={20} color={'#2472DC'} loading={loading} /> */}
							{!loading ? <p2 className="text-center text-danger" colSpan="6">Ups! Algo salió mal</p2> : null}
						</div> :
						<div>
							{/* <LoadingBar
								progress={this.state.loadingBarProgress}
								height={3}
								color='blue'
								onLoaderFinished={() => this.onLoaderFinished()}
							/> */}
							<Helmet>
								<meta charSet="utf-8" />
								<title>{categoryName}</title>
								<meta name="description" content={categoryName}></meta>
							</Helmet>
							<div id="page-content-wrapper" className="page-content-wrapper product-listing-content filter-sidepanel">
								<div className="breadcrumb-wrap">
									<BreadCrumb
										breadCrums={true}
										bread={bread}
										categoryName1={categoryName1}
										categoryName2={categoryName2}
										categoryName3={categoryName}
										sort={sort}
										isSort={true}
										sortProducts={this.sortProducts.bind(this)}
									/>
								</div>
								{/* Filter right sidenav */}
								<div className="filter-sidebar">
									<div className="filter-header">
										<button className="filter-btn btn" title="Done" onClick={() => this.props.history.push('/')}>Limpiar todo</button>
										<button className="filter-btn btn">Hecho</button>
									</div>
									{/* {brandArray && brandArray.length > 0 ? */}
									<div id="accordion" className="category-nav">
										<div className="card">
											<div id="shortBy-selected" className="category-selected">
												<div className="card-body radiobtn-nav-list">
													<label className="radio-container">{categoryName}
														<input type="radio" defaultChecked="checked" name="shortBy-selected" />
														<span className="checkmark" />
													</label>
												</div>
											</div>
										</div>
										<div className="card">
											<div className="card-header">
												<a className="card-link collapsed" data-toggle="collapse" href="#shortbyNav" aria-expanded="false">
													Ordenar por
                                                </a>
											</div>
											<div id="shortbyNav" className="collapse" data-parent="#accordion" style={{}}>
												{/* <label htmlFor="" className="col-form-label text-right" > Ordenar: </label> */}
												<Select className="dropdown-select" value={sort}
													onChange={(value) => this.sortProducts(value)}>
													<Option value="">Relevancia</Option>
													<Option value={1}>Precio de menor a mayor</Option>
													<Option value={-1}>Precio mayor a menor</Option>
												</Select>
											</div>
										</div>
										{maximumPriceRange === minimumPriceRange ? '' :
											<div className="card  mb-3">
												<div className="card-header">
													<a className="collapsed card-link" data-toggle="collapse" href="#collapseTwo">Rango</a>
												</div>
												<div className="form-group  my-3">
													<div className="col-sm-10 pl-4">
														<InputRange
															className="form-control"
															id="typeinp"
															type="range"
															maxValue={maximumPriceRange}
															minValue={minimumPriceRange}
															value={price}
															onChange={(value) => this.setState({ price: value })}
															onChangeComplete={(value) => this.handleChangeForPrice(value)}
														/>
													</div>
												</div>
											</div>}
										{total > 0 &&
											<React.Fragment>
												{brandArray && brandArray.length > 1 &&
													<div className="card">
														<div className="card-header">
															<a className="card-link collapsed" data-toggle="collapse" href="#brandNav" aria-expanded="false">Marca</a>
														</div>
														<div id="brandNav" className="collapse" data-parent="#accordion" style={{}} >
															<ul className="card-body checkbox-nav-list">
																{(brands === false) ?
																	<div>
																		{brandArray.map((brand, key) => {
																			brand.checked = brand.checked ? brand.checked : false;
																			if (key < 5) {
																				return (<div className='custom-control custom-checkbox' key={key}>
																					<input type="checkbox" className="custom-control-input" name="brand" id={key} checked={brand.checked} onChange={(e) => this.handleChangeForBrand(e, brand)} />
																					<label className="custom-control-label" htmlFor={key}>	{brand.brandName}</label>
																				</div>)
																			}
																		})}
																		{brandArray && brandArray.length > 5 ? <p onClick={() => this.setState({ brands: true })}>MÁS</p> : ''}
																	</div>
																	: ''}

																{(brands === true) ?
																	<div>
																		{brandArray.map((brand, key) => {
																			brand.checked = brand.checked ? brand.checked : false;
																			return (<div className='custom-control custom-checkbox' key={key}>
																				<input type="checkbox" className="custom-control-input" name="brand" id={key} checked={brand.checked} onChange={(e) => this.handleChangeForBrand(e, brand)} />
																				<label className="custom-control-label" htmlFor={key}>	{brand.brandName}</label>

																			</div>)

																		})}
																		<p onClick={() => this.setState({ brands: false })}>MENOS</p>
																	</div>
																	: ''}
															</ul>
														</div>
													</div>}
											</React.Fragment>}
									</div>
								</div>
								{/* Breadcrumb Wrap End */}

								{/* contentWrap Start */}
								<div className="contentWrap">
									{/* Product category Sidebar Start */}

									<div className="category-sidebar">
										<div id="accordion" className="category-nav">
											<div className="card">
												<div id="category-selected" className="category-selected">
													<div className="card-body radiobtn-nav-list">
														<label className="radio-container">{categoryName}
															<input type="radio" defaultChecked="checked" name="category-filtered" />
															<span className="checkmark" />
														</label>
													</div>
												</div>
											</div>
											{maximumPriceRange === minimumPriceRange ? '' :
												<div className="card  mb-3">
													<div className="card-header">
														<a className="collapsed card-link" data-toggle="collapse" href="#collapseTwo">Rango</a>
													</div>
													<div className="form-group  my-3">
														<div className="col-sm-10 pl-4">
															<InputRange
																className="form-control"
																id="typeinp"
																type="range"
																maxValue={maximumPriceRange}
																minValue={minimumPriceRange}
																value={price}
																onChange={(value) => this.setState({ price: value })}
																onChangeComplete={(value) => this.handleChangeForPrice(value)}
															/>
														</div>
													</div>
												</div>}
											{total > 0 &&
												<React.Fragment>
													{brandArray && brandArray.length > 1 &&
														<div className="card">
															<div className="card-header">
																<a className="card-link collapsed" data-toggle="collapse" href="#brandNav" aria-expanded="false">Marca</a>
															</div>
															<div id="brandNav" className="collapse show" data-parent="#accordion" style={{}} >
																<ul className="card-body checkbox-nav-list">
																	{(brands === false) ?
																		<div>
																			{brandArray.map((brand, key) => {
																				brand.checked = brand.checked ? brand.checked : false;
																				if (key < 5) {
																					return (<div className='custom-control custom-checkbox' key={key}>
																						<input type="checkbox" className="custom-control-input" name="brand" id={key} checked={brand.checked} onChange={(e) => this.handleChangeForBrand(e, brand)} />
																						<label className="custom-control-label" htmlFor={key}>	{brand.brandName}</label>
																					</div>)
																				}
																			})}
																			{brandArray && brandArray.length > 5 ? <p onClick={() => this.setState({ brands: true })}>MÁS</p> : ''}
																		</div>
																		: ''}

																	{(brands === true) ?
																		<div>
																			{brandArray.map((brand, key) => {
																				brand.checked = brand.checked ? brand.checked : false;
																				return (<div className='custom-control custom-checkbox' key={key}>
																					<input type="checkbox" className="custom-control-input" name="brand" id={key} checked={brand.checked} onChange={(e) => this.handleChangeForBrand(e, brand)} />
																					<label className="custom-control-label" htmlFor={key}>	{brand.brandName}</label>

																				</div>)

																			})}
																			<p onClick={() => this.setState({ brands: false })}>MENOS</p>
																		</div>
																		: ''}
																</ul>
															</div>
														</div>}
												</React.Fragment>}
										</div>
									</div>


									<div className="product-listing-wrap single-product-listing product-listinview">
										{discountLabel && Array.isArray(discountLabel) && discountLabel.length ?
											<div className="couponcodeStripe">
												<img src={tagIcon} alt="Coupon" className="img-fluid coupon-img" />
												<OwlCarousel className="product-cat-slider" {...this.state.productCatSlider}>
													{discountLabel && Array.isArray(discountLabel) && discountLabel.length ?
														discountLabel.map((each, key) => {
															let endDate = each && each.validity.endDate ? each.validity.endDate : ""
															let discount = each && each.discount ? each.discount : ''
															let onpurchaseAmount = each && each.minAmount ? each.minAmount : ""
															return (
																<div className="coupon-text" key={key}>
																	<h4>Descuento del {discount}% comprando más de ${onpurchaseAmount}</h4>
																	{/* <p>Expira {moment(endDate).format('ll')}.</p> */}
																</div>
															)
														}) : ''}
												</OwlCarousel>

											</div> : ""}
										{/* {total > 0 && statusCode == 200 ? */}
										{/* {message === '' && productListing.length !== 0 && total > 0 ? */}
										<ProductListing productListing={productListing} msg={message} productSubCat={productSubCat} categoryName1={categoryName1} categoryName2={categoryName2} categoryName3={categoryName} productDetail={this.props.match.url} />
										{/* :
										<div className="no-more-item "> No Products!</div>} */}
										{isLoading && <div className="text-center product-loader">
											<Spinner size={70} spinnerColor={'#2472DC'} spinnerWidth={7} visible={true} />

											{/* <Loader type="Oval" color={'#2472DC'} height={70} width={120} /> */}
											{/* <ClipLoader sizeUnit={"px"} size={50} color={'#2472DC'} border-width="20" /> */}
										</div>}
										{<div style={{ color: '#900' }}>{error}</div>}
										{!hasMore && <div className="text-center"></div>}
										<div className="page-footer"></div>
									</div>
								</div>
							</div>
						</div> : <PageNotFound />}
			</React.Fragment>

			// </div>
		);
	}
}

const mapStateToProps = state => ({
	subcatName: state.user.subCatDetails,
	selectDropDown1: state.user.selectDropDown
});

export default connect(mapStateToProps, actions)(ProductSubCategory);