
import React, { Component } from 'react';
import { withRouter } from "react-router-dom";
import { IMAGE_URL } from '../../config/configs';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import OwlCarousel from 'react-owl-carousel';
import { Select } from 'antd';
import 'antd/dist/antd.css';
import { Helmet } from "react-helmet";
import InputRange from 'react-input-range';
import _ from 'lodash';
import CF from '../common/commonFuntions'; // COMMON FUNCTIONS
// import queryString from 'query-string';
// import { BeatLoader } from 'react-spinners';
// ClipLoader,
import $ from "jquery";
import BreadCrumb from '../common/breadCrumb';
import CommonOwl from '../reusable/commonOwl';
import noImage from '../../../src/assets/images/noimage.jpg';
// import moment from 'moment';
import tagIcon from '../../../src/assets/images/tag-icon.svg';
import AllCategory from '../reusable/allCategory';
import PageNotFound from './PageNotFound';
// import Loader from 'react-loader-spinner';
import Spinner from 'react-spinner-material';
// import LoadingBar from 'react-top-loading-bar';
window.jQuery = $;
window.$ = $;
global.jQuery = $;
class ProductCategory extends Component {
	constructor(props) {
		window.fbq('track', 'productCategory',
		);
		super(props);
		this.state = {
			rating: '',
			sort: "",
			subCategory: [],
			categorynames: [],
			parentCategory: '',
			Products: [],
			quantity: 1,
			productSlider: {

				autoWidth: true,
				margin: 20,
				loop: false,
				mouseDrag: true,
				items: 4,
				slideBy: 4,
				nav: true,
				responsive: {
					0: { mouseDrag: false, items: 1, nav: false },
					480: { items: 1, nav: false },
					600: { mouseDrag: false, items: 2, nav: false },
					899: { mouseDrag: false, items: 2, nav: false },
					1000: { mouseDrag: false, items: 3, nav: false, },
					1025: { mouseDrag: false, items: 3, nav: true, slideBy: 1 },
					1200: { mouseDrag: false, items: 3, nav: true, slideBy: 2 },
					1600: { mouseDrag: false, items: 4, nav: true, slideBy: 3 },
					1920: { mouseDrag: false, items: 4, nav: true, slideBy: 4 }
				}
			},
			productCatSlider: {
				margin: 10,
				loop: false,
				autoWidth: true,
				nav: true,
			},
			productCatSlider1: {
				margin: 25,
				loop: true,
				animateOut: 'fadeOut',
				autoplay: true,
				autoplayTimeout: 5000,
				autoWidth: false,
				nav: false,
				items: 1
			},
			brandArray: [],
			categoryName: '',
			subCategoryName: [],
			categoryNamesArray: [],
			productListing: [],
			categoryId: '',
			_id: '',
			price: {
				min: 0,
				max: 1
			},
			maximumPriceRange: 1,
			minimumPriceRange: 0,
			stock: false,
			// Sets up our initial state
			error: false,
			hasMore: true,
			isLoading: false,
			users: {},
			page: 1,
			pagesize: 3,
			discountLabel: [],
			loading: true,
			brands: false,
			applyPrice: false,
			subCategories: false,
			message: '',
			total: '',
			url: '',
			urlField: '',
			page1: 1,
			pagesize1: 12,
			allCat: false,
			allCategory: [],
			productListingNoData: [],
			productListingIds: [],
			color: '',
			pageFound: true,
			loadingBarProgress: 0,
			previousValue: 1,
			previousValue1: 1

		};
		// for lazy loading...
		window.onscroll = () => {
			const {
				state: { error, isLoading, hasMore, allCat },
			} = this;
			// Bails early if:
			// * there's an error
			// * it's already loading
			// * there's nothing left to load
			if (error || !hasMore || isLoading) return;
			// Checks that the page has scrolled to the bottom
			if ($(window).scrollTop() + window.innerHeight >= document.body.scrollHeight) {

				this.setState({ isLoading: true }, allCat === true ? () => this.getAllCategories() : () => this.getProductFilterApi())
			}
			if (document.documentElement.scrollTop + window.innerHeight === document.documentElement.scrollHeight) {
				this.setState({ isLoading: true }, allCat === true ? () => this.getAllCategories() : () => this.getProductFilterApi())
			}
			if (document.body.scrollTop + window.innerHeight === document.body.scrollHeight) {
				this.setState({ isLoading: true }, allCat === true ? () => this.getAllCategories() : () => this.getProductFilterApi())
			}
		}
	}


	// complete = () => {
	// 	this.setState({ loadingBarProgress: 100 });
	// };
	// onLoaderFinished = () => {
	// 	this.setState({ loadingBarProgress: 0 });
	// };

	componentDidMount() {
		// window.scrollTo(0, 0);
		if ($(window).width() > 1200) {
			$(".main").removeClass('nav-wrap home-page');
		}
		else {
			$(".main").addClass('fixed-sidebar');
		}
		const url = this.props.location.pathname;
		this.setState({ isLoading: true, url, allCat: false, page: 1, pagesize: 3, page1: 1, pagesize1: 12, allCategory: [], productListing: [], }, () => this.getProductFilterApi());
		window.gtag('config', 'G-WB2B2WMR7Q', {
			'page_title': 'Product Category',
			'page_path': `${url}`
		});
		// if (this.props.getproductId) this.setState({ page: 1, pagesize: 4, isLoading: true, allCat: false }, () => this.getProductFilterApi())
		this.discountLabel();
	}

	componentWillReceiveProps(nextProps) {
		// window.scrollTo(0, 0);
		if (nextProps.location.pathname !== this.state.url) {
			const url = nextProps.location.pathname
			window.gtag('config', 'G-WB2B2WMR7Q', {
				'page_title': 'Product Category',
				'page_path': `${url}`
			});
			this.setState({
				url, page: 1, pagesize: 3, price: {
					min: 0,
					max: 1
				}, maximumPriceRange: 1,
				minimumPriceRange: 0, brandArray: [], productListing: [], page1: 1, pagesize1: 12, allCategory: [], allCat: false, subCategoryName: [], isLoading: true, categoryName: '', categoryImage: '',
			}, () => this.getProductFilterApi())
		}
		if (nextProps.selectDropDown1) this.sortProducts(nextProps.selectDropDown1);
	}

	// for handling the price
	handleChangeForPrice() {
		let { price } = this.state;
		this.setState({ price, page: 1, pagesize: 3, productListing: [], page1: 1, pagesize1: 12, allCategory: [], applyPrice: true, isLoading: true, allCat: false }, () => this.getProductFilterApi())
	}
	sortProducts(value) {
		const { sort } = this.state;
		if (sort === value) return;
		this.setState({
			sort: value, page: 1, pagesize: 3, page1: 1, pagesize1: 12, allCategory: [], productListing: [], applyPrice: true, isLoading: true, allCat: false
		}, () => this.getProductFilterApi());
	}
	//************************** From  OwlCaurosel List Id More or From Name ******************************//
	getproducts = (_id, customUrl) => {
		if (_id === undefined || customUrl === undefined) return;
		else {
			const { subCategoryName } = this.state;
			const subCat = subCategoryName.find(x => x._id === _id);
			if (subCat.subLevel) {
				window.gtag('event', 'SubCategory');
				this.props.getproducts(_id);
				this.props.history.push({ pathname: `${this.state.url}/${customUrl}`, state: { customUrl: customUrl, categoryName1: this.state.categoryName } });
			}
			else {
				window.gtag('event', 'Products');
				this.props.getSubCat(_id, this.state.categoryName)
				this.props.history.push({ pathname: `/allProducts${this.state.url}/${customUrl}`, state: { customUrl: customUrl, categoryName1: this.state.categoryName } });
			}
		}
	}

	//************************** From subCategory List - Id ******************************//
	getproductSubCategory = (id, count, customUrl) => {
		if (id === undefined || count === undefined || count === 0) return;
		else {
			this.props.getproducts(id)
			this.props.history.push({ pathname: `${this.state.url}/${customUrl}`, state: { customUrl: customUrl, categoryName1: this.state.categoryName } })
		}
	}
	//********************************** going to final level ********************************//
	getproductSubSubCategory = (id, count, customUrl) => {
		if (id === undefined || count === undefined || count === 0) return;
		else {
			this.props.getSubCat(id, this.state.categoryName)
			this.props.history.push({ pathname: `/allProducts${this.state.url}/${customUrl}`, state: { customUrl: customUrl, categoryName1: this.state.categoryName } })
		}
	}
	//************************** getting all categories and products ********************************//
	async getAllCategories() {
		this.setState({ allCat: true, previousValue1: this.state.page1 })
		const existedIds = []
		this.state.productListing && this.state.productListing.map(list => {
			list.products.map(prod => {
				existedIds.push(prod._id)
			})
		})
		const { price, sort, page1, pagesize1, brandArray, applyPrice } = this.state;
		let body = { 'page': page1, 'pagesize': pagesize1, existedIds };
		if (applyPrice) {
			if (price.max !== 1) {
				const minSalePrice = price.min;
				const maxSalePrice = price.max;
				body.salePrice = { minSalePrice, maxSalePrice }
			}
		}
		if (!_.isEmpty(brandArray)) {
			const brandName = _.map(_.filter(brandArray, (o) => { return o.checked }), object => { return object._id });
			if (!_.isEmpty(brandName)) body.brandName = brandName;
		}
		if (sort !== '') body.sort = { salePrice: Number(sort) };
		const response = await CF.callApi('/products/allCategoryProducts' + this.state.url, JSON.stringify(body), 'post');
		// console.log('previousValue1', previousValue1)
		// console.log('page1', page1)
		if (response.status === 1) {
			const { data } = response;
			// if (page1 * pagesize1 < data.total){
			// if (data.categoryProducts && data.categoryProducts.products !== 0 && (page1 === 1 || page1 !== previousValue1)) {

			this.setState({
				// lazy loading -- Start
				// //  data.subcategoryName.length
				hasMore: (page1 * pagesize1 < data.total),
				allCategory: data && data.categoryProducts && data.categoryProducts.products && [...this.state.allCategory, ...data.categoryProducts.products],
				page1: page1 + 1,
				pagesize1: 12,
				isLoading: false,
				brandArray: this.state.brandArray && this.state.brandArray.length ? this.state.brandArray : data.brand,
			})
			// }


		}
	}

	//***************************** getting categories and sliders ********************************//
	async getProductFilterApi() {
		this.setState({ previousValue: this.state.page })
		const { price, sort, page, pagesize, brandArray, applyPrice, url, previousValue } = this.state;
		let body = { page, pagesize };
		if (applyPrice) {
			if (price.max !== 1) {
				const minSalePrice = price.min;
				const maxSalePrice = price.max;
				body.salePrice = { minSalePrice, maxSalePrice }
			}
		}
		if (!_.isEmpty(brandArray)) {
			const brandName = _.map(_.filter(brandArray, (o) => { return o.checked }), object => { return object._id });
			if (!_.isEmpty(brandName)) body.brandName = brandName;
		}
		if (sort !== '') body.sort = { salePrice: Number(sort) };
		const response = await CF.callApi('/products/productFilter' + url, JSON.stringify(body), 'post');
		if (response.status === 1) {
			const { data } = response;
			const minMaxPrices = {
				min: data.minRange,
				max: data.maxRange
			};
			const { maximumPriceRange, minimumPriceRange } = this.state;
			if (data.productDetails && data.productDetails.length !== 0 && (page === 1 || page !== previousValue)) {
				this.setState({
					hasMore: (page * pagesize < data.total),
					page: page + 1,
					pagesize: 3,
					productListing: [...this.state.productListing, ...data.productDetails],
					isLoading: false,
					pageFound: true,
					color: data && data.categoryName && data.categoryName.categoryName ? data.categoryName.color : '', categoryName: data && data.categoryName && data.categoryName.categoryName ? data.categoryName.categoryName : '',
					categoryImage: data && data.categoryName && data.categoryName.categoryImage ? data.categoryName.categoryImage : '',
					categoryId: data && data.categoryName && data.categoryName._id ? data.categoryName._id : '',
					subCategoryName: data.subcategoryName && Array.isArray(data.subcategoryName) && data.subcategoryName.length ? data.subcategoryName : [],
					total: data && data.total ? data.total : '',
					brandArray: this.state.brandArray && this.state.brandArray.length ? this.state.brandArray : data.brand,
					categoryNamesArray: data && data.categoryNamesArray ? data.categoryNamesArray : '',
					price: price.max === 1 ? minMaxPrices : price,
					maximumPriceRange: (maximumPriceRange === 1 || maximumPriceRange <= minMaxPrices.max) ? minMaxPrices.max : maximumPriceRange,
					minimumPriceRange: (minimumPriceRange === 0 || minimumPriceRange <= minMaxPrices.min) ? minMaxPrices.min : minimumPriceRange,
				});
			}
			else if (data.productDetails && data.productDetails.length === 0 && (page * pagesize <= data.total)) {
				this.setState({
					hasMore: (page * pagesize < data.total),
					// isLoading: true,
					page: page + 1,
					pagesize: 3,
				}, () => this.getProductFilterApi())
			}

			if (page * pagesize >= data.total) {
				this.getAllCategories()
			}
		}
		else if (response.status === 0 && response.message === "Category details are not found.") {
			this.setState({ isLoading: false, hasMore: false, message: response.message, allCat: false, pageFound: false });
		}
		else if (response.status === 0 && response.message !== "Category details are not found.") {
			this.setState({ isLoading: false, hasMore: false, message: response.message, allCat: false });
		}
	}

	// for selecting the brand 
	handleChangeForBrand(event, brand) {
		let { brandArray } = this.state;
		const index = brandArray.findIndex(b => b._id === brand._id);
		if (index > -1) brandArray[index].checked = event.target.checked;
		this.setState({ brandArray, page: 1, pagesize: 3, productListing: [], isLoading: true, allCat: false }, () => this.getProductFilterApi());
	}

	//******************************** discounts ************************************//
	async discountLabel() {
		const response = await CF.callApi('/discount/getAllDiscounts', '', 'get', false, true);
		if (response.status === 1) {
			this.setState({ discountLabel: response.data })
		}
	}

	render() {
		let { Option } = Select;
		let { error, hasMore, isLoading, categoryName, subCategoryName, productListing, maximumPriceRange, brands, price, brandArray, categoryImage, sort, loading, subCategories, minimumPriceRange, discountLabel, total, allCat, message, allCategory, color, pageFound } = this.state;
		sort = _.isEmpty(sort) ? this.props.selectDropDown1 : sort;
		let bread = this.state.url.split('/');
		return (
			<React.Fragment>
				{pageFound === true ?
					categoryName.length === 0 ?
						<div className="text-center page-loader">
							<Spinner size={70} spinnerColor={'#2472DC'} spinnerWidth={7} visible={true} />
							{!loading ? <p2 className="text-center text-danger" colSpan="6">Ups! Algo salió mal</p2> : null}
						</div> :
						(<div>
							<Helmet>
								<meta charSet="utf-8" />
								<title>{categoryName}</title>
								<meta name="description" content={categoryName} />
							</Helmet>
							<div className="main fixed-sidebar mt-0">
								<div id="page-content-wrapper" className="page-content-wrapper product-listing-content filter-sidepanel">
									<div className="cat-banner-stripe">
										<div className="cat-banner-name darkYellowBg" style={{ backgroundColor: color }}>
											<h3>{categoryName}</h3>
										</div>
										<div className="cat-banner-img">
											{(!_.isEmpty(categoryImage)) ? (<img src={IMAGE_URL + categoryImage} width="1036px" height="183px" alt={categoryName} />) : (<img src={noImage} width="1036px" height="183px" alt={categoryName} />)}
										</div>
									</div>
									{/* Breadcrumb Wrap Start */}
									<div className="breadcrumb-wrap">
										<BreadCrumb
											breadCrums={true}
											bread={bread}
											categoryName1={categoryName}
											sort={sort}
											isSort={true}
										/>

									</div>
									{/* Filter right sidenav */}
									<div className="filter-sidebar">
										<div className="filter-header">
											<button className="filter-btn btn" title="clear all" onClick={() => this.props.history.push('/')}>Limpiar todo</button>
											<button className="filter-btn btn" title="Done">Hecho</button>
										</div>
										{total > 0 ?
											<div id="accordion" className="category-nav">
												<div className="card">
													<div className="card-header">
														<a className="card-link collapsed" data-toggle="collapse" href="#shortbyNav" aria-expanded="false">
															Ordenar por
                                                     </a>
													</div>
													<div id="shortbyNav" className="collapse" data-parent="#accordion" style={{}}>
														<Select className="dropdown-select" value={sort}
															onChange={(value) => this.sortProducts(value)}>
															<Option value="">Relavence</Option>
															<Option value={1}>Precio de menor a mayor</Option>
															<Option value={-1}>Precio mayor a menor</Option>
														</Select>
													</div>
												</div>
												<div className="card">
													<div className="card-header">
														<a className="card-link collapsed" data-toggle="collapse" href="#categoryNav" aria-expanded="false">
															{categoryName}
														</a>
													</div>
													<div id="categoryNav" className="collapse" data-parent="#accordion" style={{}}>
														{subCategories === false ?
															<ul className="card-body">
																{(subCategoryName) ? (
																	subCategoryName.map((subcategory, key) => {
																		if (subcategory.availableProducts > 0 && key < 15) {
																			return (<li key={key} onClick={subcategory.subLevel === true ? () => this.getproductSubCategory(subcategory._id, subcategory.count, subcategory.customUrl) : () => this.getproductSubSubCategory(subcategory._id, subcategory.count, subcategory.customUrl)}>
																				<a>{subcategory.categoryName}</a>
																			</li>)
																		}
																	})) : null
																}
																{subCategoryName && subCategoryName.length > 15 &&
																	<a className='btn nav-more-btn' onClick={() => this.setState({ subCategories: true })}>MÁS</a>
																}
															</ul> :
															<ul className="card-body">
																{(subCategoryName) ? (
																	subCategoryName.map((subcategory, key) => {
																		if (subcategory.availableProducts > 0) {
																			return (<li key={key} onClick={subcategory.subLevel === true ? () => this.getproductSubCategory(subcategory._id, subcategory.count, subcategory.customUrl) : () => this.getproductSubSubCategory(subcategory._id, subcategory.count, subcategory.customUrl)}>
																				<a>{subcategory.categoryName}</a>
																			</li>)
																		}
																	})) : null
																}
																<a className='btn nav-more-btn' onClick={() => this.setState({ subCategories: false })}>MENOS</a>
															</ul>}
													</div>
												</div>
												{maximumPriceRange === minimumPriceRange ? '' :
													<div className="card custom-input-range">
														<div className="card-header">
															<a className="collapsed card-link" data-toggle="collapse" href="#storeNav" aria-expanded="false">
																Rango
                                                    	</a>
														</div>
														<div id="storeNav" className="collapse" data-parent="#accordion" style={{}}>
															<div className="card-body radiobtn-nav-list">
																<InputRange
																	className="form-control"
																	id="typeinp"
																	type="range"
																	maxValue={maximumPriceRange}
																	minValue={minimumPriceRange}
																	value={price}
																	onChange={(value) => this.setState({ price: value })}
																	onChangeComplete={() => this.handleChangeForPrice()}
																/>
															</div>
														</div>
													</div>}

												{brandArray && brandArray.length > 1 &&
													<div className="card">
														<div className="card-header">
															<a className="collapsed card-link" data-toggle="collapse" href="#deliveryNav" aria-expanded="false">
																Marca
                                                    </a>
														</div>
														<div id="deliveryNav" className="collapse" data-parent="#accordion" style={{}}>
															<div className="card-body radiobtn-nav-list">
																{(brands === false) ?
																	<div>
																		{brandArray.map((brand, key) => {
																			brand.checked = brand.checked ? brand.checked : false;
																			if (key < 5) {
																				return (<div className='custom-control custom-checkbox' key={key}>
																					<input type="checkbox" className="custom-control-input" name="brand" id={key} checked={brand.checked} onChange={(e) => this.handleChangeForBrand(e, brand)} />
																					<label className="custom-control-label" htmlFor={key}>	{brand.brandName}</label>
																				</div>)
																			}
																		})}
																		{brandArray && brandArray.length > 5 ? <a className='btn nav-more-btn' onClick={() => this.setState({ brands: true })}>MÁS</a> : ''}
																	</div>
																	: ''}
																{(brands === true) ?
																	<div>
																		{brandArray.map((brand, key) => {
																			return (<div className='custom-control custom-checkbox' key={key}>
																				<input type="checkbox" className="custom-control-input" name="brand" id={key} checked={brand.checked} onChange={(e) => this.handleChangeForBrand(e, brand)} />
																				<label className="custom-control-label" htmlFor={key}>	{brand.brandName}</label>

																			</div>)
																		})}
																		<a className='btn nav-more-btn' onClick={() => this.setState({ brands: false })}>MENOS</a>
																	</div>
																	: ''}
															</div>
														</div>
													</div>}
											</div>
											: ""}
									</div>
									{/* Breadcrumb Wrap End */}
									{(subCategoryName) &&
										<div className="categoryList-mobile">
											<OwlCarousel className="product-cat-slider" {...this.state.productCatSlider}>
												{subCategoryName.map((subcategory, key) => {
													if (subcategory.availableProducts > 0) {
														return (
															<div className="item" key={key} onClick={subcategory.subLevel === true ? () => this.getproductSubCategory(subcategory._id, subcategory.count, subcategory.customUrl) : () => this.getproductSubSubCategory(subcategory._id, subcategory.count, subcategory.customUrl)}>
																<a title={subcategory.categoryName}>{subcategory.categoryName}</a>
															</div>
														)
													}
												})}
											</OwlCarousel>
										</div>
									}

									{/* contentWrap Start */}
									<div className="contentWrap">
										{/* Product category Sidebar Start */}
										<div className="category-sidebar">
											<div id="accordion" className="category-nav">
												{subCategoryName && subCategoryName.length > 0 && brandArray && brandArray.length > 0 ?
													<React.Fragment>
														<div className="card">
															<div className="card-header">
																<a className="card-link collapsed" data-toggle="collapse" href="#collapseOne">{categoryName}</a>
															</div>
															<div id="collapseOne" className="collapse show" data-parent="#accordion">
																{subCategories === false ?
																	<ul className="card-body">
																		{(subCategoryName) ? (
																			subCategoryName.map((subcategory, key) => {
																				if (subcategory.availableProducts > 0 && key < 15) {
																					return (<li key={key} onClick={subcategory.subLevel === true ? () => this.getproductSubCategory(subcategory._id, subcategory.count, subcategory.customUrl) : () => this.getproductSubSubCategory(subcategory._id, subcategory.count, subcategory.customUrl)}>
																						<a>{subcategory.categoryName}</a>
																					</li>)

																				}
																			})) : null
																		}


																		{subCategoryName && subCategoryName.length > 15 &&
																			<a className='btn nav-more-btn' onClick={() => this.setState({ subCategories: true })}>MÁS</a>
																		}


																	</ul> :
																	<ul className="card-body">
																		{(subCategoryName) ? (
																			subCategoryName.map((subcategory, key) => {
																				if (subcategory.availableProducts > 0) {
																					return (<li key={key} onClick={subcategory.subLevel === true ? () => this.getproductSubCategory(subcategory._id, subcategory.count, subcategory.customUrl) : () => this.getproductSubSubCategory(subcategory._id, subcategory.count, subcategory.customUrl)}>
																						<a >{subcategory.categoryName}</a>
																					</li>)
																				}
																			})) : null
																		}
																		<a className='btn nav-more-btn' onClick={() => this.setState({ subCategories: false })}>MENOS</a>
																	</ul>}
															</div>
														</div>

														{productListing && maximumPriceRange === minimumPriceRange ? '' :
															<div className="card mb-3">
																<div className="card-header">
																	<a className="collapsed card-link" data-toggle="collapse" href="#collapseTwo">Rango</a>
																</div>
																{/* {productListing && productListing.length === 0 && */}
																<div className="form-group  my-3">
																	<div className="col-sm-10 pl-4">
																		<InputRange
																			className="form-control"
																			id="typeinp"
																			type="range"
																			maxValue={maximumPriceRange}
																			minValue={minimumPriceRange}
																			value={price}
																			onChange={(value) => this.setState({ price: value })}
																			onChangeComplete={() => this.handleChangeForPrice()}
																		/>
																	</div>
																</div>
															</div>}
														{brandArray && brandArray.length > 1 &&
															<div className="card">
																<div className="card-header">
																	<a className="card-link collapsed" data-toggle="collapse" href="#brandNav" aria-expanded="false">Marca</a>
																</div>
																<div id="brandNav" className="collapse show" data-parent="#accordion" style={{}} >
																	<ul className="card-body checkbox-nav-list">
																		{(brands === false) ?
																			<div>
																				{brandArray.map((brand, key) => {
																					if (key < 5) {
																						return (<div className='custom-control custom-checkbox' key={key}>
																							<input type="checkbox" className="custom-control-input" name="brand" id={key} checked={brand.checked} onChange={(e) => this.handleChangeForBrand(e, brand)} />
																							<label className="custom-control-label" htmlFor={key}>	{brand.brandName}</label>
																						</div>)
																					}
																				})}
																				{brandArray && brandArray.length > 5 ? <a className='btn nav-more-btn' onClick={() => this.setState({ brands: true })}>MÁS</a> : ''}

																			</div> : ''}
																		{(brands === true) ?
																			<div>
																				{brandArray.map((brand, key) => {
																					return (<div className='custom-control custom-checkbox' key={key}>
																						<input type="checkbox" className="custom-control-input" name="brand" id={key} checked={brand.checked} onChange={(e) => this.handleChangeForBrand(e, brand)} />
																						<label className="custom-control-label" htmlFor={key}>	{brand.brandName}</label>

																					</div>)
																				})}
																				<a className='btn nav-more-btn' onClick={() => this.setState({ brands: false })}>MENOS</a>
																			</div>
																			: ''}
																	</ul>
																</div>
															</div>}
													</React.Fragment>
													: ''}
											</div>
										</div>

										<div className="product-listing-wrap">
											{discountLabel && Array.isArray(discountLabel) && discountLabel.length ?
												<div className="couponcodeStripe">
													<img src={tagIcon} alt="Coupon" className="img-fluid coupon-img" />
													<OwlCarousel className="product-cat-slider" {...this.state.productCatSlider1}>
														{discountLabel && Array.isArray(discountLabel) && discountLabel.length ?
															discountLabel.map((each, key) => {
																// console.log("discount", each)
																let endDate = each && each.validity.endDate ? each.validity.endDate : ""
																let discount = each && each.discount ? each.discount : ''
																let onpurchaseAmount = each && each.minAmount ? each.minAmount : ""
																return (
																	<div className="coupon-text" key={key}>
																		<h4>Descuento del {discount}% comprando más de ${onpurchaseAmount}</h4>
																		{/* <p>Expira {moment(endDate).format('ll')}.</p> */}
																	</div>
																)
															}) : ''}
													</OwlCarousel>

												</div> : ""}
											{productListing &&
												<CommonOwl
													productListing={productListing}
													homePage={false}
													productSlider={this.state.productSlider}
													subCategoryName={subCategoryName}
													onClick={this.getproducts.bind(this)}
													products={false}
													url={this.state.url}
													categoryName1={categoryName}
													categoryName={categoryName}
													message={message}
													allCategory={allCategory}
													productDetail={this.props.match.url}
												/>}
											{/* : <div className="no-more-item "> No Products!</div>} */}

											{allCat === true && <AllCategory allCategory={allCategory} categoryName={categoryName} url={this.state.url}
												categoryName1={categoryName} />}

											{isLoading && <div className="text-center product-loader">
												<Spinner size={70} spinnerColor={'#2472DC'} spinnerWidth={7} visible={true} />
											</div>}

											{<div style={{ color: '#900' }}>{error}</div>}
											{!hasMore && <div className="text-center"></div>}

											<div className="page-footer"></div>
										</div>
									</div>
								</div>
							</div>
						</div>) : <PageNotFound />}
			</React.Fragment>
		)
	}
}

const mapStateToProps = state => ({
	getproductId: state.user.getproductIdCat,
	sideBarId1: state.user.sideBarId,
	selectDropDown1: state.user.selectDropDown

});

export default withRouter(connect(mapStateToProps, actions)(ProductCategory));