import React, { Component } from 'react';
import { withRouter } from "react-router-dom";
import { connect } from 'react-redux';
import * as actions from '../../actions';
import { Select } from 'antd';
import _ from 'lodash';
import CF from '../common/commonFuntions'; // COMMON FUNCTIONS
import InputRange from 'react-input-range';
import queryString from 'query-string';
import { BeatLoader } from 'react-spinners';
import tagIcon from '../../../src/assets/images/tag-icon.svg';
import OwlCarousel from 'react-owl-carousel';
import ProductListing from '../reusable/productListing';
import Spinner from 'react-spinner-material';
import $ from "jquery";
window.jQuery = $;// For jquery -- Start
window.$ = $;
global.jQuery = $;// for jquery -- End
class SearchProducts extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            searchText: '',
            productListing: [],
            sort: '',
            stock: false,
            queryValues: '',
            brandArray: [],
            price: {
                min: 0,
                max: 1
            },
            maximumPriceRange: 1,
            minimumPriceRange: 0,
            productCatSlider: {
                margin: 20,
                loop: true,
                autoplay: true,
                animateOut: 'fadeOut',
                autoplayTimeout: 5000,
                autoWidth: false,
                nav: false,
                items: 1
            },
            // Sets up our initial state
            error: false,
            hasMore: true,
            isLoading: false,
            page: 1,
            pagesize: 12,
            discountLabel: [],
            brands: false,
            applyPrice: false,
            message: '',
            total: '',
        };
        // for lazy loading...
        window.onscroll = () => {
            const {
                state: { error, isLoading, hasMore },
            } = this;
            // Bails early if:
            // * there's an error
            // * it's already loading
            // * there's nothing left to load
            if (error || isLoading || !hasMore) return;
            // Checks that the page has scrolled to the bottom
            if ($(window).scrollTop() + window.innerHeight >= document.body.scrollHeight) { this.setState({ isLoading: true }, () => this.searchProduct()) }
            if (document.documentElement.scrollTop + window.innerHeight === document.documentElement.scrollHeight) { this.setState({ isLoading: true }, () => this.searchProduct()) }
            if (document.body.scrollTop + window.innerHeight === document.body.scrollHeight) { this.setState({ isLoading: true }, () => this.searchProduct()) }
        };
    }

    componentDidMount() {
        window.scrollTo(0, 0);
        if ($(window).width() > 1200) {
            $(".main").removeClass('nav-wrap home-page');
        }
        else {
            $(".main").addClass('fixed-sidebar');
        }
        const { searchText } = queryString.parse(window.location.search);
        this.setState({ searchText, isLoading: true }, () => this.searchProduct());
        this.discountLabel();
        window.gtag('config', 'G-WB2B2WMR7Q', {
            'page_title': 'Search Page',
            'page_path': `${searchText}`
        });
    }

    componentWillReceiveProps() {
        window.scrollTo(0, 0);
        const { searchText } = queryString.parse(window.location.search);
        this.setState({ searchText, price: { min: 0, max: 1 }, maximumPriceRange: 1, minimumPriceRange: 0, brandArray: [], productListing: [], page: 1, pagesize: 12, isLoading: true, total: '', brands: false }, () => this.searchProduct())

    }

    /**************************** For searching the product ******************************* */
    searchProduct = async () => {
        const { page, pagesize, searchText, brandArray, stock, price, sort, productListing, applyPrice, maximumPriceRange, minimumPriceRange } = this.state;
        const body = { searchText, page, pagesize };
        if (applyPrice) {
            if (price.max !== 1) {
                const minSalePrice = price.min;
                const maxSalePrice = price.max;
                body.salePrice = { minSalePrice, maxSalePrice }
            }
        }
        if (stock === true) body.stock = "inStock"
        if (!_.isEmpty(brandArray)) {
            const brandName = _.map(_.filter(brandArray, (o) => { return o.checked }), object => { return object._id });
            if (!_.isEmpty(brandName)) body.brandName = brandName;
        }
        if (sort !== '') body.sort = { salePrice: Number(sort) };
        // console.log('response body search', body)
        const response = await CF.callApi('/products/searchProduct', JSON.stringify(body), 'post');
        // console.log('response search', response)
        if (response.status === 1) {
            const { data } = response;
            const { minRange, maxRange, brandData } = data;
            const minMaxPrices = {
                min: minRange,
                max: maxRange
            };
            this.setState({
                // lazy loading -- Start
                hasMore: (page < data.productListing.length),
                productListing: [...productListing, ...data.productListing],
                isLoading: false,
                page: page + 1,
                pagesize: 12,
                message: '',
                // lazy loading -- End
                brandArray: _.isEmpty(brandArray) ? brandData : brandArray,
                total: data.total ? data.total : '',
                price: price.max === 1 ? minMaxPrices : price,
                maximumPriceRange: (maximumPriceRange === 1 || maximumPriceRange <= minMaxPrices.max) ? minMaxPrices.max : maximumPriceRange,
                minimumPriceRange: (minimumPriceRange === 0 || minimumPriceRange >= minMaxPrices.min) ? minMaxPrices.min : minimumPriceRange,
            })
            window.fbq('track', 'Search',
                {
                    value: data && data.productListing[0].salePrice,
                    currency: 'ARS',
                    contents: [{ id: data && data.productListing[0].SKU }],
                    content_type: 'product'
                }
            );
        }
        else if (response.status === 0) {
            this.setState({ isLoading: false, hasMore: false, message: response.message });
        }
    }

    // for selecting the brand 
    handleChangeForBrand(event, brand) {
        // console.log(" brand in handleChangeForBrand ===>>>", brand);
        let { brandArray } = this.state;
        const index = brandArray.findIndex(b => b._id === brand._id);
        // console.log(" index in handleChangeForBrand ===>>>", index);
        if (index > -1) brandArray[index].checked = event.target.checked;
        this.setState({ brandArray, page: 1, pagesize: 12, productListing: [], isLoading: true }, () => this.searchProduct());
    }

    // for sorting the products
    sortProducts(value) {
        const { sort } = this.state;
        if (sort === value) return;
        this.setState({ sort: value, page: 1, pagesize: 12, productListing: [], isLoading: true }, () => this.searchProduct());
    }

    // for handling the price
    handleChangeForPrice(price) {
        this.setState({ price, page: 1, pagesize: 12, productListing: [], applyPrice: true, isLoading: true }, () => this.searchProduct())
    }

    // for in stock
    handleStock = e => {
        this.setState({ stock: e.target.checked, page: 1, pagesize: 12, productListing: [], isLoading: true }, () => this.searchProduct());
    }
    //********************** discounts ************************************/
    async discountLabel() {
        const response = await CF.callApi('/discount/getAllDiscounts', '', 'get', false, true);
        // console.log("response discount", response)
        if (response.status === 1) {
            this.setState({ discountLabel: response.data })
        }
    }
    render() {
        // console.log("props", this.props.search)
        const { error, isLoading, productListing, brandArray, maximumPriceRange, minimumPriceRange, price, searchText, loading, brands, sort, message, discountLabel, total } = this.state;
        const { Option } = Select;
        // console.log("brandArray ===>>>", brandArray);
        return (
            <React.Fragment>
                {searchText && searchText.length === 0 ?
                    <div className="text-center page-loader">
                        <BeatLoader sizeUnit={"px"} size={20} color={'#2472DC'} loading={loading} />
                        {!loading ? <p2 className="text-center text-danger" colSpan="6">Ups! Algo salió mal</p2> : null}
                    </div> :
                    <div>

                        <div id="page-content-wrapper" className="page-content-wrapper product-listing-content">
                            <div className="breadcrumb-wrap">
                                <nav aria-label="breadcrumb" className="mobile-d-none">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item active" onClick={() => this.props.history.push('/')}>Inicio</li>
                                        {/* <Link to='/'>Inicio</Link> */}
                                        {(searchText) && <li className="breadcrumb-item active" aria-current="page">{searchText}</li>}
                                    </ol>
                                </nav>
                                {productListing && productListing.length === 1 ? '' : total > 1 &&
                                    <div className="selectdropdownBox">
                                        <div className="select-Box mobile-d-none">
                                            {/* <label htmlFor="" className="col-form-label text-right" > Ordenar: </label> */}
                                            <Select className="dropdown-select" value={sort}
                                                onChange={(value) => this.sortProducts(value)}>
                                                <Option value="">Relevancia</Option>
                                                <Option value={1}>Precio de menor a mayor</Option>
                                                <Option value={-1}>Precio mayor a menor</Option>
                                            </Select>
                                        </div>
                                        <button className="filter-btn btn mobile-d-block" title="Filter"> Filtrar </button>
                                    </div>}
                            </div>
                            <div className="filter-sidebar">
                                <div className="filter-header">
                                    <button className="filter-btn btn" title="clear all" onClick={() => this.props.history.push('/')}>Limpiar todo</button>
                                    <button className="filter-btn btn">Hecho</button>
                                </div>
                                {productListing && productListing.length === 1 ? '' : total > 1 &&
                                    <div id="accordion" className="category-nav">
                                        <div className="card">
                                            <div className="card-header">
                                                <a className="card-link collapsed" data-toggle="collapse" href="#shortbyNav" aria-expanded="false">
                                                    Ordenar por
                                                </a>
                                            </div>
                                            <div id="shortbyNav" className="collapse" data-parent="#accordion" style={{}}>
                                                <label htmlFor="" className="col-form-label text-right" > Ordenar: </label>
                                                <Select className="dropdown-select" defaultValue=""
                                                    onChange={(value) => this.sortProducts(value)}
                                                    placeholder="Please select">
                                                    <Option value="">Relevancia</Option>
                                                    <Option value={1}>Precio de menor a mayor</Option>
                                                    <Option value={-1}>Precio mayor a menor</Option>
                                                </Select>
                                            </div>
                                        </div>
                                        {(maximumPriceRange === minimumPriceRange || productListing) && productListing.length === 1 ? '' :
                                            <div className="card  mb-3">
                                                <div className="card-header">
                                                    <a className="collapsed card-link" data-toggle="collapse" href="#collapseTwo">Rango</a>
                                                </div>
                                                <div className="form-group  my-3">
                                                    <div className="col-sm-10 pl-4">
                                                        <InputRange
                                                            className="form-control"
                                                            id="typeinp"
                                                            type="range"
                                                            maxValue={maximumPriceRange}
                                                            minValue={minimumPriceRange}
                                                            value={price}
                                                            onChange={(value) => this.setState({ price: value })}
                                                            onChangeComplete={(value) => this.handleChangeForPrice(value)}
                                                        />
                                                    </div>
                                                </div>
                                            </div>}
                                        {
                                            message === "" && brandArray && brandArray.length > 1 &&
                                            <div className="card">
                                                <div className="card-header">
                                                    <a className="card-link collapsed" data-toggle="collapse" href="#brandNav" aria-expanded="false">Marca</a>
                                                </div>
                                                <div id="brandNav" className="collapse " data-parent="#accordion" style={{}} >
                                                    <ul className="card-body checkbox-nav-list">
                                                        {(brands === false) &&
                                                            <div>
                                                                {brandArray.map((brand, key) => {
                                                                    brand.checked = brand.checked ? brand.checked : false;
                                                                    if (key < 5) {
                                                                        return (<div className='custom-control custom-checkbox' key={key}>
                                                                            <input type="checkbox" className="custom-control-input" name="brand" id={key} checked={brand.checked} onChange={(e) => this.handleChangeForBrand(e, brand)} />
                                                                            <label className="custom-control-label" htmlFor={key}>	{brand.brandName}</label>
                                                                        </div>)
                                                                    }
                                                                    return '';
                                                                })}
                                                                {brandArray && brandArray.length > 5 && <a className='btn nav-more-btn' onClick={() => this.setState({ brands: true })}>MÁS</a>}
                                                            </div>
                                                        }
                                                        {(brands) &&
                                                            <div>
                                                                {brandArray.map((brand, key) => {
                                                                    brand.checked = brand.checked ? brand.checked : false;
                                                                    return (<div className='custom-control custom-checkbox' key={key}>
                                                                        <input type="checkbox" className="custom-control-input" name="brand" id={key} checked={brand.checked} onChange={(e) => this.handleChangeForBrand(e, brand)} />
                                                                        <label className="custom-control-label" htmlFor={key}>	{brand.brandName}</label>
                                                                    </div>)
                                                                })}
                                                                {brandArray && brandArray.length < 5 ? "" : <a className='btn nav-more-btn' onClick={() => this.setState({ brands: false })}>MENOS</a>}
                                                            </div>
                                                        }
                                                    </ul>
                                                </div>
                                            </div>}
                                    </div>
                                }
                            </div>
                            <div className="contentWrap">
                                {total > 0 ?
                                    <div className="category-sidebar">
                                        <div id="accordion" className="category-nav">
                                            {maximumPriceRange === minimumPriceRange ? '' :
                                                <div className="card  mb-3">
                                                    <div className="card-header">
                                                        <a className="collapsed card-link" data-toggle="collapse" href="#collapseTwo">Rango</a>
                                                    </div>
                                                    <div className="form-group  my-3">
                                                        <div className="col-sm-10 pl-4">
                                                            <InputRange
                                                                className="form-control"
                                                                id="typeinp"
                                                                type="range"
                                                                maxValue={maximumPriceRange}
                                                                minValue={minimumPriceRange}
                                                                value={price}
                                                                onChange={(value) => this.setState({ price: value })}
                                                                onChangeComplete={(value) => this.handleChangeForPrice(value)}
                                                            />
                                                        </div>
                                                    </div>
                                                </div>}
                                            {
                                                message === "" && brandArray && brandArray.length > 1 &&
                                                <div className="card">
                                                    <div className="card-header">
                                                        <a className="card-link collapsed" data-toggle="collapse" href="#brandNav" aria-expanded="false">Marca</a>
                                                    </div>
                                                    <div id="brandNav" className="collapse show" data-parent="#accordion" >
                                                        <ul className="card-body checkbox-nav-list">
                                                            {(brands) ?
                                                                <div>
                                                                    {brandArray.map((brand, key) => {
                                                                        brand.checked = brand.checked ? brand.checked : false;
                                                                        return (<div className='custom-control custom-checkbox' key={key}>
                                                                            <input type="checkbox" className="custom-control-input" name="brand" id={key} checked={brand.checked} onChange={(e) => this.handleChangeForBrand(e, brand)} />
                                                                            <label className="custom-control-label" htmlFor={key}>	{brand.brandName}</label>
                                                                        </div>)
                                                                    })}
                                                                    {brandArray && brandArray.length < 5 ? "" : <a className='btn nav-more-btn' onClick={() => this.setState({ brands: false })}>MENOS</a>}
                                                                </div> :
                                                                <div>
                                                                    {/* {console.log("brands", brands)} */}
                                                                    {brandArray.map((brand, key) => {
                                                                        brand.checked = brand.checked ? brand.checked : false;
                                                                        if (key < 5) {
                                                                            return (<div className='custom-control custom-checkbox' key={key}>
                                                                                <input type="checkbox" className="custom-control-input" name="brand" id={key} checked={brand.checked} onChange={(e) => this.handleChangeForBrand(e, brand)} />
                                                                                <label className="custom-control-label" htmlFor={key}>	{brand.brandName}</label>
                                                                            </div>)
                                                                        }
                                                                        return '';
                                                                    })}
                                                                    {brandArray && brandArray.length > 5 && <a className='btn nav-more-btn' onClick={() => this.setState({ brands: true })}>MÁS</a>}
                                                                </div>
                                                            }
                                                        </ul>
                                                    </div>
                                                </div>}
                                        </div>
                                    </div> : ""}
                                <div className={(total === 0 || total === '') ? "product-listing-wrap single-product-listing product-listinview product-all-item" : "product-listing-wrap single-product-listing product-listinview"}>
                                    {discountLabel && Array.isArray(discountLabel) && discountLabel.length ?
                                        <div className="couponcodeStripe">
                                            <img src={tagIcon} alt="Coupon" className="img-fluid coupon-img" />
                                            <OwlCarousel className="product-cat-slider" {...this.state.productCatSlider}>
                                                {discountLabel && Array.isArray(discountLabel) && discountLabel.length ?
                                                    discountLabel.map((each, key) => {
                                                        let endDate = each && each.validity.endDate ? each.validity.endDate : ""
                                                        let discount = each && each.discount ? each.discount : ''
                                                        let onpurchaseAmount = each && each.minAmount ? each.minAmount : ""
                                                        return (
                                                            <div className="coupon-text" key={key}>
                                                                <h4>Descuento del {discount}% comprando más de ${onpurchaseAmount}</h4>
                                                                {/* <p>Expira {moment(endDate).format('ll')}.</p> */}
                                                            </div>
                                                        )
                                                    }) : ''}
                                            </OwlCarousel>
                                        </div> : ""}
                                    <ProductListing productListing={productListing} msg={message} />
                                    {isLoading && <div className="text-center product-loader">
                                        <Spinner size={70} spinnerColor={'#2472DC'} spinnerWidth={7} visible={true} />
                                        {/* <Loader type="Oval" color={'#2472DC'} height={70} width={120} /> */}
                                        {/* <ClipLoader sizeUnit={"px"} size={50} color={'#2472DC'} border-width="20" /> */}
                                    </div>}
                                    {<div style={{ color: '#900' }}>{error}</div>}
                                    <div className="page-footer"></div>
                                    {/* {!hasMore && <div className="no-more-item "> No Products!</div>} */}
                                </div>
                            </div>
                        </div>
                    </div>}
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    productListing: state.user.productListings,
    search: state.user.searchBar

});

export default withRouter(connect(mapStateToProps, actions)(SearchProducts));