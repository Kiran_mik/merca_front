
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import CF from '../common/commonFuntions'; // COMMON FUNCTIONS
import { Helmet } from "react-helmet";

class FAQ extends Component {
    state = {
        metaTitle: "",
        metaDescription: '',
        description: ''
    }
    componentDidMount() {
        this.FAQ()
    }

    /***************************** My orders ******************************/
    async FAQ() {
        window.gtag('config', 'G-WB2B2WMR7Q', {
            'page_title': 'FAQ',
            'page_path': '/faq'
        });
        window.gtag('event', 'FAQ');
        window.fbq('track', 'FAQ',
        );
        let body = { page: "Preguntas Frecuentes" }
        const response = await CF.callApi('/cms/pages', body, 'post');
        if (response.status === 1) {
            let { metaTitle, metaDescription, description, pageName } = response.data
            this.setState({ metaTitle, metaDescription, description, pageName })
        }
    }
    render() {
        let { metaTitle, description, pageName } = this.state
        return (
            <div>

                <Helmet>
                    <meta charSet="utf-8" />
                    <title>{metaTitle}</title>
                </Helmet>
                <div className="main mt-0">
                    <div id="aboutus-content" className="aboutus-content">
                        <div className="container">
                            {/* Breadcrumb Wrap Start */}
                            <div className="breadcrumb-wrap">
                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item"><Link to={'/'}>Inicio</Link></li>
                                        <li className="breadcrumb-item active" aria-current="page">Preguntas frecuentes</li>
                                    </ol>
                                </nav>
                            </div>
                            {/* Breadcrumb Wrap End */}
                            <div className="row">
                                <div className="col-md-12">
                                    <h2 className="lg-title">Preguntas frecuentes</h2>
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-md-12">
                                    <div className="infoBox">
                                        {description ?
                                            <p dangerouslySetInnerHTML={{ __html: `${description}` }} />
                                            :
                                            ''}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default FAQ;