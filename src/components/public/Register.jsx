import React from 'react';
// import SideBar from '../components/SideBar';
import CF from '../common/commonFuntions';
import EM from '../common/errorMessages';/// COMMON FUNCTIONS
import Form from '../reusable/form';

class Register extends Form {

  state = {
    formData: {
      firstName: '',
      lastName: '',
      isSubscribed: false,
      emailId: "",
      password: "",
      confirmPassword: '',
      rememberMe: false,
    },
    errors: {},
    isFormValid: true
  }

  /*******************  User Register ******************/
  async doSubmit() {
    window.gtag('config', 'G-WB2B2WMR7Q', {
      'page_title': 'Register ',
      'page_path': `/register`
    });
    window.fbq('track', 'Register',
    );
    const { firstName, lastName, emailId, password, rememberMe, isSubscribed } = this.state.formData;
    const body = { firstName, lastName, emailId, password, isSubscribed, rememberMe };
    const response = await CF.callApi('/user/register', body, 'post');
    if (response.status === 1) {
      CF.showSuccessMessage(EM._207);
      if (rememberMe) {
        CF.setItem('email', emailId)
        CF.setItem('password', btoa(password))
        CF.setItem('rememberMe', rememberMe)
      }
      this.props.history.push((`/login`))
    }
    if (response.status === 0 && response.statusCode === 402) CF.showErrorMessage(EM._402);
  }


  render() {
    let { firstName, lastName, emailId, password, confirmPassword, isSubscribed, rememberMe } = this.state;
    return (
      <div className="main mt-0">
        {/* main Content */}
        {/* Side Nav */}
        {/* <SideBar /> */}
        {/* /Side Nav */}
        {/* Content Starts */}
        <div id="page-content-wrapper">
          <form className="login-wrap" onSubmit={(e) => this.handleSubmit(e)}>
            <h2 className="main-title mb-25">Informacion personal</h2>
            <div className="row">
              {this.renderInput('col-12 p-0', true, 'firstName', 'text', firstName, 'Nombre')}
              {this.renderInput('col-12 p-0', false, 'lastName', 'text', lastName, 'Apellido')}
            </div>
            <div className="row">
              {this.renderCheckBox('col-12', 'custom-control custom-checkbox mb-50', 'Suscribirse al boletín.', 'isSubscribed', isSubscribed)}
            </div>
            <h2 className="main-title mb-25">Información de registro</h2>
            <div className="row">
              {this.renderInput('col-12 p-0', false, 'emailId', 'text', emailId, 'Email')}
              {this.renderInput('col-12 p-0', false, 'password', 'password', password, 'Contraseña')}
              {this.renderInput('col-12 p-0', false, 'confirmPassword', 'password', confirmPassword, 'Confirmar contraseña')}
            </div>
            <div className="row">
              {this.renderCheckBox('col-12', 'custom-control custom-checkbox mb-50', 'Recuérdame', 'rememberMe', rememberMe)}
            </div>
            {this.renderButton('btnbox d-flex justify-content-center', 'btn btn-block btn-default', 'Regístrate')}
          </form>
        </div>
        {/* Content End */}
        {/* /main Content */}
      </div>
    );
  }
}

export default Register;