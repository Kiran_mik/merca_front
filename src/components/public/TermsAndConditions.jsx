
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import CF from '../common/commonFuntions'; // COMMON FUNCTIONS
import { Helmet } from "react-helmet";
class TermsAndConditions extends Component {
  state = {
    metaTitle: "",
    metaDescription: '',
    description: '',
    loadingBarProgress: 0
  }

  componentDidMount() {
    this.termsAndConditions()
    window.gtag('config', 'G-WB2B2WMR7Q', {
      'page_title': 'Terms and Conditions',
      'page_path': '/termsAndConditions'
    });
    window.fbq('track', 'Terms and Conditions',
    );
  }

  /***************************** My orders ******************************/
  async termsAndConditions() {
    let body = { page: "Terminos y Condiciones" }
    const response = await CF.callApi('/cms/pages', body, 'post');
    if (response.status === 1) {
      let { metaTitle, metaDescription, description, pageName } = response.data
      this.setState({ metaTitle, metaDescription, description, pageName })
    }
  }

  render() {
    let { metaTitle, description, pageName } = this.state
    return (
      <div>

        <Helmet>
          <meta charSet="utf-8" />
          <title>{metaTitle}</title>
        </Helmet>
        <div className="main mt-0">
          <div id="aboutus-content" className="aboutus-content">
            <div className="container">
              {/* Breadcrumb Wrap Start */}
              <div className="breadcrumb-wrap">
                <nav aria-label="breadcrumb">
                  <ol className="breadcrumb">
                    <li className="breadcrumb-item"><Link to={'/'}>Home</Link></li>
                    <li className="breadcrumb-item active" aria-current="page">Términos y Condiciones</li>
                  </ol>
                </nav>
              </div>
              {/* Breadcrumb Wrap End */}
              <div className="row">
                <div className="col-md-12">
                  <h2 className="lg-title">Términos y Condiciones</h2>
                </div>
              </div>

              <div className="row">
                <div className="col-md-12">
                  <div className="infoBox">
                    {description ?
                      <p dangerouslySetInnerHTML={{ __html: `${description}` }} />
                      :
                      <div className="row">
                        <div className="col-md-6 mb-30" style={{ textAlign: 'center' }}>
                          <h3 className="main-title mb-25">No Details Found.</h3>
                          {/* <p>
                            Shop Target, Best Buy, Petsmart, and more—all in one place. Enter your info once, whether you’re checking out from one store or five. Need it again? A few quick taps is all it takes to reorder things you buy regularly.
                  </p>
                        </div>
                        <div className="col-md-6 mb-30">
                          <h3 className="main-title mb-25">A shoppiang list you’ll never forget. </h3>
                          <p>
                            Start a shopping list on Google Express and add to it or check things off from the website or app, wherever you are. Add items for later, share it with others, and shop from it with just a click. Check out these step-by-step instructions.
                  </p> */}
                        </div>
                      </div>}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* /main Content */}

        {/* Footer Starts */}
        {/* <footer class="footer">
            <div class="container">
                <p class="text-center">©1998-<span class="get-year"></span> Copyright. Indianic Infotech Ltd.</p>
            </div>
        </footer> */}
      </div>

    );
  }
}

export default TermsAndConditions;