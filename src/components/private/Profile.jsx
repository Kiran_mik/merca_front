import React from 'react';
import swal from "sweetalert";
import { withRouter } from "react-router-dom";
import { IMAGE_URL } from '../../config/configs';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import { Link } from 'react-router-dom';
// import queryString from "query-string";
import _ from 'lodash';
import CF from '../common/commonFuntions'; // COMMON FUNCTIONS
import EM from '../common/errorMessages';
import moment from 'moment';
import Form from '../reusable/form';
import AddressForm from './AddressForm'
import editIcon from '../../../src/assets/images/edit.svg';
import iconDelete from '../../../src/assets/images/icon-delete.svg';
import emptyImg from '../../../src/assets/images/emptyImg.png';
import iconHome from '../../../src/assets/images/icon-home.svg';
// import noImage from '../../../src/assets/images/noimage.jpg';
import { BeatLoader } from 'react-spinners';
import Spinner from 'react-spinner-material';

// for jquery -- Start 
import $ from "jquery";
window.jQuery = $;
window.$ = $;
global.jQuery = $;
// for jquery -- End

class Profile extends Form {
    constructor(props) {
        super(props);
        this.state = {
            OpenImgBtn: false,
            orderList: [],
            formData: {
                // for profile :: start
                firstName: "",
                lastName: '',
                emailId: "",
                mobileNo: '',
                photo: '',
                shippingAddresses: [],
                // for profile :: end
            },
            profileEditButton: true,
            addressForm: {
                name: "",
                addressLine1: "",
                addressLine2: "",
                city: "",
                state: "",
                zipCode: "",
                phone: "",
                _id: "",
                pin: ""
            },
            editAddressButton: false,
            listofStates: [],
            errors: {},
            isFormValid: true,
            firstLevel: '',
            loading: true,
            url: '',
            shippingUrl: false,
            trackingOrderDetails: [],
            previous: '',
            error: false,
            hasMore: true,
            isLoading: false,
            page: 1,
            pagesize: 10,
        }
        // for lazy loading...
        window.onscroll = () => {
            const {
                state: { error, isLoading, hasMore },
            } = this;
            // Bails early if:
            // * there's an error
            // * it's already loading
            // * there's nothing left to load
            if (error || isLoading || !hasMore) return;
            // Checks that the page has scrolled to the bottom
            if ($(window).scrollTop() + window.innerHeight >= document.body.scrollHeight) {
                this.setState({ isLoading: true }, () => this.myOrders())
            }
            if (document.documentElement.scrollTop + window.innerHeight === document.documentElement.scrollHeight) {
                this.setState({ isLoading: true }, () => this.myOrders())
            }
            if (document.body.scrollTop + window.innerHeight === document.body.scrollHeight) {
                this.setState({ isLoading: true }, () => this.myOrders())
            }
        }
    }

    tokenExpired = async () => {
        let body = { "token": localStorage.getItem('token') }
        const response = await CF.callApi('/auth/logOut', body, 'post', false, true);
        if (response.status === 1) CF.showSuccessMessage(EM._242)
        this.props.history.replace('/login');
    }

    componentDidMount() {
        if ($(window).width() > 1200) {
            $(".main").removeClass('nav-wrap home-page');
        }
        else {
            $(".main").addClass('fixed-sidebar');
        }
        this.renderData();
        window.gtag('config', 'G-WB2B2WMR7Q', {
            'page_title': 'Profile',
            'page_path': `/profile`
        });
        window.fbq('track', 'Profile',
        );
    }

    renderData = async () => {
        const firstLevel = this.props.location.state ? this.props.location.state : '';
        const orderList = await this.myOrders();
        const formData = await this.userProfile(); // user details
        const listofStates = await this.getStates();
        const url = this.props.location.pathname;
        this.setState({ orderList, url, formData, listofStates, firstLevel })
        window.gtag('config', 'G-WB2B2WMR7Q', {
            'page_title': `${firstLevel}`,
            'page_path': `/profile`
        });
    }
    componentWillReceiveProps(nextProps) {
        const url = nextProps.location.pathname
        const firstLevel = nextProps.location.state ? nextProps.location.state : '';
        window.gtag('config', 'G-WB2B2WMR7Q', {
            'page_title': `${firstLevel}`,
            'page_path': `/profile`
        });
        this.setState({ firstLevel, url })
    }

    // OpenImgDetail = () => {
    //     this.setState({ OpenImgBtn: !this.state.OpenImgBtn })
    // }
    /********************************** order tracking Details ******************************/
    async orderTrackingDetails(id) {
        const body = { "orderId": id };
        let response = await CF.callApi('/user/shippingDetails', body, 'post', false, true);
        if (response.status === 1) {
            this.setState({ shippingUrl: true, trackingOrderDetails: response.data })
        }
        // this.setState({ OpenImgBtn: !this.state.OpenImgBtn })

    }

    openDropDown(id) {
        let { orderList, previous } = this.state;
        const index = orderList.findIndex(b => b._id === id);
        if ((index > -1 && orderList) || (previous === id)) orderList[index].view = true;
        const Previousindex = orderList.findIndex(b => b._id === previous);
        if (Previousindex > -1 && Previousindex !== index) orderList[Previousindex].view = false;
        this.setState({ orderList, OpenImgBtn: true, previous: id }, () => this.orderTrackingDetails(id))
    }
    closeDropDown(id) {
        let { orderList } = this.state;
        const index = orderList.findIndex(b => b._id === id);
        if (index > -1 && orderList) orderList[index].view = false;
        this.setState({ orderList, OpenImgBtn: false })
    }
    /***************************** My orders ******************************/
    async myOrders() {
        let { page, pagesize, orderList } = this.state;
        window.gtag('event', 'orders');
        let body = { page, pagesize }
        const response = await CF.callApi('/user/getOrders', body, 'post', false, true);
        if (response.status === 1) {
            window.fbq('track', 'My Orders',
            );
            this.setState({
                hasMore: (page * pagesize < response.data.total),
                page: page + 1, pagesize, isLoading: false,
                orderList: [...orderList, ...response.data.ordersVal]
            });
            return response.data.ordersVal;
        }
        else if (response.message === "Invalid token.") CF.showErrorMessage("Session Expired"); this.tokenExpired();
    }

    /******************************* User profile ******************************/
    async userProfile() {
        const response = await CF.callApi('/user/getUserProfile', '', 'get', false, true);
        if (response.status === 0 && response.message === "Invalid token.") {
            CF.showErrorMessage("Session Expired");
            this.props.history.replace('/login')
        }
        else if (response.status === 0 && response.message !== "Invalid token.") {
            CF.showErrorMessage(response.message);
            this.tokenExpired();
        }
        if (response.status === 1) {
            window.gtag('event', 'Profile');
            window.fbq('track', 'Profile',
            );
            const { data } = response;
            let { shippingAddresses, firstName, lastName, emailId, mobileNo, photo } = data;
            mobileNo = mobileNo ? mobileNo : '';
            photo = photo ? photo : '';
            // if only one address is available then make it as a default address
            if (shippingAddresses.length === 1 && shippingAddresses[0].defaultAddress === false) this.setAsDefaultAddress(shippingAddresses[0]._id);
            CF.setItem("fname", data.firstName);
            CF.setItem("photo", data.photo ? data.photo : '');
            // CF.setItem('email', data.emailId ? data.emailId : '');
            this.updateHeader();
            return ({ firstName, lastName, emailId, mobileNo, photo, shippingAddresses });
        }
    }

    /***************** for sending data to header ******************************/
    updateHeader() {
        const { firstName: userName, photo: userPhoto } = this.state;
        this.props.userInfo(userName, userPhoto);
    }

    /******************************* states list ******************************/
    async getStates() {
        const response = await CF.callApi('/states/statesDropDown', '', 'post', false, true);
        if (response.status === 1) return response.data;
        return [];
    }

    /**************************** Submit the Form ******************************/
    doSubmit = async () => {
        const { profileEditButton } = this.state;
        const { firstName, lastName, emailId, mobileNo, photo } = this.state.formData;
        const body = { firstName, lastName, emailId, mobileNo, photo };
        const response = await CF.callApi('/user/editUserProfile', body, 'post', false, true);
        if (response.status === 1) {
            CF.showSuccessMessage(EM._210);
            this.setState({ profileEditButton: !profileEditButton })
            this.userProfile();
        }
        else if (response.status === 0 && response.message === "Invalid token.") {
            CF.showErrorMessage("Session Expired");
            this.tokenExpired();
        }
        else if (response.status === 0 && response.message !== "Invalid token.") {
            this.props.history.replace('/');
            CF.showErrorMessage(response.message);
        }
    }

    /***************** To set address as default ******************************/
    async setAsDefaultAddress(addressId) {
        const body = { addressId };
        await CF.callApi('/user/setDefaultAddress', body, 'post', false, true);
        this.renderAddresses();
    }

    /***************** make update ship address available ******************************/
    updateShippingAddressMakeAvailable(each) {
        window.gtag('event', 'ManageAddresses');
        window.fbq('track', 'ManageAddresses',
        );
        this.setState({ editAddressButton: true, addressForm: each });
    }

    /******************************* To Cancel order status ******************************/
    async cancelOrder(list, orderId, id) {
        // let { page, pagesize } = this.state;
        const body = { productId: orderId, orderId: list }
        swal({
            title: EM.WANT_TO_CANCEL,
            text: EM.WARNING_MSG_FOR_CANCEL,
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then(async willDelete => {
                if (willDelete) {
                    const response = await CF.callApi('/order/cancelProductsOfOrder', body, 'post', false, true);
                    if (response.status === 1) {
                        let orderList = this.state.orderList;
                        CF.showSuccessMessage(EM._239);
                        orderList = orderList.filter(order => {
                            if (order._id === list) {
                                const products = order.products.filter(product => {
                                    if (product._id === id) {
                                        product.isCancelled = 1;
                                    }
                                    return product;
                                });
                                order.products = products;
                                order.view = true;
                            }
                            return order
                        });
                        this.setState({ orderList });
                    }
                    else if (response.status === 0 && response.message === "Invalid token.") {
                        CF.showErrorMessage("Session Expired");
                        this.tokenExpired();
                    }
                    else if (response.status === 0 && response.message !== "Invalid token.") {
                        this.props.history.replace('/');
                        CF.showErrorMessage(response.message);
                    }
                }
            })
            .catch(error => {
                console.log("error is " + JSON.stringify(error));
            });
    }


    /***************** make update ship address available ******************************/
    async deleteShippingAddress(_id) {
        const request = { addressId: _id }
        // for conforming :: Start
        swal({
            title: EM.WANT_TO_DELETE,
            text: EM.WARNING_MSG_FOR_DELETE,
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then(async (willDelete) => {
                if (willDelete) {
                    const response = await CF.callApi('/user/deleteShippingAddress', request, 'post', false, true);
                    if (response.status === 1) this.renderAddresses();
                    CF.showSuccessMessage(EM._218);
                }
            })
            .catch(error => {
                console.log("error is " + JSON.stringify(error));
            });
    }

    renderAddresses = async () => {
        const { formData } = this.state;
        const { shippingAddresses } = await this.userProfile();
        formData.shippingAddresses = shippingAddresses;
        this.setState({ editAddressButton: false, formData })
    }

    changingState1 = () => {
        this.setState({ editAddressButton: false, errors: {} })
    }
    render() {
        const { orderList, formData, profileEditButton, listofStates, editAddressButton, addressForm, firstLevel, OpenImgBtn, shippingUrl, trackingOrderDetails, isLoading, error, hasMore } = this.state;
        const { firstName, lastName, emailId, photo, mobileNo, shippingAddresses, loading } = formData;

        let imagePreview = (<img src={emptyImg} alt='blank_image' />);
        if (photo) imagePreview = (<img src={IMAGE_URL + photo} alt="image" />);
        const sendAddressFormEmpty = { name: '', addressLine1: '', addressLine2: '', city: '', state: '', zipCode: '', phone: '', _id: '', pin: '' };
        // let localLocale = moment();
        return (
            <div id="profile-wrap" className="profile-wrap">
                {firstLevel ?

                    <div className="container">
                        {/* Breadcrumb Wrap Start */}
                        <div className="breadcrumb-wrap mb-10">
                            <nav aria-label="breadcrumb">
                                <ol className="breadcrumb">
                                    <li className="breadcrumb-item"><Link to='/'>Inicio</Link></li>
                                    <li className="breadcrumb-item"><a>Perfil</a></li>
                                    <li className="breadcrumb-item active" aria-current="page">
                                        <a> {(firstLevel === 'Mis ordenes') ? "Mis ordenes" : (firstLevel === 'Perfil') ? "Perfil" : (firstLevel === 'Administrar dirección') ? "Administrar dirección" : ''}</a>
                                    </li>
                                </ol>
                            </nav>
                        </div>
                        {/* Breadcrumb Wrap End */}
                        <h2 className="breadcrumb-item active sub-title mb-3">{firstLevel}</h2>
                        <div className="profile-tabs">
                            <div className="row">
                                <div className="col-lg-3 col-md-12 mb-30">
                                    <div className="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                        <a className={(firstLevel === 'Mis ordenes') ? "nav-link active" : "nav-link"} title="Mis ordenes" id="myorder-tab" data-toggle="pill" href="#myorder-wrap" role="tab" aria-controls="myorder-wrap" aria-selected="true">
                                            <span className="icon-icon-my-order" /> Mis ordenes </a>
                                        <a className={(firstLevel === 'Perfil') ? "nav-link active" : "nav-link"} title="Informacion personal" id="personal-info-tab" data-toggle="pill" href="#personal-info-wrap" role="tab" aria-controls="personal-info-wrap" aria-selected="true">
                                            <span className="icon-icon-user" /> Informacion personal </a>
                                        <a className={(firstLevel === 'Administrar dirección') ? "nav-link active" : "nav-link"} id="manageaddress-tab" title="Administrar dirección" data-toggle="pill" href="#manage-address-wrap" role="tab" aria-controls="manage-address-wrap" aria-selected="true">
                                            <span className="icon-icon-book" /> Administrar dirección </a>
                                        <a className="nav-link" title="Desconectarse" onClick={() => this.logout()}>
                                            <span className="icon-icon-logout" />Desconectarse</a>
                                    </div>
                                </div>
                                <div className="col-lg-9 col-md-12">
                                    <div className="tab-content" id="v-pills-tabContent">
                                        {/*********************** Orders Status -- Start ***************************/}
                                        <div className={(firstLevel === 'Mis ordenes') ? "tab-pane fade active show" : "tab-pane fade"} id="myorder-wrap" role="tabpanel" aria-labelledby="myorder-tab">
                                            <div className="myorderBox">
                                                <ul className="cart-items-list my-cart">
                                                    {(!_.isEmpty(orderList)) ? (
                                                        orderList.map((list, id) => {
                                                            moment.locale('es');

                                                            list.view = list.view ? true : false;
                                                            // new Date(each).getDay() === 2 ? "Tue" :
                                                            const createdAt = list.createdAt ? moment(list.createdAt).format("dddd, MMM  Do 'YY") : "";
                                                            return (
                                                                <li key={id}>
                                                                    <div className="deleveryTags">
                                                                        <span>Pedido : <strong className="text-primary">{list.orderId} </strong></span>
                                                                        <span>Realizado el: <strong>{createdAt} </strong></span>
                                                                        <span>Estado del pedido : <strong className="text-primary">{list.orderStatus} </strong></span>
                                                                    </div>
                                                                    <React.Fragment>
                                                                        <div className={(list.view === true) ? "item-imgbox-outer open-imgbox" : "item-imgbox-outer"}>
                                                                            {list && list.products.map((data, i) => {
                                                                                let photo = data && data.productThumbnailImage ? IMAGE_URL + data.productThumbnailImage.slice(0, -4) + "-th.jpg" : IMAGE_URL + "product.jpg";
                                                                                let productNameNoImg = data && data.productName ? (data.productName.substring(0, 5) + '...') : "no name";
                                                                                return (
                                                                                    <div className="item-imgbox" key={i}>
                                                                                        <Link to={{ pathname: `/categorias/${data.customUrl}`, state: { url: '' } }} className="item-imgbox-img">
                                                                                            <img src={photo} width="93px" height="93px" className="img-fluid" alt={productNameNoImg} />
                                                                                        </Link>
                                                                                        <div className="item-info">
                                                                                            <Link to={{ pathname: `/categorias/${data.customUrl}`, state: { url: '' } }}>
                                                                                                <h6>{data.productName}</h6>
                                                                                            </Link>

                                                                                            <div className="deleveryTags">
                                                                                                {/* SalePrice */}
                                                                                                <span>Precio : <strong>{data.salePrice}</strong></span>
                                                                                                {/* quantity: */}
                                                                                                <span>Cantidad : <strong>{data.productCount}</strong></span>
                                                                                                {((data.isCancelled === 1 || data.isCancelled === 5) && (list.orderStatus === "pending")) ?
                                                                                                    (<span>Estado del pedido:<strong>Cancelado</strong></span>) :
                                                                                                    (data.isCancelled === 0 && list.orderStatus === "pending" &&
                                                                                                        <span>
                                                                                                            <button onClick={() => this.cancelOrder(list._id, data.productId, data._id)} className="btn btn-link">Cancelar</button>
                                                                                                        </span>)
                                                                                                }
                                                                                                <div className="priceBox"> <h6>${data.productCount * data.salePrice}</h6></div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                )
                                                                            })}
                                                                        </div>
                                                                        {
                                                                            (list && list.orderStatus === "shipped" && list.view === true && OpenImgBtn === true && shippingUrl === true && trackingOrderDetails) &&
                                                                            trackingOrderDetails && trackingOrderDetails.map((each, id) => {
                                                                                return <div className="shipment-details" key={id}>
                                                                                    <div className="shipment-header">
                                                                                        <div>
                                                                                            <h5>Envío :</h5>
                                                                                        </div>
                                                                                        <div>
                                                                                            <span>Número de seguimiento de pedido : {each.trackingNumber}</span>
                                                                                            <span>
                                                                                                <a href={each.trackingUrl ? each.trackingUrl : "javascripts;;"} target="_blank" className="ml-2">Rastrear pedido</a></span>
                                                                                        </div>
                                                                                    </div>
                                                                                    {
                                                                                        each && each.products && each.products.map((list, key) => {
                                                                                            let photo = list && list.productImage ? IMAGE_URL + (list.productImage.slice(0, -4) + '-th.jpg') : IMAGE_URL + "product.jpg";
                                                                                            return <div className="item-imgbox" key={key} >
                                                                                                <Link to={{ pathname: `/categorias/${list.customUrl}` }}>
                                                                                                    <img src={photo} width="93px" height="93px" className="img-fluid" />
                                                                                                </Link>
                                                                                                <div className="item-info">
                                                                                                    <Link to={{ pathname: `/categorias/${list.customUrl}` }}>
                                                                                                        <h6>{list.productName}</h6>
                                                                                                    </Link>
                                                                                                    <div className="deleveryTags">
                                                                                                        <span>Precio: <strong>{list.salePrice}</strong></span>
                                                                                                        <span>Cantidad enviada: <strong>{list.productCount}</strong></span>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        })
                                                                                    }
                                                                                </div>
                                                                            })
                                                                        }
                                                                        {
                                                                            list.view === false && list.view !== true ?
                                                                                <div className="text-right">
                                                                                    <button className="btn btn-primary btn-sm mt-2" onClick={() => this.openDropDown(list._id)}>Ver detalles</button>
                                                                                </div> :
                                                                                < div className="text-right">
                                                                                    <button className="btn btn-primary btn-sm mt-2" onClick={() => this.closeDropDown(list._id)}>Cerrar detalles</button>
                                                                                </div>
                                                                        }
                                                                    </React.Fragment>
                                                                </li>
                                                            )
                                                            // }
                                                        })
                                                    ) : < li > No se encontraron pedidos.</li>}
                                                </ul>
                                                {isLoading && <div className="text-center product-loader">
                                                    <Spinner size={70} spinnerColor={'#2472DC'} spinnerWidth={7
                                                    } visible={true} />
                                                    {/* <ClipLoader sizeUnit={"px"} size={50} color={'#2472DC'} border-width="20" /> */}
                                                </div>}
                                                {<div style={{ color: '#900' }}>{error}</div>}
                                                {!hasMore && <div className="text-center"></div>}
                                            </div>
                                        </div>

                                        {/*********************** Orders Status -- End **********************/}
                                        {/************************ Profile update -- Start *************************/}
                                        <div className={(firstLevel === 'Perfil') ? "tab-pane fade active show" : "tab-pane fade"} id="personal-info-wrap" role="tabpanel" aria-labelledby="personal-info-tab">
                                            <form className="personal-Information" onSubmit={this.handleSubmit}>
                                                <div className="row">
                                                    {this.renderFile("Foto de perfil", "photo", imagePreview, profileEditButton)}
                                                    {this.renderInput('col-md-6 p-0', false, 'firstName', 'text', firstName, 'Primer nombre', profileEditButton)}
                                                    {this.renderInput('col-md-6 p-0', false, 'lastName', 'text', lastName, 'Apellido', profileEditButton)}
                                                    {this.renderInput('col-md-6 p-0', false, 'emailId', 'text', emailId, 'Email', true)}
                                                    {this.renderInput('col-md-6 p-0', false, 'mobileNo', 'text', mobileNo, 'Número de teléfono celular', profileEditButton)}
                                                    {profileEditButton && <div className="col-md-12"><button className="btn  btn-primary mt-20 mb-15" type='button' title="Editar" onClick={() => this.setState({ profileEditButton: !profileEditButton })}>Editar</button></div>}
                                                    {!profileEditButton && this.renderButton('col-md-12', 'btn  btn-primary mt-20 mb-15', 'Enviar')}
                                                </div>
                                            </form>
                                        </div>
                                        {/************************** Profile update -- End ************************/}
                                        {/************************** Mange address -- Start ***********************/}
                                        <div className={(firstLevel === 'Administrar dirección') ? "tab-pane fade active show" : "tab-pane fade"} id="manage-address-wrap" role="tabpanel" aria-labelledby="manageaddress-tab">
                                            <div className="manage-address">
                                                <div className="row">
                                                    {(_.isEmpty(shippingAddresses)) ? (<div className="mb-3 col-md-12"><b>No se encontraron direcciones</b></div>) :
                                                        (shippingAddresses.map((each, i) => {
                                                            const addressLine1 = each.addressLine1 ? (each.addressLine1) : ("");
                                                            // const addressLine2 = each.addressLine2 ? (each.addressLine2) : ("");
                                                            const city = each.city ? (each.city) : ("");
                                                            const state = each.state ? (each.state) : ("");
                                                            const phone = each.phone ? (each.phone) : ("");
                                                            const name = each.name ? (each.name) : ("");
                                                            const zipCode = each.zipCode ? (each.zipCode) : ("");
                                                            const pin = each.pin ? (each.pin) : ('');
                                                            return (
                                                                <div className="col-md-6" key={i}>
                                                                    <div className="addressBox mb-30">
                                                                        <div className="row mb-10">
                                                                            {this.renderCheckBox("col-6", "custom-control custom-checkbox mb-3", "Seleccionar como principal", each._id, each.defaultAddress, each._id)}
                                                                            <div className="col-6 text-right">
                                                                                <button className="btn" type="button" onClick={() => this.updateShippingAddressMakeAvailable(each)}><img src={editIcon} alt="Edit" width="13px" height="13px;" />Editar</button>
                                                                                <button className="btn" type="button" onClick={() => this.deleteShippingAddress(each._id)}><img src={iconDelete} alt="Remove" width="12px" height="15px" />Eliminar</button>
                                                                            </div>
                                                                            <div className="col-md-6"> <h6>{name}</h6></div>
                                                                        </div>
                                                                        <div className="address-info">
                                                                            <span> DNI:{pin}</span>
                                                                            <span>Número de celular:{phone}</span>
                                                                            <p> {addressLine1}<br />{city},{state},{zipCode}</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            )
                                                        }))}
                                                </div>
                                            </div>
                                            {editAddressButton === true ?
                                                <div className="row">
                                                    <div className="col-md-12">
                                                        <div className="mb-20">
                                                            <div id="shippingaddress" className="collapse show shippingaddress" data-parent="#accordion">
                                                                <a className="card-link " data-toggle="collapse" href="#shippingaddress">
                                                                    <div className="title py-3">
                                                                        <img src={iconHome} alt='' width="26px" height="22px" />Dirección de Envío
                                                                    </div>
                                                                </a>
                                                                {/******************** Update Addresss -- start *********************/}
                                                                <AddressForm
                                                                    formData={addressForm}
                                                                    listofStates={listofStates}
                                                                    onClick={this.renderAddresses}
                                                                    editAddressButton={editAddressButton}
                                                                    // changingState={this.changingState.bind(this)}
                                                                    changingState1={this.changingState1.bind(this)}
                                                                />
                                                                {/******************** Update Addresss -- end ************************/}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> : ""}
                                            {editAddressButton === false &&
                                                <button
                                                    className="btn btn-primary mb-3"
                                                    type='button'
                                                    onClick={() => { this.setState({ editAddressButton: true, addressForm: sendAddressFormEmpty, errors: {} }) }}>Añadir
                                                    </button>
                                            }
                                        </div>
                                        {/*********************** Mange address -- End ********************/}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    : <div className="text-center page-loader">
                        <BeatLoader sizeUnit={"px"} size={20} color={'#2472DC'} loading={loading} />
                        {/* {!loading ? <p2 className="text-center text-danger" colSpan="6">Ups! Algo salió mal</p2> : null} */}
                    </div>
                }
            </div>
        );
    }

    fileUpload = async (event) => {
        const { formData } = this.state;
        const selectedFile = event.target.files[0]
        const data = new FormData();
        data.append('file', selectedFile)
        const response = await CF.callApi('/auth/fileUpload', data, 'post', true, false);
        if (response.status === 1) {
            formData.photo = response.data.filepath;
            this.setState({ formData })
        }
    }

    logout() {
        localStorage.removeItem("token");
        localStorage.removeItem("fname");
        localStorage.removeItem("photo");
        // localStorage.removeItem("email");
        this.props.history.push("/")
    }
}

const mapStateToProps = state => ({
    userDetails: state.user.userDetails,
});

export default withRouter(connect(mapStateToProps, actions)(Profile));