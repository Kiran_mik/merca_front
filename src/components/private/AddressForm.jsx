import React from 'react';
import _ from 'lodash';
import Form from '../reusable/form';
import CF from '../common/commonFuntions';

class AddressForm extends Form {

    state = {
        formData: this.props.formData,
        listofStates: this.props.listofStates,
        editAddressButton: this.props.editAddressButton,
        errors: {}
    }

    componentWillReceiveProps = ({ formData, listofStates }) => {
        this.setState({ formData, listofStates, errors: {} })
    }

    /***************** submit address ******************************/
    doSubmit = async () => {
        const { name, addressLine1, addressLine2, city, state, zipCode, phone, _id, pin, defaultAddress } = this.state.formData;
        const body = { name, pin, addressLine1, addressLine2, city, state, zipCode, phone, defaultAddress }
        if (_.isEmpty(_id)) await CF.callApi('/user/addShippingAddress', body, 'post', false, true);
        else {
            body.addressId = _id;
            await CF.callApi('/user/updateShippingAddress', body, 'post', false, true);
        }
        this.props.onClick();
        this.setState({ formData: { name: '', pin: '', addressLine1: '', addressLine2: '', city: '', state: '', zipCode: '', phone: '', _id: '' } });
    }

    render() {
        const { formData, listofStates, editAddressButton } = this.state;
        let { name, addressLine1, addressLine2, city, state, zipCode, phone, pin } = formData
        if (_.isEmpty(formData)) { name = ''; addressLine1 = ''; addressLine2 = ''; city = ''; state = ''; zipCode = ''; phone = ''; }
        return (
            <React.Fragment>
                <form onSubmit={this.handleSubmit} >
                    <div className='row'>
                        {this.renderInput('col-md-12 p-0', true, 'name', 'text', name, 'Primer nombre', false, 'Primer nombre:')}
                        {this.renderInput('col-md-12 p-0', false, 'pin', 'text', pin, 'DNI', false, 'DNI:')}
                        {this.renderInput('col-md-12 p-0', false, 'addressLine1', 'text', addressLine1, 'Dirección', false, 'Dirección:')}
                        {/* {this.renderInput('col-md-12 p-0', false, 'addressLine2', 'text', addressLine2, 'Dirección 2', false, 'Dirección 2:')} */}
                        {this.renderInput('col-md-12 p-0', false, 'city', 'text', city, 'Ciudad', false, 'Ciudad :')}
                    </div>
                    <div className='row'>
                        {this.renderSelect('col-md-6 p-0', 'Provincia:', 'Seleccione provincia', 'state', state, listofStates)}
                        {this.renderInput('col-md-6 p-0', false, 'zipCode', 'text', zipCode, 'Código postal', false, 'Código postal')}
                    </div>
                    <div className='row'>
                        {this.renderInput('col-md-12 p-0', false, 'phone', 'text', phone, 'Número de teléfono celular', false, 'Número de teléfono celular')}
                    </div>
                    <div className="row justify-content-end m-0">
                        {editAddressButton === true &&
                            <button className="btn btn-line mr-3" type='button' onClick={() => this.props.changingState1()}>
                                Cancelar
                            </button>}
                        {this.renderButton('text-right', 'btn btn-primary', 'Guardar')}
                    </div>
                </form>

            </React.Fragment>
        );
    }
}

export default AddressForm;
