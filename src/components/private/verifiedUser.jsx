import React, { Component } from 'react';
import CF from '../common/commonFuntions';

class VerifiedUser extends Component {

    async componentDidMount() {
        // window.gtag('config', 'G-TTR3J41G77', {
        //     'page_title': 'verified User',
        //     'page_path': `/user/verifiedUser${queryValues}`
        // });
        const queryValues = window.location.search;
        const response = await CF.callApi(`/user/verifiedUser${queryValues}`, '', 'get');
        if (response.status === 1) CF.showSuccessMessage(response.message);
        else {
            CF.showErrorMessage(response.message);
            this.props.history.replace("/");
        }
    }
    render() {
        return (
            <div>
                <div className="main">
                    <div id="page-content-wrapper">
                        <div className="container">
                            <div className="confirmBox text-center">
                                <h2 className="main-title mb-25">Verificado con éxito</h2>
                                <p>Hemos verificado con éxito su correo electrónico.</p>
                                <p>Ahora puede iniciar sesión en el sitio web y continuar sus compras.</p>
                                <div className="btnBox mt-50">
                                    <button className='btn btn-primary' onClick={() => this.props.history.replace("/login")}>Haga clic aquí</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default VerifiedUser;