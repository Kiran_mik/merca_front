import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import { IMAGE_URL, PRODUCT_URL } from '../../config/configs';
import { Helmet } from "react-helmet";
import { Link } from "react-router-dom";
import Modal from 'react-responsive-modal';
import StarRatingComponent from 'react-star-rating-component';
import OwlCarousel from 'react-owl-carousel';
import swal from 'sweetalert';
import _ from 'lodash';
import moment from 'moment';
import Button from '../common/Button';
import EM from '../common/errorMessages'; // ERROR MESSAGE
import Pagination from 'rc-pagination';
import ImageGallery from 'react-image-gallery';
import { BeatLoader } from 'react-spinners';
import CF from '../common/commonFuntions';
import Form from '../reusable/form';
import BreadCrumb from '../common/breadCrumb'
import iconStar from '../../../src/assets/images/icon-star.svg';
import PageNotFound from '../public/PageNotFound';
import deliveryTruck from '../../../src/assets/images/delivery-truck.svg';
// import ReactImageMagnify from 'react-image-magnify';

// for jquery -- Start 
import $ from "jquery";
// app.get('/', function (request, response) {
// 	console.log('Home page visited!');
// 	const filePath = path.resolve(__dirname, './build', 'index.html');
// 	fs.readFile(filePath, 'utf8', function (err, data) {
// 		if (err) {
// 			return console.log(err);
// 		}
// 		data = data.replace(/\$OG_TITLE/g, 'Home Page');
// 		data = data.replace(/\$OG_DESCRIPTION/g, "Home page description");
// 		let result = data.replace(/\$OG_IMAGE/g, 'https://i.imgur.com/V7irMl8.png');
// 		response.send(result);
// 	});
// });
moment.locale('es-es');
window.jQuery = $;
window.$ = $;
global.jQuery = $;
// for jquery -- End
// $scope.var = moment($scope.var).toDate();


class ProductDetails extends Form {
	constructor(props) {
		super(props);
		this.state = {
			isLoggedIn: false,
			customUrl: '',
			brandName: '',
			categories: [],
			productDetails: {},
			reviewDetails: [],
			reviewsCount: [],
			totalCount: 0,
			addToCart: [],
			averageRating: 0,
			defaultReviewCounts: [
				{ "rating": 1, "count": 0 },
				{ "rating": 2, "count": 0 },
				{ "rating": 3, "count": 0 },
				{ "rating": 4, "count": 0 },
				{ "rating": 5, "count": 0 }
			],
			// formData -- Start
			formData: {
				reviewTitle: '',
				reviewRating: '',
				reviewDescription: ''
			},
			errors: {},
			// formData -- End
			// for image galery -- start
			// for model -- start
			imageVisible: false,
			modelStartIndex: 0,
			// for image galery -- end
			// For Reviews -- Start
			relatedItems: [],
			isOpenReviewModel: false,
			// pagination -- Start
			page: 1,
			pagesize: 5,
			showPagination: false,
			// pagination -- End
			// For Reviews -- End
			productSliderStyle: {
				margin: 10,
				loop: false,
				mouseDrag: true,
				items: 6,
				slideBy: 3,
				autoWidth: true,
				nav: true,
				responsive: {
					0: { mouseDrag: false, items: 1, nav: false },
					480: { items: 1, nav: false },
					600: { mouseDrag: false, items: 2, nav: false },
					1000: { mouseDrag: false, items: 3, nav: false },
					1025: { mouseDrag: false, items: 3, nav: true, slideBy: 2 },
					1366: { mouseDrag: false, items: 4, slideBy: 3, nav: true, }
				}
			},
			loading: true,
			urlFromCategory: '',
			avgRating: '',
			categoryName1: '',
			categoryName2: '',
			categoryName3: '',
			pageFound: true,
			availability: '',
			recent: ''
		}
		this.handleSlide = this.handleSlide.bind(this);
	}

	componentDidMount() {
		window.scrollTo(0, 0);
		if ($(window).width() > 1200) {
			$(".main").removeClass('nav-wrap home-page');
		}
		else {
			$(".main").addClass('fixed-sidebar');
		}
		const isLoggedIn = !_.isEmpty(CF.getItem('token'));
		const { customUrl } = this.props.match.params;
		window.gtag('config', 'G-WB2B2WMR7Q', {
			'page_title': 'Product Detail Page',
			'page_path': `/product/${customUrl}`
		});
		let categoryName1 = this.props.location.state && this.props.location.state.categoryName1 ? this.props.location.state.categoryName1 : '';
		let categoryName2 = this.props.location.state && this.props.location.state.categoryName2 ? this.props.location.state.categoryName2 : '';
		let categoryName3 = this.props.location.state && this.props.location.state.categoryName3 ? this.props.location.state.categoryName3 : '';
		const urlFromCategory = this.props && this.props.location && this.props.location.pathname ? this.props.location.pathname : '';
		const recent = this.props && this.props.location && this.props.location.state ? this.props.location.state.url : '';
		this.setState({ customUrl, isLoggedIn, urlFromCategory, categoryName1, categoryName2, categoryName3, recent }, () => this.getDetails())
	}

	componentWillReceiveProps(nextProps) {
		window.scrollTo(0, 0);
		const { customUrl } = nextProps.match.params;
		window.gtag('config', 'G-WB2B2WMR7Q', {
			'page_title': 'Product Detail Page',
			'page_path': `/product/${customUrl}`
		});
		this.setState({ customUrl, productDetails: {}, }, () => this.getDetails())
	}

	//****************************  most viewed ****************************/
	mostViewed = async () => {
		let body = { productId: this.state.id }
		await CF.callApi('/products/mostViewed', body, 'post');
	}


	getDetails = async () => {
		window.gtag('event', 'productDetails');
		const { page, pagesize, customUrl, defaultReviewCounts } = this.state;
		const body = { page, pagesize };
		const response = await CF.callApi(`/products/getProduct/${customUrl}`, body, 'post');
		if (response.status === 1) {
			const { data } = response;
			let { brandName, categories, productDetails, relatedItems, reviewDetails, reviewsCount, totalCount, avgRating, availability } = data;
			// For reviews -- Start
			if (_.isEmpty(reviewsCount)) reviewsCount = [...defaultReviewCounts];
			const averageRating = reviewsCount.map(list => list.count).reduce((a, c) => a + c);
			reviewsCount = _.reverse(reviewsCount);
			reviewsCount = _.map(reviewsCount, (object) => { return { ...object, starRating: (object.count * 100) / averageRating } });
			// For reviews -- End
			this.setState({ brandName, categories, productDetails, relatedItems, reviewDetails, reviewsCount, totalCount, averageRating, id: productDetails._id, avgRating, pageFound: true, availability })
			this.mostViewed();
			window.fbq('track', 'Product Details',
				// begin parameter object data
				{
					value: data.productDetails.salePrice,
					currency: 'ARD',
					contents: [
						{
							id: data.productDetails.SKU,
						},
					],
					content_type: 'product'
				}
				// end parameter object data
			);
		}
		else if (response.message === "Details are not found.") {
			this.setState({ pageFound: false })
		}
	}

	// for open model in image gallery -- Start

	openModal = () => this.setState({ imageVisible: true, });

	handleSlide = (index) => this.setState({ modelStartIndex: index });

	closeModal = () => this.setState({ imageVisible: false });

	// for open model in image gallery -- end

	// for write a review  -- Start
	openReviewMode = () => {
		const { isLoggedIn } = this.state;
		if (isLoggedIn) {
			this.setState({ isOpenReviewModel: true });
			window.gtag('event', 'Review');
			return;
		}
		const sepratepathId = window.location.pathname.split("/")
		swal({
			title: EM.PLEASE_SIGN_IN_REVIEW,
			icon: "warning",
			buttons: true,
			dangerMode: true,
		})
			.then(async (willOpen) => {
				if (willOpen) this.props.history.push(`/login${"?redirectPage=" + sepratepathId[1]}${sepratepathId[2] ? '/' + sepratepathId[2] : ""}`);
			})
			.catch(error => {
				console.log("error is " + JSON.stringify(error));
			});
	}

	closeisReviewModel = () => {
		this.setState({ isOpenReviewModel: false, errors: {}, formData: {} });
	}

	onStarClick = (nextValue) => {
		const { formData } = this.state;
		formData.reviewRating = nextValue
		this.setState({ formData });
	}
	// for write a review  -- End

	// reviews paginations -- Start

	// reviews paginations -- End
	handleShowPagination = () => this.setState({ showPagination: true, pagesize: 10 }, () => { this.getDetails() });

	paginationChange(page, pagesize) { this.setState({ page, pagesize }, () => this.getDetails()); }
	// for write a review  -- End

	async doSubmit() {
		const { productDetails, customUrl, formData } = this.state;
		const { productId, productName } = productDetails
		const productLink = PRODUCT_URL + customUrl;
		const { reviewRating: rating, reviewTitle: title, reviewDescription: description } = formData;
		const body = { rating, title, description, productName, productId, productLink }
		const response = await CF.callApi('/review/addReviews', body, 'post', false, true);
		if (response.status === 1) {
			CF.showSuccessMessage(response.message === "Details added successfully." ? "Comentario agregado con éxito" : response.message);
			formData.reviewRating = '';
			formData.reviewTitle = '';
			formData.reviewDescription = '';
			this.setState({ isOpenReviewModel: false, formData, errors: {} })
			this.getDetails(this.state.customUrl)
		}
		else if (response.message === "Invalid token.") {
			CF.showErrorMessage("Sesión expirada.");
			this.props.history.replace('/login')
		}
	}

	addWeekdays = (date, days) => {
		date.setDate(date.getDate());
		var counter = 0;
		if (days > 0) {
			while (counter < days) {
				date.setDate(date.getDate() + 1); // Add a day to get the date tomorrow
				var check = date.getDay(); // turns the date into a number (0 to 6)
				if (check === 0 || check === 6) {
					// Do nothing it's the weekend (0=Sun & 6=Sat)
				}
				else {
					counter++;  // It's a weekday so increase the counter
				}
			}
		}
		return date;
	}

	// var date = addWeekdays(new Date(), 0);
	// console.log('date', date.setDate(date.getDate()))
	render() {
		const { brandName, productDetails, reviewDetails, reviewsCount, totalCount, imageVisible, modelStartIndex, relatedItems, productSliderStyle, page, pagesize, showPagination, isOpenReviewModel, loading, formData, errors, urlFromCategory, avgRating, pageFound, availability, recent } = this.state;
		// productDetails
		const { productName, shortDescription, productImage, price, salePrice, images, stockQuantity, description, metaTitle, SKU, metaDescription, metaKeyword, size, supplierName } = productDetails;
		// For make a review
		const { reviewTitle, reviewDescription, reviewRating } = formData;
		let products = [];
		if (!_.isEmpty(productImage)) {
			products = images.map(data => {
				return ({
					"original": IMAGE_URL + data.imgName,
					"thumbnail": IMAGE_URL + data.imgName.slice(0, -4) + "-th.jpg"
				})
			});
			products.unshift({ "original": IMAGE_URL + productImage, "thumbnail": IMAGE_URL + productImage.slice(0, -4) + "-th.jpg" });
		}
		else {
			products.unshift({ "original": IMAGE_URL + "product.jpg", "thumbnail": IMAGE_URL + "product.jpg" });
		}
		const discount = (((price - salePrice) / price) * 100).toFixed();
		let capShortDescription = shortDescription ? shortDescription : '';
		let capDescription = description ? description : '';
		let bread = urlFromCategory ? urlFromCategory.split('/') : '';
		let recentData = recent ? recent.split('/') : '';
		// console.log('bread', bread)
		// console.log('recentData', recentData)
		var date = this.addWeekdays(new Date(), availability);
		const options = { year: 'numeric', month: 'long', day: 'numeric' };
		return (
			<React.Fragment>
				{pageFound === true ?
					_.isEmpty(productDetails) ?
						<div className="text-center page-loader">
							<BeatLoader sizeUnit={"px"} size={20} color={'#2472DC'} loading={loading} />
							{!loading ? <p2 className="text-center text-danger" colSpan="6">Ups! Algo salió mal</p2> : null}
						</div> :
						<div>
							<Helmet>
								<meta charSet="utf-8" />
								<title>{metaTitle}</title>
								<meta name="keyWord" content={metaKeyword} />
								<meta name="description" content={metaDescription} />
								<meta property="og:image" content={IMAGE_URL + productImage.slice(0, -4) + "-sm.jpg"} />
							</Helmet>
							<div className="main mt-0">
								<div id="product-details-content" className="product-details-content">
									<div className="container">
										{/************** Bread Crumb -- Start ************/}
										<div className="breadcrumb-wrap">
											<nav aria-label="breadcrumb" className="mobile-d-none">
												<ol className="breadcrumb" style={{ textTransform: 'capitalize' }}>
													<li className="breadcrumb-item" style={{ cursor: 'pointer' }} onClick={() => this.props.history.push('/')}>Inicio</li>
													{(recentData[1] === "Agregados Recientemente" || recentData[1] === "Productos Recomendados" || recentData[1] === "Más Vendidos" || recentData[1] === "Recomendado para ti") ?
														< li className="breadcrumb-item" onClick={() => this.props.history.push(`/products`, recentData[1])}>
															{recentData[1]}
														</li> : ''}
													{
														(bread[2] && bread[2] !== "allProducts" && bread[2] !== "all" && bread[2] !== "Agregados Recientemente" && bread[2] !== "Productos Recomendados" && bread[2] !== "Más Vendidos" && bread[2] !== "Recomendado para ti") &&
														(bread[3] ?
															<li className="breadcrumb-item" onClick={() => this.props.history.push(`/${bread[2]}`)}>{bread[2].split("-").join(" ")}</li> :
															<li className="breadcrumb-item" >{bread[2].split("-").join(" ")}</li>)
													}
													{bread[3] && bread[3] !== "product" && bread[2] !== "allProducts" ?
														(bread[4] ?
															<li className="breadcrumb-item" onClick={() => this.props.history.push({ pathname: `/${bread[2]}/${bread[3]}` })}>{bread[3].split("-").join(" ")}</li>
															:
															<li className="breadcrumb-item" >{bread[3].split("-").join(" ")}</li>) :

														((bread[2] === "all" && bread[3] !== "product") || (bread[2] === "allProducts" && bread[3] !== "product")) ?
															(bread[4] ?
																<li className="breadcrumb-item" onClick={() => this.props.history.push({ pathname: `/${bread[3]}` })}>{bread[3].split("-").join(" ")}</li> :
																<li className="breadcrumb-item" >{bread[3].split("-").join(" ")}</li>) : ''
													}

													{bread[4] &&
														(bread[5] ?
															<li className="breadcrumb-item" onClick={() => this.props.history.push({ pathname: `/${bread[2]}/${bread[3]}/${bread[4]}` })}>{bread[4].split("-").join(" ")}</li> :
															<li className="breadcrumb-item" >{bread[4].split("-").join(" ")}</li>)}

													{bread[5] && <li className="breadcrumb-item">{bread[5].split("-").join(" ")}</li>}
												</ol>
											</nav>
											<BreadCrumb isSort={false} />
										</div>
										{/********************* Bread Crumb -- End ******************/}
										<div className="row mb-50">
											<div className="col-xl-9 col-lg-9 col-md-12 pro-detailsBox-border">
												<h2 className="pro-title mb-20" >{productName}</h2>
												{/**************** For start rating -- Start *****************/}
												<div className="review-ratting-count">
													<StarRatingComponent
														name="rating"
														value={Number(avgRating ? avgRating : 0)}
														starCount={5}
														emptyStarColor={"#cecdca"}
													/>
													{(totalCount !== 0) && <a className="review-text" onClick={() => window.scrollTo(0, document.body.scrollHeight)}>{totalCount} Comentarios</a>}
												</div>
												{/********************** For start rating -- End **********************/}
												<div className="row">
													<div className="col-md-8">
														<ImageGallery items={products} onClick={() => this.openModal()} onSlide={this.handleSlide} startIndex={modelStartIndex} />
														<Modal open={imageVisible} onClose={() => this.closeModal()}>
															{imageVisible && <div><ImageGallery items={products} startIndex={modelStartIndex} /></div>}
														</Modal>
													</div>
													{capShortDescription ? <div className="col-md-4">
														<div className="item-description">
															<h4 className="mb-25">Acerca de este producto</h4>
															<p dangerouslySetInnerHTML={{ __html: `${capShortDescription}` }} />
														</div>
													</div> : null}
												</div>
											</div>
											<div className="col-xl-3 col-lg-3 col-md-12">
												<div className="product-item-delivery-Info">
													<div className="row">
														<div className="col-lg-12 col-md-6">
															<span className="greentext delivery-title mb-10"><img src={deliveryTruck} alt="Delivery Time" width="22px" height="14px" />
																{('Será entregado a su transporte el') + " " + (date && date.toLocaleDateString('es-eS', options))}
																{/* (date && (moment(date).format('D MMM YYYY')) */}
															</span>
														</div>

														<div className="col-lg-12 col-md-6">
															<div className="sellername">
																<div className="info">
																	<span><b>Marca: </b> <span className="ml-2">{brandName} </span></span>
																</div>
															</div>
															<div className="sellername">
																<div className="info">
																	<span><b>SKU : </b> <span className="ml-2">{SKU ? SKU : ''}</span></span>
																</div>
															</div>
															{size &&
																<div className="sellername">
																	<div className="info">
																		<span><b>Tamaño : </b> <span className="ml-2">{size ? size : ''}</span></span>
																	</div>
																</div>}
															{supplierName &&
																<div className="sellername">
																	<div className="info">
																		<span><b>Proveedor : </b> <span className="ml-2">{supplierName ? supplierName : ''}</span></span>
																	</div>
																</div>}
														</div>

														<div className="col-lg-12 col-md-6">
															<div className="priceBox">
																<h2>${salePrice === "" ? price : salePrice} <strike>${(salePrice === price || salePrice > price) ? "" : price}</strike> </h2>
																<span className="bluetext">{discount && `(${discount}% OFF)`}</span>
															</div>
															{(stockQuantity > 0) ? <Button data={productDetails} /> : <button className="btn btn-block qyn-cart mt-3 product-itemBox blur_image">Agotado</button>}
														</div>
													</div>
												</div>
											</div>
										</div>
										{description ?
											<div className="product-more-info">
												<h3 className="sub-title mb-20">Acerca de este producto</h3>
												<p dangerouslySetInnerHTML={{ __html: `${capDescription}` }} />
											</div> : null}
										{/***************************** Related Items -- Start ********************************/}
										<section className="product-slider-section section product-slider-part">
											{relatedItems.length !== 0 &&
												(<div className="container">
													<h3 className="sub-title">Artículos similares</h3>
													<div className="row mt-15">
														<OwlCarousel {...productSliderStyle} >
															{relatedItems.map((each, i) => {
																if (i < 9) {
																	const salePriceForItem = each.salePrice ? "$" + each.salePrice : "no price";
																	const regularPrice = each.price ? "$" + each.price : "no price";
																	let photo = each && each.productImage ? IMAGE_URL + each.productImage.slice(0, -4) + "-sm.jpg" : IMAGE_URL + "product.jpg";
																	const stockQuantityForItem = each.stockQuantity ? each.stockQuantity : 0;
																	const { customUrl } = each;
																	return (
																		<div className={stockQuantityForItem > 0 ? "product-itemBox" : "product-itemBox blur_image"} key={i}>
																			<Link to={`/categorias/${customUrl}`}>
																				<div className="imgBox" >
																					<img src={photo} alt={each.productName} width="81px" height="23px" className="img-fluid" />
																				</div>
																			</Link>
																			<div className="pro-details">
																				<Link to={`/categorias/${customUrl}`}>
																					<h6 className="product-name">
																						{each.productName}
																					</h6>
																				</Link>
																				<h4>{salePriceForItem.length > 0 ? salePriceForItem : regularPrice} <strike>{salePriceForItem === "" ? "" : regularPrice}</strike></h4>
																				<Button data={each} />
																			</div>
																		</div>
																	)
																}
																return null;
															})}
														</OwlCarousel>
													</div>
												</div>)
											}
										</section>
										{/******************* Related Items -- End *************************/}

										{/************************ Reviews -- Start ***************************/}
										<div className="product-more-info">
											<h3 className="sub-title mb-20">Resumen de comentarios</h3>
											<div className="row">
												<div className="col-3 d-flex">
													<div className="pro-total-ratings">
														<h2>{Math.round(avgRating ? avgRating : 0)}<img src={iconStar} alt="Ratting" width="20px" height="19px" /></h2>
														<p>{totalCount} Comentarios <br /></p>
													</div>
												</div>
												<div className="col-9 col-md-4">
													<ul className="product-stars-ratting">
														{(reviewsCount.length > 0) && (
															reviewsCount.map((list, i) => {
																return (
																	<li key={i}>
																		{list.rating} <img src={iconStar} alt="Ratting" width="13px" height="12px" />
																		<div className="progress">
																			<div className={(list.rating === 1) ? "progress-bar progress-bar bg-danger" : (list.rating === 2) ? "progress-bar bg-warning" : "progress-bar bg-success"} role="progressbar" aria-valuenow={40} aria-valuemin={0} aria-valuemax={100} style={{ width: (reviewsCount[i].starRating) + "%" }}>
																			</div>
																		</div>
																		{list.count}
																	</li>
																)
															}))
														}
													</ul>
												</div>
												{/************************* Writing Review -- Start **************************/}
												<div className="col-md-4">
													{<div>
														<div className="review-buttonBox" onClick={() => this.openReviewMode()}>
															<button className="btn btn-primary" title="Escribe tu reseña"  >Escribe tu reseña</button>
														</div>
														<Modal
															open={isOpenReviewModel}
															onClose={() => this.closeisReviewModel()}>
															{isOpenReviewModel &&
																<div className="review-rating-wrap">
																	<div className="wrapInfo">
																		<div className="imagebg" />
																		<div className="row">
																			<div className="col-md-12">
																				<h2>Escribe tu reseña</h2>
																				{/******************* Review Form -- Start **********************/}
																				<form method="post" id="reused_form" onSubmit={this.handleSubmit}>
																					<div className="row">
																						<div className="col-sm-12 form-group">
																							<label className="d-block">Califica este producto</label>
																							<StarRatingComponent
																								name="reviewRating"
																								starCount={5}
																								value={Number(reviewRating)}
																								emptyStarColor={"#cecdca"}
																								onStarClick={(value) => this.onStarClick(value)} />
																							<span className="error-msg">{errors.reviewRating}</span>
																						</div>
																					</div>
																					{this.renderInput('row', true, 'reviewTitle', 'text', reviewTitle, 'Title', false, 'Title:')}

																					{this.renderTextArea('Comments:', 'reviewDescription', reviewDescription, 'Your Comments')}
																					<div className="row">
																						{this.renderButton('col-sm-12 form-group', 'btn btn-primary btn-warning btn-block', 'Post')}
																					</div>
																				</form>
																				{/*********************** Review Form -- End **********************/}
																			</div>
																		</div>
																	</div>
																</div>
															}
														</Modal>
													</div>
													}
												</div>
												{/********************* Writing Review -- End ********************/}
											</div>
											{/************************** Reviews list -- Start **********************/}
											{reviewDetails.length === 0 ? "No hay comentarios para este producto" :
												(<div className="row">
													{reviewDetails.map((each, key) => {
														const title = each.title ? each.title : "";
														const descriptionForItem = each.description ? each.description : "";
														const ratingForItem = each.rating ? each.rating : "";
														const createdAt = each.createdAt ? moment(each.createdAt).format('D MMM YYYY') : ''
														const firstName = each.firstName ? each.firstName : "";
														const lastName = each.lastName ? each.lastName : "";
														return (
															<div className="col-md-12" key={key}>
																<div className="pro-details-commentsBox">
																	<StarRatingComponent
																		name="rating"
																		value={ratingForItem}
																		starCount={5}
																		emptyStarColor={"#cecdca"}
																	/>
																	<h6 className="mb-10 mt-10">{title}</h6>
																	<p>{descriptionForItem}</p>
																	<span className="writer-name">por {firstName} {lastName}, {createdAt} </span>
																</div>
															</div>
														)
													})}
												</div>)
											}
											{/************************** Reviews list -- End ******************************/}
											{/************************** Show more Reviews -- Start **************************/}
											<div className="table-footer">
												{Number(totalCount) > 5 && <span className="all-review" onClick={this.handleShowPagination}>TODAS LAS REVISIONES</span>}
												{showPagination &&
													<div className="pagination-list d-flex review-pagination custom-pagination">
														<Pagination
															className="ant-pagination"
															pageSize={pagesize}
															current={page}
															total={totalCount}
															onChange={(current, pageSize) => this.paginationChange(current, pageSize)}
															showTotal={(total, range) => `${range[0]} - ${range[1]} of ${total}`}
														/>
													</div>
												}
											</div>
											{/**************************** Show more Reviews -- End ***************************/}
										</div>
										{/********************** Reviews -- End ************************/}
									</div>
								</div>
							</div>
						</div> : <PageNotFound />}
			</React.Fragment>
		);
	}
}

const mapStateToProps = state => ({

});

export default (connect(mapStateToProps, actions)(ProductDetails));