
import React, { Component } from 'react';
import { withRouter } from "react-router-dom";
import swal from 'sweetalert';
import _ from 'lodash';
import { API_URL, IMAGE_URL } from '../../config/configs';
import axios from 'axios';
import { Select } from 'antd';
import iconHome from '../../../src/assets/images/icon-home.svg';
import iconCreditCard from '../../../src/assets/images/icon-credit-card-blue.svg';
import iconArrowDown from '../../../src/assets/images/icon-arrow-down.svg';
import iconCalender from '../../../src/assets/images/icon-calender-blue.svg';
import thankyouTick from '../../../src/assets/images/thankyou-tick.svg';
import { CardNumberElement, CardExpiryElement, CardCVCElement, injectStripe, Elements, StripeProvider } from 'react-stripe-elements';
import CF from '../common/commonFuntions'; // COMMON FUNCTIONS
// import $ from "jquery";
import deliveryTruck from '../../../src/assets/images/delivery-truck.svg';
// import moment from 'moment';
class CheckOut extends Component {
	constructor(props) {
		window.gtag('config', 'G-WB2B2WMR7Q', {
			'page_title': 'checkout',
			'page_path': '/checkOut'
		});
		window.fbq('track', 'checkout',
			{
				currency: 'ARS',
				content_type: 'product'
			}
		);
		super(props);
		this.state = {
			name: "",
			addressLine1: "",
			addressLine2: "",
			city: "",
			state: "",
			zipCode: "",
			phone: "",
			errors: {},
			listofStates: [],
			listofStates1: [],
			selectedState: [],
			selectedState1: '',
			shipping: false,
			details: [],
			tax: '',
			shippingtax: '',
			orderCount: '',
			transport: false,
			id: '',
			pin: '',
			shippingName: '',
			localProducts: [],
			productCount: '',
			common: true,
			transportId: '',
			number: '',
			email: '',
			expiry: '',
			cvv: '',
			empty: '',
			success: false,
			paymentSuccess: false,
			orderNumber: '',
			discount: '',
			amount: '',
			availability: '',
			options: [],
			optionSelected: '',
			visibleButton: false,
			paymentsVisible: {},
			mercadoPayId: '',
			mercaSuccess: false,
			mercaFailure: false,
			mercaPending: false,
			merca: 'failure',
			commission: '',
			mercadoCommission: '',
			stripeCommission: '',
			selectedRadioButton: ''
		}
	};
	//********************** min-orderAmount ************************************//
	async minOrderAmount(amount) {
		const response = await CF.callApi('/order/minOrderAmount', '', 'get', false, true);
		if (response.status === 1) {
			if (amount < response.data.minOrderAmount) {
				CF.showInfoMessage('productos Agotado')
				// CF.showInfoMessage(`Para ir a la caja, el valor mínimo del carrito debe ser $${response.data.minOrderAmount}`)
				this.props.history.push('/cart')
			}
		}

	}

	async componentDidMount() {
		window.scrollTo(0, 0);
		var withOutTokenProducts = localStorage.getItem("withOutToken")
		withOutTokenProducts = JSON.parse(withOutTokenProducts);
		let CalproductCount = withOutTokenProducts && Array.isArray(withOutTokenProducts) && withOutTokenProducts.length ? withOutTokenProducts.map(list => { return list._id }) : ""
		let body = { "productIds": CalproductCount }
		const response = await CF.callApi('/products/cartProductDetails', body, 'post', false, true);
		if (response.status === 1) {
			const result = withOutTokenProducts.filter(local => {
				response.data.product.filter(cart => {
					if (local._id === cart._id) {
						if (local.productCount > cart.stockQuantity) {
							local.stockQuantity = cart.publish ? cart.stockQuantity : 0;
							local.productCount = cart.stockQuantity;
							local.increasedStock = false
						}
						if (cart.stockQuantity > local.stockQuantity) {
							local.stockQuantity = cart.publish ? cart.stockQuantity : 0;
							local.increasedStock = true;
							local.isValidButton = false
						}
						else {
							local.increasedStock = false;
							local.stockQuantity = cart.publish === true ? cart.stockQuantity : 0;
						}

					}
				});
				if (local.stockQuantity && local.productCount >= local.minQuantity) return local;
				// && local.productCount >= local.minQuantity
			});
			localStorage.setItem('withOutToken', JSON.stringify(result));
			let mercadoPagoUrl = this.props.location.search.split('&');
			if (mercadoPagoUrl[1] === 'collection_status=pending') {
				// CF.showPopUp('Transaction is in Pending,Please try after Sometime')
				this.setState({ mercaSuccess: true });
			}
			if (mercadoPagoUrl[1] === 'collection_status=success') this.setState({ mercaSuccess: true });
			if (mercadoPagoUrl[1] === 'collection_status=failure') {
				CF.showPopUp('Transaction Failure,Please try Again')
				this.setState({ mercaFailure: true });
			}
			this.setState({ availability: response.data && response.data.orderDelivery ? response.data.orderDelivery.orderDelivery : '' })
		}
		this.getStates();
		this.getTransport();
		var productCount = withOutTokenProducts && Array.isArray(withOutTokenProducts) && withOutTokenProducts.length ? withOutTokenProducts.map((each) => { return each.productCount }) : ''
		const totalProductCount = productCount && productCount.length ? productCount.reduce((a, c) => a + c, 0) : 0
		this.setState({ productCount: totalProductCount, localProducts: withOutTokenProducts, visibleButton: false })
		this.getDefaultShippingAddress()
		this.getDiscounts();
		this.getOptions();
		const script = document.createElement("script");
		script.src = "https://js.stripe.com/v3/";
		script.async = true;
		document.body.appendChild(script);

	}



	getStates = (value) => {
		let token = localStorage.getItem('token')
		var body = { searchText: value }
		axios({
			method: 'post',
			url: API_URL + '/states/statesDropDown',
			headers: {
				'Content-Type': 'application/json',
				Authorization: token
			},
			data: body
		}).then(response => {
			if (response.data.status === 1) {
				var data = response.data.data;
				this.setState({ listofStates: data, selectedState: 'Seleccione provincia' })
			}
			// else if (response.data.status === 0) {
			// 	CF.showErrorMessage("Sesión expirada.");
			// 	this.tokenExpired();
			// }
		})
	}

	getTransport = (value) => {
		let token = localStorage.getItem('token')
		var body = { type: 'shippingName', searchText: value }
		axios({
			method: 'post',
			url: API_URL + '/order/getTransport',
			headers: {
				'Content-Type': 'application/json',
				Authorization: token
			},
			data: body
		}).then(response => {
			if (response.data.status === 1) {
				var data = response.data.data;
				data.unshift({ _id: '', shippingName: 'Seleccionar transporte' })
				this.setState({ listofStates1: data })
			}
		})
	}

	async getDefaultShippingAddress() {
		const response = await CF.callApi('/user/getShippingAddress', {}, 'get', false, true);
		if (response.data) {
			const { data } = response;
			this.setState({ addressLine1: data.addressLine1, addressLine2: data.addressLine2, city: data.city, name: data.name, phone: data.phone, state: data.state, zipCode: data.zipCode, pin: data.pin });
		} else if (response.message === "Invalid token.") {
			CF.showErrorMessage("Sesión expirada.");
			this.props.history.push('/cart');
			const empty = 0;
			this.setState({ addressLine1: '', addressLine2: '', city: '', name: '', phone: '', state: '', zipCode: '', pin: '', empty });
		}
		else {
			const empty = 0;
			this.setState({ addressLine1: '', addressLine2: '', city: '', name: '', phone: '', state: '', zipCode: '', pin: '', empty });
		}
	}


	//******************** validation for shipping addresss*********************** */
	validateCheckOut() {
		let { name, addressLine1, city, zipCode, phone, state, pin } = this.state;
		let errors = {};
		let formIsValid = true;
		if (!name || name.trim() === '') {
			formIsValid = false;
			errors["name"] = "Se requiere el nombre";
		}
		if (!addressLine1 || addressLine1.trim() === '') {
			formIsValid = false;
			errors["addressLine1"] = "Dirección 1 es obligatoria";
		}
		if ((_.isEmpty(state) && state !== 'Seleccione provincia' && state !== undefined)) {
			// || (state === undefined && state !== 'Seleccione provincia')
			formIsValid = false;
			errors["state"] = "Se requiere provincia";
		}
		if (!city || city.trim() === '') {
			formIsValid = false;
			errors["city"] = "Se requiere ciudad";
		}
		if (!zipCode) {
			formIsValid = false;
			errors["zipCode"] = "Se requiere código postal";
		}
		else if (!_.isEmpty(zipCode)) {
			var patternForZipCode = new RegExp(/^[0-9]{4}(?:-[0-9]{4})?$/);
			if (!patternForZipCode.test(zipCode)) {
				formIsValid = false;
				errors["zipCode"] = "Código postal no válido";
			}
			// else {
			// 	formIsValid = true;
			// }
		}
		if (!phone) {
			formIsValid = false;
			errors["phone"] = "Se requiere número de teléfono";
		}
		else if (!_.isEmpty(phone)) {
			var re = /^\d{8,20}$/;
			if (!re.test(phone)) {
				formIsValid = false;
				errors["phone"] = "Número invalido";
			}
			// else {
			// 	formIsValid = true;
			// }
		}
		if (!pin) {
			formIsValid = false;
			errors["pin"] = "DNI es obligatorio";
		}
		else if (!_.isEmpty(pin)) {
			var rex = /^\d{7,8}$/;
			if (!rex.test(pin)) {
				formIsValid = false;
				errors["pin"] = "Número invalido";
			}
			// else {
			// 	formIsValid = true;
			// }
		}
		this.setState({ errors: errors });
		return formIsValid;
	}

	//******************** handle change for shipping addresss *************************/
	handleChange = (event) => {
		this.setState({ [event.target.name]: event.target.value });
		if (event.target.value) {
			if (event.target.name === 'zipCode') {
				var x = event.target.value.replace(/\D/g, '').match(/(\d{0,2})(\d{0,2})/);
				let zipcode = x[1] + x[2];
				this.setState({ [event.target.name]: zipcode, errors: "" })
			}
			if (event.target.name === 'phone') {
				var y = event.target.value.replace(/\D/g, '').match(/(\d{0,8})(\d{0,8})(\d{0,4})/);
				let phone = y[1] + y[2] + y[3];
				this.setState({ [event.target.name]: phone, errors: "" })
			}
			if (event.target.name === 'pin') {
				var z = event.target.value.replace(/\D/g, '').match(/(\d{0,4})(\d{0,4})/);
				let pin = z[1] + z[2];
				this.setState({ [event.target.name]: pin, errors: "" })
			}
			this.setState({
				errors: Object.assign(this.state.errors, { [event.target.name]: "" })
			});
		}
	}

	placeOrderShipping() {
		if (this.validateCheckOut()) {
			let token = localStorage.getItem("token")
			let { name, addressLine1, addressLine2, city, selectedState, state, zipCode, phone, pin } = this.state;
			var body = { isShipping: true, shippingAddress: { name: name, addressLine1: addressLine1, addressLine2: addressLine2, city: city, state: state === '' ? selectedState : state, zipCode: zipCode, phone: phone, pin: pin } }
			axios({
				"method": "post",
				url: API_URL + '/order/placeOrder',
				headers: {
					"Content-Type": "application/json",
					'Authorization': token
				},
				data: body
			})
				.then(response => {
					if (response.data.status === 1) {
						var { data } = response
						this.setState({ id: data.data._id })
						this.setState({ shipping: true })
						swal("Detalles de envío agregados con éxito", '', 'success');
						// $('#collapse transport').on('focus', function () {
						// 	document.body.scrollTop = $(this).offset().top;
						// });
					}
					else {
						CF.showErrorMessage(response.data.message === "Invalid token." ? "Session Expired." : response.data.message);
						this.tokenExpired();
					}
				})
				.catch(err => console.log('err', err))
		}
	}

	placeOrderTranport = () => {
		let token = localStorage.getItem("token")
		let { selectedState1, id, common } = this.state;
		var body = { isTransport: true, transportId: selectedState1, orderId: id }
		common === true ? body["transportType"] = "Enviar a sucursal" : body["transportType"] = "Enviar a domicilio"
		axios({
			"method": "post",
			url: API_URL + '/order/placeOrder',
			headers: {
				"Content-Type": "application/json",
				'Authorization': token
			},
			data: body
		})
			.then(response => {
				if (response.data.status === 1) {
					window.scrollTo(0, 0);
					localStorage.setItem('transportId', response.data.data._id)
					this.defaultPayments();
					swal("Detalles de transporte agregados con éxito", '', 'success');
				}
				else {
					CF.showErrorMessage(response.data.message === "Invalid token." ? "Sesión expirada." : response.data.message);
					this.tokenExpired();
				}
			})
			.catch(err => console.log('err', err))
	}


	addShippingAddress() {
		if (this.validateCheckOut()) {
			let { name, addressLine1, addressLine2, city, selectedState, zipCode, phone, pin } = this.state;
			var body = { name: name, addressLine1: addressLine1, addressLine2: addressLine2, city: city, state: selectedState, zipCode: zipCode, phone: phone, pin: pin }
			let token = localStorage.getItem("token");
			axios({
				"method": "post",
				url: API_URL + '/user/addShippingAddress',
				headers: {
					"Content-Type": "application/json",
					'Authorization': token
				},
				data: body
			})
				.then(response => {
					if (response.data.status === 1) {
						this.placeOrderShipping();
					}
					else {
						CF.showErrorMessage(response.data.message === "Invalid token." ? "Sesión expirada." : response.data.message);
						this.tokenExpired();
					}
				})
				.catch(err => console.log('err', err))
		}
	}
	//************************ getting all discounts ****************************/
	async getDiscounts() {
		let withOutTokenProducts = localStorage.getItem("withOutToken")
		withOutTokenProducts = JSON.parse(withOutTokenProducts)
		let CalproductCount = withOutTokenProducts && Array.isArray(withOutTokenProducts) && withOutTokenProducts.length ? withOutTokenProducts.map(list => {
			if (list.stockQuantity) return list.productCount
		}) : ""
		let CalsalePrice = withOutTokenProducts && Array.isArray(withOutTokenProducts) && withOutTokenProducts.length ? withOutTokenProducts.map(list => { return list.salePrice }) : "";
		let amount = 0;
		if (!_.isEmpty(CalproductCount)) {
			amount = CalproductCount.map(e => Number.isInteger(e) ? e : 0).reduce((a, c, i) => a + c * CalsalePrice[i], 0);
		}
		this.setState({ amount })
		const body = { "amount": amount }
		const response = await CF.callApi('/discount/getDiscount', body, 'post', false, true);
		if (response.status === 1) {
			let { data } = response;
			let { discount } = data
			this.setState({ discount })
		}
		else this.setState({ discount: 0 })
		this.minOrderAmount(amount);
	}
	// *********************************** END Discount **********************************//

	tokenExpired = async () => {
		let body = { "token": localStorage.getItem('token') }
		const response = await CF.callApi('/auth/logOut', body, 'post', false, true);
		if (response.status === 1) {
			localStorage.removeItem('token');
			this.props.history.replace('/login');
		}
	}

	continueShopping = () => {
		this.props.history.push('/');
		localStorage.removeItem('transportId')
		localStorage.removeItem('withOutToken')
		localStorage.removeItem('orderId')
		localStorage.removeItem('mercadoPayId')
		localStorage.removeItem('email')
		this.setState({ paymentSuccess: false, mercadoPayId: '', mercaSuccess: false, merca: 'failure' })
	}

	// *********************************** Stripe Payment **********************************//
	getpaymentSuccess(callback, orderId, mercaId, name) {
		localStorage.setItem('mercadoPayId', mercaId)
		this.setState({
			paymentSuccess: true,
			orderNumber: orderId,
			mercadoPayId: mercaId,
			merca: name
		})
	}
	//********************************* selection for customer order info ******************************//
	getOptions = async () => {
		const response = await CF.callApi('/options/optionsList', {}, 'get', false, true);
		if (response.status === 1) this.setState({ options: response.data });
	}

	option(e) {
		this.setState({ optionSelected: e.target.id, errors: [] })
	}

	validateOptions = () => {
		let { optionSelected } = this.state;
		let errors = {};
		let formIsValid = true;
		if (!optionSelected) {
			formIsValid = false;
			errors["optionSelected"] = "Por favor seleccioná una opción para poder continuar.";
		}
		this.setState({ errors: errors });
		return formIsValid;
	}


	//********************************* MercadoPago Payment ********************************/
	mercadoPagoPayment = async () => {
		let { pin } = this.state;
		var productsArray = [];
		var items = [];
		var itemsFb = [];
		var withOutTokenProducts = localStorage.getItem("withOutToken")
		withOutTokenProducts = JSON.parse(withOutTokenProducts)
		if (withOutTokenProducts && withOutTokenProducts.length > 0) {
			withOutTokenProducts.map((each) => {
				productsArray.push({
					productCount: each.productCount, productId: each.productId, SKU: each.SKU
				})
				items.push({
					id: each._id, name: each.productName, quantity: each.productCount, price: each.salePrice,
				})
				itemsFb.push({
					id: each._id, quantity: each.productCount,
				})
				return ''
			})
		}
		let body = { "isPayment": true, "paymentType": "Mercado Pago", orderId: localStorage.getItem('transportId'), 'productsArray': productsArray, 'identificationEmailId': localStorage.getItem('email') ? localStorage.getItem('email') : '', 'identificationNumber': pin, 'identificationType': 'DNI' }
		const response = await CF.callApi('/order/placeOrder', body, 'post', false, true);
		if (response.status === 1) {
			window.open(response.data.init_point, "_self");
			this.getpaymentSuccess("", '', response.data.orderId, "success", 'merca')
			window.gtag('event', 'purchase', {
				"transaction_id": localStorage.getItem('transportId'),
				"value": this.state.amount,
				"currency": "ARS",
				"tax": 0,
				"shipping": 0,
				items
			});
			window.gtag('event', 'conversion', {
				'send_to': 'AW-692710707/aZPdCOr2hbUBELPap8oC',
				'value': this.props.total,
				'currency': 'ARS',
				'transaction_id': localStorage.getItem('transportId')
			});
			window.fbq('track', 'Purchase',
				{
					value: this.state.amount,
					currency: 'ARS',
					contents: itemsFb,
					content_type: 'product'
				}
			);
		}
		else {
			this.getpaymentSuccess("", '', "", 'failure')
			CF.showErrorMessage(response.message === "Invalid token." ? "Sesión expirada." : response.message);
			this.tokenExpired();
		}
	}


	BankTransfer = (type) => {
		let token = localStorage.getItem("token")
		var productsArray = [];
		var items = [];
		var itemsFb = [];
		var withOutTokenProducts = localStorage.getItem("withOutToken")
		withOutTokenProducts = JSON.parse(withOutTokenProducts)
		if (withOutTokenProducts && withOutTokenProducts.length > 0) {
			withOutTokenProducts.map((each) => {
				productsArray.push({
					productCount: each.productCount, productId: each.productId, SKU: each.SKU
				})
				items.push({
					id: each._id, name: each.productName, quantity: each.productCount, price: each.salePrice,
				})
				itemsFb.push({
					id: each._id, quantity: each.productCount,
				})
				return '';
			})
		}
		var body = { isPayment: true, orderId: localStorage.getItem('transportId'), productsArray: productsArray }
		if (type === "Bank") body["paymentType"] = "Depósito/Transferencia Bancaria";
		if (type === "Rapigo") body["paymentType"] = "Pago Fácil / Rapipago / CobroExpress";
		axios({
			"method": "post",
			url: API_URL + '/order/placeOrder',
			headers: {
				"Content-Type": "application/json",
				'Authorization': token
			},
			data: body
		})
			.then(response => {
				if (response.data.status === 1) {
					this.setState({
						orderId: response.data.data.orderId,
						success: true
					})
					localStorage.removeItem('withOutToken')
					this.getpaymentSuccess("", this.state.orderId, '', '')
					window.gtag('event', 'purchase', {
						"transaction_id": localStorage.getItem('transportId'),
						"value": this.state.amount,
						"currency": "ARS",
						"tax": 0,
						"shipping": 0,
						items
					});
					window.gtag('event', 'conversion', {
						'send_to': 'AW-692710707/aZPdCOr2hbUBELPap8oC',
						'value': this.props.total,
						'currency': 'ARS',
						'transaction_id': localStorage.getItem('transportId')
					});
					window.fbq('track', 'Purchase',
						{
							value: this.state.amount,
							currency: 'ARS',
							contents: items,
							content_type: 'product'
						}
					);
				}
				else {
					if (response.data.message === "Invalid token.") {
						CF.showErrorMessage("Sesión expirada.")
						this.tokenExpired();
					}
					else if (response.data.message !== "Invalid token.") {
						CF.showErrorMessage(response.data.message)
						this.tokenExpired();
					}
				}
			})
			.catch(err => console.log('payment err', err))
	}

	//*********************************** Selected Option Sending Back *********************************//
	selection = async () => {
		// this.setState({ optionSelected: e.target.id })
		let { id, optionSelected } = this.state;
		let body = {
			"isSelect": true,
			"orderId": id,
			"options": optionSelected
		}
		const response = await CF.callApi('/order/placeOrder', body, 'post', false, true);
		if (response.status === 1) this.setState({ visibleButton: true, transport: true, errors: [] }, () => this.placeOrderTranport())
		else this.validateOptions();
	}
	//*********************************** Default Payments to visible *********************************//
	defaultPayments = async () => {
		const response = await CF.callApi('/payments/getPayments', {}, 'get', false, true);
		this.setState({ paymentsVisible: response.data, mercadoCommission: response.data.mercadoCommission, stripeCommission: response.data.stripeCommission })
	}
	method = (value) => {
		this.setState({ commission: value })
	}

	radioButtons = (value) => {
		this.method(value.target.id)
		this.setState({ selectedRadioButton: value.target.id })
	}
	addWeekdays = (date, days) => {
		date.setDate(date.getDate());
		var counter = 0;
		if (days > 0) {
			while (counter < days) {
				date.setDate(date.getDate() + 1); // Add a day to get the date tomorrow
				var check = date.getDay(); // turns the date into a number (0 to 6)
				if (check === 0 || check === 6) {
					// Do nothing it's the weekend (0=Sun & 6=Sat)
				}
				else {
					counter++;  // It's a weekday so increase the counter
				}
			}
		}
		return date;
	}
	render() {
		let { name, addressLine1, city, zipCode, phone, state, errors, listofStates, listofStates1, selectedState, selectedState1, shipping, transport, localProducts, productCount, common, empty, paymentSuccess, orderNumber, pin, discount, amount, availability, options, optionSelected, paymentsVisible, mercadoPayId, mercaSuccess, commission, selectedRadioButton, mercadoCommission } = this.state;
		let discountApplied = (amount * discount) / 100;
		selectedState = selectedState ? selectedState : 'Select your state';
		let mercaId = localStorage.getItem('mercadoPayId');
		var date = this.addWeekdays(new Date(), availability);
		const optionsCheck = { year: 'numeric', month: 'long', day: 'numeric' };
		return (
			<div className="page-wrap">
				<div className="main mt-0">
					{/* <SideBar />  */}
					<div id="checkout" className="checkout-content">
						<div className="container">
							{((mercadoPayId === '' && (paymentSuccess === true || paymentSuccess === false)) || (mercaSuccess === true && paymentSuccess === true)) &&
								<React.Fragment>
									<h2 className="sub-title">Pasar por caja</h2>
									<div id="wizard-progress" className="wizard-progress">
										<ol className="step-indicator">
											<li className={shipping === true || mercaSuccess === true ? "complete" : ""}>
												<div className="step" />
												<div className="caption hidden-xs hidden-sm">Dirección de Envío</div>
											</li>
											<li className={transport === true || mercaSuccess === true ? "complete" : ""}>
												<div className="step" />
												<div className="caption hidden-xs hidden-sm">Transporte</div>
											</li>
											<li className={paymentSuccess === true || mercaSuccess === true ? "complete" : ""} >
												<div className="step" />
												<div className="caption hidden-xs hidden-sm">Método de pago</div>
											</li>
											<li className={paymentSuccess === true || mercaSuccess === true ? "complete" : ""}>
												<div className="step" />
												<div className="caption hidden-xs hidden-sm">Confirmar</div>
											</li>
										</ol>
									</div>
								</React.Fragment>}
							{
								paymentSuccess === false && mercaSuccess === false ?
									<div className="row">
										<div className="col-lg-7 col-md-12">
											<div id="accordion" className="checkout-process">
												<div className="card mb-20">
													<div className="card-header">
														<a className="card-link" data-toggle="collapse" href="#shippingaddress">
															<div className="title">
																<img src={iconHome} alt='' width="26px" height="22px" /> Dirección de Envío
                                                                </div>
															<span className="downarroBox"><img src={iconArrowDown} alt="arrow" width="14px" height="8px" /></span>
														</a>
													</div>
													<div id="shippingaddress" className={shipping ? "collapse shippingaddress" : "collapse show shippingaddress"} data-parent="#accordion">
														<div className="card-body">
															<form>
																<div className="row">
																	<div className="col-md-12">
																		<div className="form-group">
																			<input type="text" className="form-control" id="nameinput" placeholder="Nombre y Apellido" name="name" value={name} onChange={this.handleChange} />
																			<span className="error-msg">{errors.name}</span>
																		</div>
																	</div>
																	<div className="col-md-12">
																		<div className="form-group">
																			<input type="text" className="form-control" id="pininput" placeholder="DNI" name="pin" value={pin} onChange={this.handleChange} />
																			<span className="error-msg">{errors.pin}</span>
																		</div>
																	</div>
																	<div className="col-md-12">
																		<div className="form-group">
																			<input type="text" className="form-control" id="address1" placeholder="Ingrese su dirección" name="addressLine1" value={addressLine1} onChange={this.handleChange} />
																			<span className="error-msg">{errors.addressLine1}</span>
																		</div>
																	</div>
																	{/* <div className="col-md-12">
																		<div className="form-group">
																			<input type="text" className="form-control" id="address2" placeholder="Ingrese su dirección línea 2" name="addressLine2" value={addressLine2} onChange={this.handleChange} />
																		</div>
																	</div> */}
																	<div className="col-md-12">
																		<div className="form-group">
																			<input type="text" className="form-control" id="cityinput" placeholder="Ingresa tu ciudad" name="city" value={city} onChange={this.handleChange} />
																			<span className="error-msg">{errors.city}</span>
																		</div>
																	</div>
																</div>
																<div className="row">
																	<div className="col-md-6">
																		<div className="form-group quantitybox">
																			<Select
																				showSearch
																				value={state ? state : selectedState}

																				onChange={(selectedValue) =>
																					this.setState({
																						selectedState: selectedValue,
																						state: selectedValue,
																						errors: Object.assign(this.state.errors, {
																							selectedState: "", state: ''
																						})
																					})}
																				onInputKeyDown={this.getStates}
																				style={{ width: '100%' }}
																				filterOption={(input, option) =>
																					option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
																				}
																			>
																				{listofStates.map(item => (
																					<Select.Option key={item} value={item.province}>
																						{item.province}
																					</Select.Option>
																				))}
																			</Select>
																			<span className="error-msg">{errors.state}</span>
																		</div>
																	</div>
																	<div className="col-md-6">
																		<div className="form-group">
																			<input type="text" className="form-control" id="zipcode" placeholder="codigo postal" name="zipCode" value={zipCode} onChange={this.handleChange} />
																			<span className="error-msg">{errors.zipCode}</span>
																		</div>
																	</div>
																</div>
																<div className="row">
																	<div className="col-md-12">
																		<div className="form-group">
																			<input type="number" className="form-control" id="phoneinput" placeholder="Ingrese su numero celular" name="phone" value={phone} onChange={this.handleChange} />
																			<span className="error-msg">{errors.phone}</span>
																		</div>
																	</div>

																	<div className="col-md-12 text-right">
																		<div className="form-group">
																			{empty === 0 ? <button type="button" className="btn btn-primary" title="Next" onClick={() => { this.addShippingAddress() }}>Siguiente</button> :
																				<button type="button" className="btn btn-primary" title="Next" onClick={() => { this.placeOrderShipping() }}>Siguiente</button>}
																		</div>
																	</div>
																</div>
															</form>
														</div>
													</div>
												</div>

												{shipping === true ?
													(<div className="card mb-20">
														<div className="card-header">
															<a className="collapsed card-link" data-toggle="collapse" href="#transport">
																<div className="title">
																	<img src={iconCalender} alt="21px" width="22px" /> Transporte
                          </div>

																<span className="downarroBox"><img src={iconArrowDown} alt="arrow" width="14px" height="8px" /></span>
															</a>
														</div>
														<div id="transport" className={transport === false ? "collapse show transport" : "collapse transport"} data-parent="#accordion">
															<div className="card-body">
																<form>
																	<div className="row">
																		<div className="col-12 info-text">
																			<span className="mb-3 greentext">Abonará el costo del envío
																			al recibir su pedido.</span>
																		</div>
																		<div className="col-md-12">
																			<div className="form-group quantitybox">
																				<Select
																					showSearch
																					value={selectedState1}
																					onChange={(state1) => this.setState({ selectedState1: state1 })}
																					onInputKeyDown={(e) => this.getTransport(e, 'shippingName')}
																					style={{ width: '100%' }}
																					filterOption={(input, option) =>
																						option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
																					}
																				>
																					{listofStates1.map(item => (
																						<Select.Option key={item} value={item._id}>
																							{item.shippingName}
																						</Select.Option>
																					))}
																				</Select>

																			</div>
																		</div>
																		<div className="col-md-12 transpot-part">
																			<div className="radiobtn-nav-list form-inline mb-15">
																				<label className="radio-container">Enviar a sucursal
                                    <input type="radio" name="ship" value={true} checked={common ? true : false} onChange={(e) => this.setState({ common: e.target.value === "true" })} />
																					<span className="checkmark" />
																				</label>
																				<label className="radio-container">Enviar a domicilio
                                    <input type="radio" name="ship" value={false} checked={common ? false : true} onChange={(e) => this.setState({ common: e.target.value === "true" })} />
																					<span className="checkmark" />
																				</label>
																			</div>
																			{this.state.selectedState1.length > 0 ?
																				(options && options.question) &&
																				<div>
																					<p>{options.question}</p>
																					{options.answers.map((each, key) => {
																						return <div className="radiobtn-nav-list form-inline" key={key}>
																							<label className="radio-container">{each.answer}
																								<input type="radio" id={each.answer} value={each.answer} checked={optionSelected === each.answer && each.answer} onChange={(e) => this.option(e)} />
																								<span className="checkmark" />

																							</label>
																						</div>
																					})}
																					<span className="error-msg" style={{ color: 'red' }} >{errors.optionSelected}</span>
																					<div className="form-group text-right mt-3">
																						<button className="btn btn-primary" type='button' title="Next" onClick={() => { this.selection() }} >Siguiente</button>
																					</div>
																					{/* disabled={visibleButton === true ? false : true} */}

																					{/* <div className="form-group text-right">
																						<button className="btn btn-primary" type='button' title="Next" onClick={() => { this.setState({ transport: true }); this.placeOrderTranport() }} >Siguiente</button>
																					</div> */}
																				</div>
																				: ''}



																		</div>
																	</div>
																</form>
															</div>
														</div>
													</div>
													) : ""}

												{/************************************  transpor*************************************/}

												{transport === true && this.state.selectedState1.length > 0 && paymentsVisible ?
													<div className="card mb-20">
														<div className="card-header">
															<a className="card-link" data-toggle="collapse" href="#payment" aria-expanded="true">
																<div className="title">
																	<img src={iconCreditCard} alt="" width="22px" height="18px" /> Pagar
						</div>
																<span className="downarroBox"><img src={iconArrowDown} alt="arrow" width="14px" height="8px" /></span>
															</a>
														</div>
														<form>
															<div id="payment" className="payment payment-checkout show collapse" data-parent="#accordion">
																<div className="card-body">
																	<div className="radiobtn-nav-list form-inline mb-0">
																		{paymentsVisible && paymentsVisible.stripe === true && <label className="radio-container"> Tarjeta de crédito (1 pago)
              <input type="radio" id="Stripe" name="payment" value="Stripe" checked={selectedRadioButton === "Stripe" ? true : false} onChange={(e) => this.radioButtons(e)} />
																			<span className="checkmark" />
																		</label>}
																		{paymentsVisible && paymentsVisible.mercado === true &&
																			<label className="radio-container">MercadoPago
         <input type="radio" id="MercadoPago" name="payment" value="MercadoPago" checked={selectedRadioButton === "MercadoPago" ? true : false} onChange={(e) => this.radioButtons(e)} />
																				<span className="checkmark" />
																			</label>}
																		{paymentsVisible && paymentsVisible.bank === true &&
																			<label className="radio-container">Depósito/Transferencia Bancaria
         <input type="radio" id="Bank" name="payment" value="Bank" checked={selectedRadioButton === "Bank" ? true : false} onChange={(e) => this.radioButtons(e)} />
																				<span className="checkmark" />
																			</label>}
																		{paymentsVisible && paymentsVisible.rapigo === true &&
																			<label className="radio-container">Pago Fácil/Rapipago/CobroExpress
         <input type="radio" id="Rapigo" name="payment" value="Rapigo" checked={selectedRadioButton === "Rapigo" ? true : false} onChange={(e) => this.radioButtons(e)} />
																				<span className="checkmark" />
																			</label>}
																	</div>

																	{selectedRadioButton === "Stripe" ?
																		<StripeProvider apiKey={paymentsVisible.stripePk}>
																			<CheckoutForm getpaymentSuccess={this.getpaymentSuccess.bind(this)} {...this.props} paymentsVisible={paymentsVisible} total={amount} pin={pin} />
																		</StripeProvider> : ''}

																	<div className="">
																		{selectedRadioButton === "Bank" ?
																			<div className="text-right">
																				<div className="form-group">
																					<button className="btn btn-primary" type="button" title="Next" onClick={() => { this.BankTransfer("Bank") }}>Siguiente</button>
																				</div>
																			</div> : ""}
																		{selectedRadioButton === "MercadoPago" ?
																			<div className="row">
																				<div className="mercadopago col-12">
																					<div className="info-text">
																						<div className="form-group">
																							<span className="greentext">Abonará un {mercadoCommission}% extra por costos administrativos</span>
																						</div>
																					</div>
																				</div>
																				<div className="mercadopago col-12">
																					<div className="text-right">
																						<div className="form-group">
																							<button className="btn btn-primary" type="button" title="Next" onClick={() => { this.mercadoPagoPayment() }}>Siguiente</button>
																						</div>
																					</div>
																				</div>
																			</div> : ""}
																		{selectedRadioButton === "Rapigo" ?
																			<div className="text-right">
																				<div className="form-group">
																					<button className="btn btn-primary" type="button" title="Next" onClick={() => { this.BankTransfer("Rapigo") }}>Siguiente</button>
																				</div>
																			</div> : ""}

																	</div>
																</div>
															</div>
														</form >
													</div > : ''}
											</div>
										</div>

										<div className="col-lg-5 col-md-12">
											<div className="orderSummary">
												{availability &&
													<div className="mb-3">
														<span className="greentext delivery-title mb-10"><img src={deliveryTruck} alt="Delivery Time" width="22px" height="14px" className="mr-3" />
															{('Será entregado a su transporte el') + " " + (date && date.toLocaleDateString('es-eS', optionsCheck))}
															{/* (date && (moment(date).format('D MMM YY'))) */}
														</span>
													</div>}
												<h4 className="title mb-30"><span className="icon-icon-invoice" />Resumen del pedido</h4>
												<div className="itemlist">
													<div className="spanBox mb-20">
														<span>Articulos totales ({productCount})</span>
														<span>Precio</span>
													</div>
													<ul className="list-group smooth-scroll">
														{(localProducts) ? (
															localProducts.map((details, key) => {
																let photo = details && details.productImage ? IMAGE_URL + details.productImage.slice(0, -4) + "-th.jpg" : IMAGE_URL + "product.jpg";
																return (
																	<li className="list-group-item" key={key}>
																		<div className="imgBox"><img src={photo} alt={details.productName.substring(0, 5) + '......'} width="43px" height="43px" className="img-fluid" /></div>
																		<div className="item-name">{details.productName} </div>
																		<div className="price">${details.salePrice}</div>
																	</li>
																)
															})) : null
														}
													</ul>
												</div>
												<div className="itemsTotal mt-30">
													<div className="spanBox">
														<span><strong>Total Estimado</strong></span>
														<span>${amount}</span>
													</div>
													<div className="spanBox">
														<span><strong>Descuento</strong></span>
														<span>${discountApplied.toFixed(2)}</span>
													</div>

													{((commission === "Stripe" || commission === "MercadoPago") && (paymentsVisible) && (paymentsVisible.stripeCommission || paymentsVisible.mercadoCommission)) &&
														<div className="spanBox">
															<span><strong>Comisión de {((((commission === 'Stripe') && commission !== 'MercadoPago') && 'Stripe') || (((commission === 'MercadoPago') && (commission !== 'Stripe')) && 'Medio de pago'))}
															</strong></span>

															<span>${(((commission === 'Stripe') && (commission !== 'MercadoPago') && (paymentsVisible.stripeCommission * (amount - discountApplied) / 100).toFixed(2)) || ((commission === 'MercadoPago') && (commission !== 'Stripe') && (paymentsVisible.mercadoCommission * (amount - discountApplied) / 100).toFixed(2)))}</span>
														</div>}

													<div className="spanBox">
														<span>Impuesto estimado</span>
														<span>$0.00</span>
													</div>

													{(commission && (commission === "Stripe" || commission === "MercadoPago") && paymentsVisible && (paymentsVisible.stripeCommission
														|| paymentsVisible.mercadoCommission)) ?
														<div className="spanBox">
															<span><strong>Total</strong></span>
															<span>${(((commission === 'Stripe') && (commission !== 'MercadoPago') && ((paymentsVisible.stripeCommission * (amount - discountApplied) / 100) + (amount - discountApplied)).toFixed(2)) || (((commission === 'MercadoPago') && (commission !== 'Stripe') && ((paymentsVisible.mercadoCommission * (amount - discountApplied) / 100) + (amount - discountApplied)).toFixed(2))))}</span>
														</div>
														:
														<div className="spanBox">
															<span><strong>Total</strong></span>
															<span>${(amount - discountApplied).toFixed(2)}</span>
														</div>}


												</div>
											</div>
										</div>

									</div>
									:
									((paymentSuccess === true && mercadoPayId === '') || (mercaSuccess === true)) ?
										(<div className="col-lg-12 col-md-12">
											<div id="confirmBox mt-20" className="confirmBox text-center">
												<img src={thankyouTick} alt="Thank you" width="208px" height="208px" className="img-fluid" />
												{/* Thank you for your order. */}
												<h2>Gracias por su orden.</h2>
												{/* Your order has been placed successfully with MercadoMayorista.com */}
												<p>Su pedido ha sido realizado con éxito en MercadoMayorista.com</p>
												{/* Order Number is: */}
												<p>Número de pedido: <strong> {orderNumber ? orderNumber : (mercaId ? mercaId : '')}</strong></p>
												<p>Le llegará un correo electrónico con las instrucciones y el resumen de su pedido</p>
												<div className="btnBox mt-50">
													<a className="btn btn-primary" title="Continue Shopping" onClick={this.continueShopping}
													>Seguir comprando</a>
												</div>
											</div>
										</div>) : ''
							}
						</div>
					</div>
					{/* Checkout Content End */}
				</div>
			</div >
		);
	}
}


class _CardForm extends Component {
	constructor(props) {
		super(props)
		this.state = {
			tokenId: {},
			common: '',
			success: false,
			orderId: '',
			popUp: false,
			stockDetails: [],
			noLength: [],
			noStockQuantity: [],
			typeVariable: true,
			payEmail: localStorage.getItem('email') ? localStorage.getItem('email') : '',
			payNumber: this.props.pin ? this.props.pin : '',
			errors: {},
			mercadoPayId: '',
		}
	}

	tokenExpired = () => {
		localStorage.removeItem('token')
		this.props.history.replace('/login');
	}

	tokenGeneration = (payload) => {
		var token_id = payload.token;
		if (!_.isEmpty(token_id)) {
			var withOutTokenProducts = localStorage.getItem("withOutToken")
			withOutTokenProducts = JSON.parse(withOutTokenProducts)
			var productsArray = [];
			var items = [];
			var itemsFb = [];
			if (withOutTokenProducts && withOutTokenProducts.length > 0) {
				withOutTokenProducts.map((each) => {
					productsArray.push({
						productCount: each.productCount, productId: each.productId, SKU: each.SKU
					})
					items.push({
						id: each._id, name: each.productName, quantity: each.productCount, price: each.salePrice
					})
					itemsFb.push({
						id: each._id, quantity: each.productCount
					})
					return '';
				})
			}
			// var { common } = this.state
			let token = localStorage.getItem("token")
			var body = { isPayment: true, orderId: localStorage.getItem('transportId'), productsArray: productsArray, paymentType: "Tarjeta de crédito (1 pago)", stripeToken: token_id ? token_id.id : '', funding: token_id && token_id.card.funding ? token_id.card.funding : '' }
			axios({
				"method": "post",
				url: API_URL + '/order/placeOrder',
				headers: {
					"Content-Type": "application/json",
					'Authorization': token
				},
				data: body
			})
				.then(response => {
					if (response.data.status === 1) {
						this.setState({ orderId: response.data.data.orderId, success: true })
						localStorage.removeItem('withOutToken')
						this.props.getpaymentSuccess(payload, this.state.orderId, '', '');
						window.gtag('event', 'purchase', {
							"transaction_id": localStorage.getItem('transportId'),
							"value": this.props.total,
							"currency": "ARS",
							"tax": 0,
							"shipping": 0,
							items
						});
						window.gtag('event', 'conversion', {
							'send_to': 'AW-692710707/aZPdCOr2hbUBELPap8oC',
							'value': this.props.total,
							'currency': 'ARS',
							'transaction_id': localStorage.getItem('transportId')
						});
						window.fbq('track', 'Purchase',
							{
								value: this.props.total,
								currency: 'ARS',
								contents: itemsFb,
								content_type: 'product'
							}

						);
					}
					else {
						if (response.data.message === "Invalid token.") {
							CF.showErrorMessage("Sesión expirada.")
							this.tokenExpired();
						}
						else if (response.data.message !== "Invalid token.") {
							CF.showErrorMessage(response.data.message)
							this.tokenExpired();
						}
					}
				})
				.catch(err => console.log('payment err', err))
		}
		else if (payload.error.code === "invalid_number") {
			swal("Por favor, ingrese correctamente los datos de su tarjeta", "", 'error')
		} else {
			swal("Por favor, ingrese los datos de su tarjeta", "", 'error')
		}
	}

	render() {
		let stripeCommission = this.props.paymentsVisible ? this.props.paymentsVisible.stripeCommission : '';
		return (
			<React.Fragment>
				<div className="row">
					<div className="col-md-12">
						<div className="form-group icon-form-group">
							<label>
								<span className="icon-icon-credit-card-black input-icon"></span>
								<CardNumberElement
									className="form-control"
									placeholder="Card Number"
								/>
							</label>
						</div>
					</div>
				</div>
				<div className="row">
					<div className="col-md-4">
						<div className="form-group icon-form-group">
							<label>
								<span className="icon-icon-calendar input-icon"></span>
								<CardExpiryElement
									className="form-control"
								/>
							</label>
						</div>
					</div>
					<div className="col-md-4">
						<div className="form-group icon-form-group">
							<label>
								<span className="icon-icon-locked input-icon"></span>
								<CardCVCElement
									className="form-control" />
							</label>
						</div>
					</div>
					<div className="col-md-12">
						<div className="form-group info-text">
							{stripeCommission && <span className="greentext">Abonará un {stripeCommission}% extra por costos administrativos</span>}
						</div>
					</div>
					<div className="col-md-12 text-right">
						<div className="form-group">
							<button className="btn btn-primary" type="button" title="Next" onClick={() => { this.props.stripe.createToken().then(payload => this.tokenGeneration(payload)) }}>Siguiente</button>
						</div>
					</div>
				</div>
			</React.Fragment>
		)
	}
}

const CardForm = injectStripe(_CardForm)

class CheckoutForm extends React.Component {
	render() {
		return (
			<div className="Checkout">
				<Elements>
					<CardForm  {...this.props} />
				</Elements>
			</div>
		)
	}
}

export default withRouter(CheckOut);
