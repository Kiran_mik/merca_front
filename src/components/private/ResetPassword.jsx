
import React from 'react';
import queryString from 'query-string';
import Form from '../reusable/form';
import CF from '../common/commonFuntions';
import EM from '../common/errorMessages';
class ResetPassword extends Form {

	state = {
		formData: {
			password: "",
			confirmPassword: "",
		},
		errors: {},
		isFormValid: true,
	};

	async doSubmit() {
		window.gtag('config', 'G-WB2B2WMR7Q', {
			'page_title': 'Reset Page',
			'page_path': '/resetPassword'
		});
		window.fbq('track', 'ResetPassWord',
		);
		const { token } = queryString.parse(window.location.search);
		const { password } = this.state.formData;
		const body = { password, token };
		const response = await CF.callApi('/auth/resetPassword', body, 'post');
		if (response.status === 1) {
			this.props.history.replace("/login");
			CF.showSuccessMessage(EM._234);
		}
		if (response.status === 0 && response.statusCode === 416) CF.showErrorMessage(EM._416);
	}

	render() {
		let { password, confirmPassword } = this.state.formData;
		return (
			<div>
				<div className="main">
					<div id="page-content-wrapper">
						<form className="login-wrap" onSubmit={(event) => this.handleSubmit(event)}>
							<h2 className="main-title mb-25">Restablecer la contraseña</h2>
							<div className="row">
								{this.renderInput('col-12', true, 'password', 'password', password, 'Contraseña')}
								{this.renderInput('col-12', false, 'confirmPassword', 'password', confirmPassword, 'Confirmar contraseña')}
							</div>
							{this.renderButton('btnbox d-flex justify-content-center mt-38 mb-50', 'btn btn-block btn-primary', 'Enviar')}
						</form>
					</div>
				</div>
			</div>
		);
	}
}

export default ResetPassword;




