import React from 'react';

const CheckBox = ({ blockClass, checkBoxClass, lable, name, checked, onChange }) => {
    return (
        <div className={blockClass}>
            <div className={checkBoxClass}>
                <input
                    type="checkbox"
                    className="custom-control-input"
                    id={name}
                    name={name}
                    checked={checked}
                    onChange={onChange} />
                <label className="custom-control-label" htmlFor={name}>{lable}</label>
            </div>
        </div>
    );
}

export default CheckBox;