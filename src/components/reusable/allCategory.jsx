import React from 'react';
import { Link } from 'react-router-dom';
import { IMAGE_URL } from '../../config/configs';
import Button from '../common/Button';
import _ from 'lodash';


const AllCategory = ({ allCategory, categoryName, url, categoryName1, categoryName2, categoryName3, }) => {
    return (
        <React.Fragment>
            {(!_.isEmpty(allCategory)) ?
                <React.Fragment>
                    <h3 className="sub-title" >{`Todo en ${categoryName}`}</h3>
                    <div className="item all-product-list">
                        {allCategory.map((each, i) => {
                            const salePrice = (each.salePrice) ? "$" + each.salePrice : "";
                            let regularPrice = (each.price) ? "$" + each.price : "";
                            let photo = each && each.productImage ? IMAGE_URL + each.productImage.slice(0, -4) + "-sm.jpg" : IMAGE_URL + "product.jpg";
                            let image = photo;
                            return (<div className={each.stockQuantity > 0 ? "product-itemBox" : "product-itemBox blur_image"} key={i} >
                                <Link to={{ pathname: `/categorias/${each.customUrl}`, state: { url: url } }}>
                                    <div className="imgBox" >
                                        {/* <LazyLoadImage
                                    className="img-fluid"
                                    alt={each.productName}
                                    height={'23px'}
                                    delayTime={100}
                                    src={image} // use normal <img> attributes as props
                                    width={"81px"} /> */}
                                        <img src={image} alt={each.productName} width="81px" height="23px" className="img-fluid" />
                                    </div>
                                </Link>
                                <div className="pro-details">
                                    <Link to={{ pathname: `/categorias/${each.customUrl}`, state: { url: url, categoryName1: categoryName1, categoryName2: categoryName2, categoryName3: categoryName3 } }}>
                                        <h6 className="product-name">{each.productName} <br /></h6>
                                    </Link>
                                    <h4> {salePrice === "" ? regularPrice : salePrice}
                                        <strike>{salePrice === regularPrice ? "" : regularPrice}</strike></h4>
                                    <Button data={each} />
                                </div>
                            </div>)
                        })}
                    </div>  </React.Fragment> : ''}
        </React.Fragment>
    );
}

export default AllCategory;