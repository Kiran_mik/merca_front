import React from 'react';
import { Link } from "react-router-dom";
import { IMAGE_URL } from '../../config/configs';
import Button from '../common/Button';
import OwlCarousel from 'react-owl-carousel';
// import { LazyLoadImage } from 'react-lazy-load-image-component';
import _ from 'lodash';

const CommonOwl = ({ productListing, productSlider, subCategoryName, onClick, homePage, products, directProducts, message, categoryName, url, categoryName1, categoryName2, categoryName3, productDetail }) => {
    return (
        <React.Fragment>
            {(!_.isEmpty(productListing)) ? (
                Object.keys(productListing).map((keyName, keyIndex) => {
                    var sideHeading, more, customUrl, count = '', subCount = '', productsData = [];
                    if (homePage ? (keyName !== "banner" && keyName !== "shopByCategory") : (keyName !== "categoryName" && keyName !== "subcategoryName" && keyName !== "brand" && keyName !== "categoryNamesArray" && keyName !== "products" && keyName !== "maxRange" && keyName !== "minRange" && keyName !== "total" && keyName !== `All ${categoryName}`)) {
                        sideHeading = productListing[keyName].categoryName || (keyName);
                        // sideHeading = productListing[keyName].categoryName 
                        if (homePage) productsData = productListing[keyName] && productListing[keyName].length ? productListing[keyName] : '';
                        else productsData = productListing[keyName] && productListing[keyName].products && productListing[keyName].products.length ? productListing[keyName].products : '';
                        count = productListing[keyName].count;
                        return (
                            <div key={keyIndex}>
                                {homePage ? "" :
                                    (subCategoryName) ?
                                        subCategoryName.map(data => {
                                            if (sideHeading === data.categoryName) {
                                                more = data._id;
                                                customUrl = data.customUrl;
                                                subCount = data.count;
                                            }
                                            return '';
                                        })
                                        : ""}
                                {(productsData && productsData.length) && (
                                    <React.Fragment>
                                        <section className="product-slider-section section product-slider-part">
                                            <h3 className="sub-title" onClick={homePage ? () => onClick(sideHeading) : (count > 0 || products === true) ? (() => onClick(more, customUrl)) : (() => onClick())}>
                                                {((sideHeading === 'recentlyAdded' && 'Agregados Recientemente') || (sideHeading === 'bestSeller' && 'Más Vendidos') || (sideHeading === 'featuredProducts' && 'Productos Recomendados') || sideHeading)}
                                            </h3>
                                            <OwlCarousel {...productSlider}>
                                                {productsData.map((each, id) => {
                                                    if (id < 12) {
                                                        let salePrice = each && each.salePrice ? "$" + each.salePrice : "";
                                                        let regularPrice = each && each.price ? "$" + each.price : "";
                                                        let photo = each && each.productImage ? IMAGE_URL + each.productImage.slice(0, -4) + "-sm.jpg" : IMAGE_URL + "product.jpg";
                                                        let image = photo;
                                                        return (
                                                            <div className={each.stockQuantity > 0 ? "product-itemBox" : "product-itemBox blur_image"} key={id} >
                                                                <Link to={{ pathname: (productDetail ? `/categorias${productDetail}/${each.customUrl}` : `/categorias/${each.customUrl}`), state: { url: url } }} >
                                                                    <div className="imgBox">
                                                                        {/* {homePage ?
                                                                            <LazyLoadImage
                                                                                className="img-fluid"
                                                                                alt={each.productName}
                                                                                height={'23px'}
                                                                                src={image} // use normal <img> attributes as props
                                                                                width={"81px"} /> : */}

                                                                        <img src={image} alt={each.productName} width="81px" height="23px" className="img-fluid" />
                                                                    </div>
                                                                </Link>
                                                                <div className="pro-details">
                                                                    <Link to={{ pathname: (url ? `/categorias/${url}${each.customUrl}` : `/categorias/${each.customUrl}`), state: { url: url, categoryName1: categoryName1, categoryName2: categoryName2, categoryName3: categoryName3 } }} >
                                                                        <h6 className="product-name">
                                                                            {each.productName} <br />
                                                                        </h6>
                                                                    </Link>
                                                                    <h4>{salePrice === "" ? regularPrice : salePrice}
                                                                        <strike>{salePrice === regularPrice ? "" : regularPrice}</strike>
                                                                    </h4>
                                                                    <Button data={each} />
                                                                </div>
                                                            </div>
                                                        )
                                                    }
                                                    return '';
                                                })}
                                                {homePage && productsData && productsData.length >= 8 ?
                                                    <div className="item">
                                                        <div className="product-itemBox-more">
                                                            <a onClick={homePage ? () => onClick(sideHeading) : () => onClick(more, customUrl)} className="btn moreBtn more-product-btn"><span>›</span> Más</a>
                                                        </div>
                                                    </div> :
                                                    (!homePage && productsData && productsData.length >= 8 && subCount > 0 ?
                                                        <div className="item">
                                                            <div className="product-itemBox-more">
                                                                <a onClick={homePage ? () => onClick(sideHeading) : () => onClick(more, customUrl)} className="btn moreBtn more-product-btn"><span>›</span> Más</a>
                                                            </div>
                                                        </div> : null)
                                                }
                                            </OwlCarousel>
                                        </section>
                                    </React.Fragment>)}
                            </div>
                        )
                    }
                    return '';
                })
            ) : ''

            }
        </React.Fragment >);
}

export default CommonOwl;




