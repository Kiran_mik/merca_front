import React from 'react';

const File = ({ lable, imagePreview, name, profileEditButton, onChange }) => {
    // console.log('file inpiut', lable)
    return (
        <div className="col-md-12 form-group">
            {/* <div className="form-group"> */}
            <label htmlFor={name}>{lable}</label>
            <div className="profile-picture">
                <div className="inner-image">
                    {imagePreview}
                    <a><i className="fa fa-camera" aria-hidden="true" ></i></a>
                </div>
                <div className="upload-file">
                    <input
                        className="file-input"
                        type="file"
                        name={name}
                        id={name}
                        accept="image/*"
                        onChange={onChange} disabled={profileEditButton}
                    />
                </div>

            </div>
        </div>
        // </div>
    );
}

export default File;