import React from 'react';

const Input = ({ label, className, name, error, ...rest }) => {
    return (
        <div className={className}>
            <div className="col-sm-12 form-group">
                {label && <label htmlFor={name}>{label}</label>}
                <input
                    className={error ? "form-control input-error" : "form-control"}
                    name={name}
                    id={name}
                    aria-describedby="emailHelp"
                    {...rest}
                />
                <span className="error-msg">{error}</span>
            </div>
        </div>
    );
}

export default Input;