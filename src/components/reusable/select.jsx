import React from 'react';

const Select = ({ className, lable, placeholder, dataList, error, ...rest }) => {
    return (
        <div className={className}>
            <div className="col-sm-12 form-group">
                <label>{lable}</label>
                <select
                    className={error ? "form-control input-error" : "form-control"}
                    multiple={false}
                    placeholder={placeholder}
                    {...rest}
                >
                    <option value="">{placeholder}</option>
                    {dataList.length > 0 && dataList.map((list, i) => <option value={list.province} key={i}>{list.province} </option>)}
                </select>
                <span className="error-msg">{error}</span>
            </div >
        </div >
    );
}

export default Select;