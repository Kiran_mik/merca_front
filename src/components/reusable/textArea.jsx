import React from 'react';

const TextArea = ({ label, name, error, ...rest }) => {
    return (<div className="row">
        <div className="col-sm-12 form-group">
            <label className="d-block" htmlFor={name}>{label}</label>
            <textarea
                className="form-control"
                type="textarea"
                name={name}
                id={name}
                maxLength={6000}
                rows={7}
                {...rest}
            // value={value}
            // placeholder={placeholder}
            // onChange={onChange} 
            />
            <span className="error-msg">{error}</span>
        </div>
    </div>);
}

export default TextArea;