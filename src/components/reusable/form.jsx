import React, { Component } from 'react';
import { isEmpty } from 'lodash';
import CF from '../common/commonFuntions'; // COMMON FUNCTIONS
import EM from '../common/errorMessages'; // ERROR MESSAGE
import Input from './input';
import CheckBox from './checkbox';
import Select from './select';
import File from './file';
import TextArea from './textArea';
// import { max } from 'moment';

class Form extends Component {

    state = {
        formData: {},
        errors: {},
        isFormValid: true,
    }

    /*******************  For submitting the Form : Start ******************/
    handleSubmit = (event) => {
        event.preventDefault();
        if (this.validateForm()) {
            this.doSubmit();
        }
    }
    /*******************  For submitting the Form : End ******************/

    /*******************  Form validation : Start ******************/
    validateForm = () => {
        let { errors, formData } = this.state;
        const { emailId, password, confirmPassword, firstName, lastName, mobileNo, name, addressLine1, addressLine2, city, state, zipCode, pin, phone,
            reviewRating, reviewTitle, reviewDescription
        } = formData;
        // for email
        if (formData.hasOwnProperty('emailId')) {
            if (isEmpty(emailId)) errors.emailId = EM.PROVIDE_EMAIL;
            else if (!CF.validateEmail(emailId)) errors.emailId = EM.PROVIDE_VALID_EMAIL;
            else delete errors.emailId;
        }
        // for password
        if (formData.hasOwnProperty('password')) {
            if (isEmpty(password)) errors.password = EM.PROVIDE_PASSWORD;
            else if (!CF.validatePassword(password)) errors.password = EM.PROVIDE_VALID_PASSWORD;
            else delete errors.password;
        }
        // for conform password
        if (formData.hasOwnProperty('confirmPassword')) {
            if (isEmpty(confirmPassword)) errors.confirmPassword = EM.PROVIDE_CONFIRM_PASSWORD;
            else if (password !== confirmPassword) errors.confirmPassword = EM.PASSWORD_NOT_MATCHED
            else delete errors.password;
        }
        // for firstName
        if (formData.hasOwnProperty('firstName')) {
            if (isEmpty(firstName.trim())) errors.firstName = EM.PROVIDE_FIRST_NAME;
            else delete errors.firstName;
        }
        // for lastName
        if (formData.hasOwnProperty('lastName')) {
            if (isEmpty(lastName.trim())) errors.lastName = EM.PROVIDE_LAST_NAME;
            else delete errors.lastName;
        }
        // for mobileNumber
        if (formData.hasOwnProperty('mobileNo')) {
            if (!mobileNo) delete errors.mobileNo;
            // if (!mobileNo) errors.mobileNo = EM.PROVIDE_MOBILE_NUMBER;
            else if (!CF.validateMobileNumber(mobileNo)) errors.mobileNo = EM.PROVIDE_VALID_MOBILE_NUMBER;
            else delete errors.mobileNo;
        }
        // for name
        if (formData.hasOwnProperty('name')) {
            if (isEmpty(name.trim())) errors.name = EM.PROVIDE_NAME;
            else delete errors.name;
        }

        // for pin
        if (formData.hasOwnProperty('pin')) {
            if (isEmpty(pin.trim())) errors.pin = EM.PROVIDE_PIN;
            else if (!CF.validatePinNumber(pin)) errors.pin = EM.PROVIDE_PIN;
            else delete errors.pin;
        }
        // for addressLine1
        if (formData.hasOwnProperty('addressLine1')) {
            if (isEmpty(addressLine1.trim())) errors.addressLine1 = EM.PROVIDE_ADDRESS_LINE_1;
            else delete errors.addressLine1;
        }
        // for addressLine2
        // if (formData.hasOwnProperty('addressLine2')) {
        //     if (isEmpty(addressLine2.trim())) errors.addressLine2 = EM.PROVIDE_ADDRESS_LINE_2;
        //     else delete errors.addressLine2;
        // }
        // for city
        if (formData.hasOwnProperty('city')) {
            if (isEmpty(city.trim())) errors.city = EM.PROVIDE_CITY;
            else delete errors.city;
        }
        // for state
        if (formData.hasOwnProperty('state')) {
            if (isEmpty(state.trim())) errors.state = EM.PROVIDE_STATE;
            else delete errors.state;
        }
        // for zipCode
        if (formData.hasOwnProperty('zipCode')) {
            if (!zipCode) errors.zipCode = EM.PROVIDE_ZIPCODE;
            else if (!CF.validatePincodeNumber(zipCode)) errors.zipCode = EM.PROVIDE_VALID_ZIPCODE;
            else delete errors.zipCode;
        }
        // for phone
        if (formData.hasOwnProperty('phone')) {
            if (!phone) errors.phone = EM.PROVIDE_MOBILE_NUMBER;
            else if (!CF.validateMobileNumber(phone)) errors.phone = EM.PROVIDE_VALID_MOBILE_NUMBER;
            else delete errors.phone;
        }
        // for reviewRating
        if (formData.hasOwnProperty('reviewRating')) {
            if (!reviewRating) errors.reviewRating = EM.PROVIDE_RATING;
            else delete errors.reviewRating;
        }
        // for reviewTitle
        if (formData.hasOwnProperty('reviewTitle')) {
            if (!reviewTitle) errors.reviewTitle = EM.PROVIDE_TITLE;
            else delete errors.reviewTitle;
        }
        // for reviewDescription
        if (formData.hasOwnProperty('reviewDescription')) {
            if (!reviewDescription) errors.reviewDescription = EM.PROVIDE_DESCRIPTION;
            else delete errors.reviewDescription;
        }
        const isFormValid = Object.keys(errors).length !== 0 ? false : true;
        this.setState({ errors, isFormValid });
        return isFormValid;
    }
    /*******************  Form validation : End ******************/
    /***************** Handle Change : Start ******************************/
    handleChange = ({ target }) => {
        const { name, value, checked } = target;
        let { errors, isFormValid, formData } = this.state;
        if (!isFormValid) {
            if (name === 'emailId')
                if (!CF.validateEmail(value)) errors[name] = EM.PROVIDE_VALID_EMAIL;
                else delete errors[name];

            if (name === 'password')
                if (!CF.validatePassword(value)) errors[name] = EM.PROVIDE_VALID_PASSWORD;
                else delete errors[name];

            if (name === 'confirmPassword')
                if (!CF.validatePassword(value)) errors[name] = EM.PROVIDE_CONFIRM_PASSWORD;
                else delete errors[name];


            if (name === 'pin')
                if (!CF.validatePinNumber(value)) errors[name] = EM.PROVIDE_PIN;
                else delete errors[name];

            // if (name === 'mobileNo')
            //     if (!value) errors.mobileNo = EM.PROVIDE_MOBILE_NUMBER;
            //     else delete errors[name];

            if (name === 'firstName')
                if (isEmpty(value.trim())) errors.firstName = EM.PROVIDE_FIRST_NAME;
                else delete errors[name];

            if (name === 'lastName')
                if (isEmpty(value.trim())) errors.lastName = EM.PROVIDE_LAST_NAME;
                else delete errors[name];

            // For address
            if (name === 'name') {
                if (isEmpty(value.trim())) errors.name = EM.PROVIDE_NAME;
                else delete errors[name];
            }
            if (name === 'pin') {
                if (isEmpty(value.trim())) errors.pin = EM.PROVIDE_PIN;
                else delete errors[name];
            }

            if (name === 'addressLine1')
                if (isEmpty(value.trim())) errors[name] = EM.PROVIDE_ADDRESS_LINE_1;
                else delete errors[name];

            // if (name === 'addressLine2')
            //     if (isEmpty(value.trim())) errors[name] = EM.PROVIDE_ADDRESS_LINE_2;
            //     else delete errors[name];

            if (name === 'city')
                if (isEmpty(value.trim())) errors[name] = EM.PROVIDE_CITY;
                else delete errors[name];

            if (name === 'state')
                if (isEmpty(value.trim())) errors[name] = EM.PROVIDE_STATE;
                else delete errors[name];

            if (name === 'zipCode')
                if (isEmpty(value)) errors[name] = EM.PROVIDE_ZIPCODE;
                else delete errors[name];

            if (name === 'phone')
                if (isEmpty(value)) errors[name] = EM.PROVIDE_MOBILE_NUMBER;
                else delete errors[name];

            if (name === 'reviewRating')
                if (isEmpty(value)) errors[name] = EM.PROVIDE_RATING;
                else delete errors[name];

            if (name === 'reviewTitle')
                if (isEmpty(value)) errors[name] = EM.PROVIDE_TITLE;
                else delete errors[name];

            if (name === 'reviewDescription')
                if (isEmpty(value)) errors[name] = EM.PROVIDE_DESCRIPTION;
                else delete errors[name];
        }
        // Login
        if (name === 'emailId' || name === 'password' || name === 'confirmPassword' || name === 'addressLine1' || name === 'state' || name === 'reviewTitle' || name === 'reviewDescription' || name === 'reviewRating')
            formData[name] = value;

        // for remember me
        if (name === 'rememberMe')
            formData[name] = checked;

        // For profile
        if (name === 'firstName' && CF.allowChar(value))
            formData[name] = value;

        if (name === 'lastName' && CF.allowChar(value))
            formData[name] = value;

        if (name === 'mobileNo' && value.length <= 20 && (CF.allowNumbers(value) || value === ''))
            formData[name] = value;

        // For address
        if (name === 'name' && CF.allowChar(value))
            formData[name] = value;
        if (name === 'pin' && value.length <= 8 && CF.allowNumbers(value))
            formData[name] = value;
        // For City
        if (name === 'city' && CF.allowChar(value))
            formData[name] = value;

        // For zipCode
        if (name === 'zipCode' && value.length <= 4 && (CF.allowNumbers(value) || value === ''))
            formData[name] = value;

        // For phone
        if (name === 'phone' && value.length <= 20 && (CF.allowNumbers(value) || value === ''))
            formData[name] = value;

        // for set as primary
        if (target.type === 'checkbox')
            if (target.name.length === 24)
                this.setAsDefaultAddress(target.name)

        this.setState({ formData, errors });
    }
    /***************** Handle Change : End *******************************/

    // for Input field
    renderInput(className, autoFocus, name, type, value, placeholder, disabled = false, lable = '') {
        const { errors } = this.state;
        return (
            <Input
                label={lable}
                className={className}
                autoFocus={autoFocus}
                name={name}
                type={type}
                value={value}
                placeholder={placeholder}
                onChange={this.handleChange}
                error={errors[name]}
                disabled={disabled}
            />
        )
    }

    // for check box
    renderCheckBox(blockClass, checkBoxClass, lable, name, checked) {
        return (
            <CheckBox
                blockClass={blockClass}
                checkBoxClass={checkBoxClass}
                lable={lable}
                name={name}
                checked={checked}
                onChange={this.handleChange}
            />
        )
    }

    // for file input
    renderFile(lable, name, imagePreview, profileEditButton) {
        return (
            <File
                lable={lable}
                name={name}
                imagePreview={imagePreview}
                profileEditButton={profileEditButton}
                onChange={this.fileUpload}
            />
        )
    }

    // render select
    renderSelect(className, lable, placeholder, name, value, dataList) {
        const { errors } = this.state;
        // console.log(" placeholder ===>>>",placeholder);
        return (
            <Select
                className={className}
                lable={lable}
                placeholder={placeholder}
                name={name}
                value={value}
                dataList={dataList}
                onChange={this.handleChange}
                error={errors[name]}
            />
        )
    }

    // render Text area
    renderTextArea(lable, name, value, placeholder) {
        const { errors } = this.state;
        return (
            <TextArea
                lable={lable}
                name={name}
                value={value}
                placeholder={placeholder}
                onChange={this.handleChange}
                error={errors[name]}
            />
        )
    }

    // for buttton
    renderButton(blockClass, buttonClass, label) {
        return (
            <div className={blockClass}>
                <button className={buttonClass} type="submit"><span>{label}</span></button>
            </div>
        )
    }

}

export default Form;