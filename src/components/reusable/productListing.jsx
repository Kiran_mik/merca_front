import React from 'react';
// import StarRatingComponent from 'react-star-rating-component';
import { Link } from 'react-router-dom';
import { IMAGE_URL } from '../../config/configs';
import Button from '../common/Button';
import _ from 'lodash';
import { LazyLoadImage } from 'react-lazy-load-image-component';

const ProductListing = ({ productListing, msg, url, productSubCat, categoryName1, categoryName2, categoryName3, productDetail }) => {
    return (
        <React.Fragment>
            {(!_.isEmpty(productListing)) ? <div className="item">
                {productListing.map((each, i) => {
                    const salePrice = (each.salePrice) ? "$" + each.salePrice : "";
                    let regularPrice = (each.price) ? "$" + each.price : "";
                    let photo = each && each.productImage ? IMAGE_URL + each.productImage.slice(0, -4) + "-sm.jpg" : IMAGE_URL + "product.jpg";
                    let image = photo;
                    // let rating = each.rating;
                    return (<div className={each.stockQuantity > 0 ? "product-itemBox" : "product-itemBox blur_image"} key={i} >
                        <Link to={{ pathname: (productDetail ? `/categorias${productDetail}/${each.customUrl}` : `/categorias/${each.customUrl}`), state: { url: productSubCat } }}>
                            <div className="imgBox" >
                                {/* <LazyLoadImage
                                    className="img-fluid"
                                    alt={each.productName}
                                    height={'23px'}
                                    delayTime={100}
                                    src={image} // use normal <img> attributes as props
                                    width={"81px"} /> */}
                                <img src={image} alt={each.productName} width="81px" height="23px" className="img-fluid" />
                            </div>
                        </Link>
                        <div className="pro-details">
                            <Link to={`/categorias/${each.customUrl}`}>
                                <h6 className="product-name">{each.productName} <br /></h6>
                            </Link>
                            <h4> {salePrice === "" ? regularPrice : salePrice}
                                <strike>{salePrice === regularPrice ? "" : regularPrice}</strike></h4>
                            <Button data={each} />
                        </div>
                    </div>)
                })}
            </div> : (msg !== '' && productListing.length === 0) ? <div className="no-more-item "> Sin productos!</div> : null}
        </React.Fragment>
    );
}

export default ProductListing;