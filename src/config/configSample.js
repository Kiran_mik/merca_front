module.exports = {
    IMAGE_URL: 'https://media.mercadomayorista.com/upload/',
    API_URL: "https://api.mercadomayorista.com/api",
    PRODUCT_URL: "https://dev.mercadomayorista.com/categorias/",
    PORT: 4162,
    // ENV: 'dev'
};