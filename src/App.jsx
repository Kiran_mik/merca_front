import React, { Component, lazy, Suspense } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { PersistGate } from 'redux-persist/integration/react';
import { store, persistor } from './store';
import { Provider } from 'react-redux';
import Header from './components/common/Header';
import SideBar from './components/common/SideBar';
import Offlinepage from './components/public/Offlinepage';
import { Offline, Online } from "react-detect-offline";

const HomePage = lazy(() => import('./components/public/HomePage'));
const ProductSubCategory = lazy(() => import("./components/public/ProductSubCategory"));
const HomePageProducts = lazy(() => import('./components/public/HomePageProducts'));
const ProductCategory = lazy(() => import('./components/public/ProductCategory'));
const ForgotPassword = lazy(() => import('./components/public/ForgotPassword'));
const ProductListing = lazy(() => import('./components/public/ProductListing'));
const SearchProducts = lazy(() => import('./components/public/SearchProducts'));
const Cart = lazy(() => import('./components/public/Cart'));
const Login = lazy(() => import('./components/public/Login'));
const AboutUs = lazy(() => import('./components/public/AboutUs'));
const Register = lazy(() => import('./components/public/Register'));
const ResetPassword = lazy(() => import('./components/private/ResetPassword'));
const CheckOut = lazy(() => import('./components/private/CheckOut'));
const ProductDetails = lazy(() => import('./components/private/ProductDetails'));
const Profile = lazy(() => import('./components/private/Profile'));
const VerifiedUser = lazy(() => import('./components/private/verifiedUser'));
const PageNotFound = lazy(() => import('./components/public/PageNotFound'));
const TermsAndConditions = lazy(() => import('./components/public/TermsAndConditions'));
const FAQ = lazy(() => import('./components/public/FAQ'));

class App extends Component {
	Authorization = () => {
		let token = localStorage.getItem('token')
		let withOutToken = localStorage.getItem("withOutToken");
		var withOutToken_Array = JSON.parse(withOutToken)
		return (token && withOutToken_Array && withOutToken_Array.length > 0) ? true : false
	}

	render() {
		const PrivateRoute = ({ component: PComponent, ...rest }) => (
			<Route {...rest} render={(props) => (this.Authorization() ? <PComponent {...props} /> : <Redirect to='/' />)} />
		);
		return (
			<Provider store={store}>
				<div className="page-wrap">
					<Header />
					{/* <SideBar /> */}
					<Offline>
						<Offlinepage />
					</Offline>
					<Online>
						<PersistGate loading={null} persistor={persistor}>
							<Suspense fallback={<div></div>}>
								<Switch>
									<Route exact path="/" component={HomePage} />
									<Route exact path="/login" component={Login} />
									<Route exact path="/products" component={HomePageProducts} />
									<Route exact path="/searchProducts" component={SearchProducts} />
									<Route exact path="/register" component={Register} />
									<Route exact path="/forgotPassword" component={ForgotPassword} />
									<Route exact path="/resetPassword" component={ResetPassword} />
									<Route exact path="/profile" component={Profile} />
									<Route exact path="/categorias/:url?/:url1?/:url2?/:customUrl" component={ProductDetails} />
									<PrivateRoute exact path="/checkOut" component={CheckOut} />
									<Route exact path="/cart" component={Cart} />
									<Route exact path="/verifiedUser" component={VerifiedUser} />
									<Route exact path="/aboutUs" component={AboutUs} />
									<Route exact path="/termsAndConditions" component={TermsAndConditions} />
									<Route exact path="/preguntas-frecuentes" component={FAQ} />
									<Route exact path="/:customUrl" component={ProductCategory} />
									<Route exact path="/:customUrl1/:customUrl" component={ProductListing} />
									<Route exact path="/:customUrl1/:customUrl2/:customUrl" component={ProductSubCategory} />
									<Route path='/pagenotfound' component={PageNotFound} />
								</Switch>
							</Suspense>
						</PersistGate>
					</Online>
				</div>
			</Provider>
		);
	}
}

export default App; 