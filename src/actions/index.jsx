import axios from 'axios';
import { API_URL } from '../config/configs';

/************************************** Logout **********************************************/
export const LOG_OUT = 'log_out';
export const logOut = () => async dispatch => {
	try {
		await localStorage.removeItem("token");
		dispatch({ type: LOG_OUT, payload: { undefined } });
	}
	catch (error) {
		console.log('error ===>>>', error);
	}
};
/************************************** Register ********************************************/
export const USER_REGISTER = 'user_register';
export const register = (body, callback) => async dispatch => {
	try {
		let response = await axios({
			method: 'post',
			url: API_URL + '/user/register',
			headers: {
				"Content-Type": "application/json"
			},
			data: body
		});
		let { data } = response;
		if (data.status === 1) {
			let payload = {}
			dispatch({ type: USER_REGISTER, payload });
		}
		callback(response);
	} catch (error) {
		console.log('error ===>>>', error);
	}
};
/*********************************** GetSingleProduct ***********************************/
export const GET_SINGLE_PRODUCT = 'single_product';
export const getSingleProduct = (customUrl) => async dispatch => {
	dispatch({ type: GET_SINGLE_PRODUCT, payload: customUrl })
}
/*************************** Get Categories Id from SideBar *****************************/
export const GET_PRODUCTS = 'get_products';
export const getproducts = (id) => async dispatch => {
	dispatch({ type: GET_PRODUCTS, payload: id })
}
/*************************** Get Categories Id from homePage *****************************/
export const GET_PRODUCTS_Cat = 'get_products_cat';
export const getproductIdCat = (id) => async dispatch => {
	dispatch({ type: GET_PRODUCTS_Cat, payload: id })
}
/******************************** Get SideBar Categories *********************************/
export const CAT_DETAILS = 'get_category_name';
export const catName = (name) => async dispatch => {
	dispatch({ type: CAT_DETAILS, payload: name });
}
/********************** productListing Id - productSubCategory **************************/
export const GET_SUBCATNAME = 'get_subcat_name';
export const getSubCat = (id, name) => async dispatch => {
	dispatch({ type: GET_SUBCATNAME, payload: { id, name } })
}
/**************************** Home Page Slider Ids **************************************/
export const SUB_PAGE = 'sub_page';
export const subPage = (body) => async dispatch => {
	dispatch({ type: SUB_PAGE, payload: body })
}
/************************** update name and photo in header *****************************/
export const USER_INFO = 'user_info';
export const userInfo = (userName, userPhoto) => async dispatch => {
	dispatch({ type: USER_INFO, payload: { userName, userPhoto } })
}
/************************************ withOut Token *************************************/
export const WITHOUT_TOKEN = 'without_token';
export const withOutToken = (body) => async dispatch => {
	dispatch({ type: WITHOUT_TOKEN, payload: body })
}
/*********************************** SideBar Id ****************************************/
export const SIDE_BAR = 'side_bar';
export const sideBarId = (id, customUrl) => async dispatch => {
	// console.log("sidebar", customUrl, id)
	dispatch({ type: SIDE_BAR, payload: { id, customUrl } })
}
/***************************** Select-Drop-Down(value) **********************************/
export const SELECT_DROP_DOWN = 'select_drop_down';
export const selectDropDown = (string) => async dispatch => {
	// console.log("selectDropDown from redux", string)
	dispatch({ type: SELECT_DROP_DOWN, payload: string })
}
/************************************ Search Data *************************************/
export const SEARCH_BAR = 'search_bar';
export const searchBar = (body) => async dispatch => {
	dispatch({ type: SEARCH_BAR, payload: body })
}
/************************************ Search Data *************************************/
export const OPEN_SIDEBAR = 'open_SideBar';
export const openSideBar = (name) => async dispatch => {
	// console.log('actions===>', name)
	dispatch({ type: OPEN_SIDEBAR, payload: name })
}

