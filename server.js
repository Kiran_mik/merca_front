const compression = require('compression');
const express = require('express');
const path = require('path');
const app = express();
const { PORT, ENV } = require('./src/config/configs')
const fs = require('fs')

// app.get('/', function (request, response) {
// 	console.log('Home page visited!');
// 	const filePath = path.resolve(__dirname, './build', 'index.html');
// 	fs.readFile(filePath, 'utf8', function (err, data) {
// 		if (err) {
// 			return console.log(err);
// 		}
// 		data = data.replace(/\$OG_TITLE/g, 'Home Page');
// 		data = data.replace(/\$OG_DESCRIPTION/g, "Home page description");
// 		result = data.replace(/\$OG_IMAGE/g, 'https://i.imgur.com/V7irMl8.png');
// 		response.send(result);
// 	});
// });
app.use(compression());
app.use(express.static(path.join(__dirname, 'build')));

app.get('/*', (req, res) => {
	res.sendFile(path.join(__dirname, 'build', 'index.html'));
});
// console.log("Environtment ===>>>",ENV);

// if (ENV === 'staging') {
// 	// configure https for listening server.
// 	const options = {
// 		key: fs.readFileSync('/home/node/ssl/privkey1.pem'),
// 		cert: fs.readFileSync('/home/node/ssl/fullchain1.pem')
// 	};

// 	// Listening To API Server
// 	let server = https.createServer(options, app);
// 	server.listen(serverPort, () => {
// 		console.log(`Server running at :${serverPort}`);
// 	});
// }
// else {
// https
app.listen(PORT, () => console.log(`server is listening on port ${PORT}`));
// }
